<?php

namespace App\Http\Controllers\Api\TeamApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\ModelPricing;


class ModelPricingController extends Controller
{
	public function modelList(){

			$getmodel = ModelPricing::get()->all();
			if(!empty($getmodel)){

			return response()->json(['data'=>$getmodel, 'status'=>200,'message'=>'Fetch model list.']);
			}
			else{
				return response()->json(['status'=>404,'message'=>'No model found..!']);
			}
	}

		public function businessmodelList(){

			$getmodel = ModelPricing::get()->all();
			if(!empty($getmodel)){

			return response()->json(['data'=>$getmodel, 'status'=>200,'message'=>'Fetch model list.']);
			}
			else{
				return response()->json(['status'=>404,'message'=>'No model found..!']);
			}
	}
}