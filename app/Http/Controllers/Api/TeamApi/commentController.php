<?php

namespace App\Http\Controllers\Api\TeamApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Customer;
use App\Models\Order;
use App\Models\Comment;
use App\Models\deliveryComments;
use App\Models\OrderitemComments;
use App\Models\Salesman;

class commentController extends Controller
{
	public function addComment(Request $request){
		$validator = Validator::make($request->all(), [ 
		    'user_id' => 'required',
		    'order_unique_id' => 'required', 
			'comment' => 'required'				
		]);
		if ($validator->fails()) {			
					return response()->json(['message'=>$validator->errors(),'status'=>400]);     
		}
		else{
            if($request->file('comment_image')){

                $image = $request->file('comment_image');
                $data['comment_image'] = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/comments');
                $image->move($destinationPath, $data['comment_image']);                
            }
			$data['user_id'] = $request->user_id;
			$data['order_unique_id'] = $request->order_unique_id;
            $data['comment'] = $request->comment;
			$comment = Comment::create($data);
			if($comment->count() >0)
            {
            	return response()->json(['status'=>200, 'message' => 'Comment successfully done..!']);
            }
            else{
            	return response()->json(['status'=>404, 'message' => 'Sorry! Please try again.']);
            }
		}
	}

	public function getComments(Request $request){
	    $validator = Validator::make($request->all(), [ 
		    'user_id' => 'required',
		    'order_unique_id' => 'required'			
		]);
		if ($validator->fails()) {			
					return response()->json(['message'=>$validator->errors(),'status'=>400]);     
		}
		else{
            $getcomments = Comment::where('order_unique_id',$request['order_unique_id'])
                ->get();
            if($getcomments->count() >0)
            {
                $commentArray = [];
                foreach ($getcomments as $value) {
                    $commentBy = Salesman::where('id',$value->user_id)->first();

                	$key['id'] = $value['id'];
                	$key['user_id'] = $value['user_id'];
                    $key['commentBy'] = $commentBy->title;
                	$key['order_unique_id'] = $value['order_unique_id'];
                	$key['comment'] = $value['comment'];
                	$key['comment_image'] = url('public/uploads/comments').'/'.$value['comment_image'];
                	$stamp1 = strtotime($value['created_at']);
                    $key['created_at'] = $stamp1*1000;
                	$commentArray[] = $key;
                }
                return response()->json(['result' =>$commentArray, 'status'=>200, 'message' => 'Comments..!']);
            }
            else{
            	return response()->json(['status'=>404, 'message' => 'No comment found..!']);
            }
		}		
	}

    public function addDeliveryComments(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'order_unique_id' => 'required', 
            'comment' => 'required'             
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            if($request->file('comment_image')){
                $image = $request->file('comment_image');
                $data['comment_image'] = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/deliverycomments');
                $image->move($destinationPath, $data['comment_image']);
            }
            $data['user_id'] = $request->user_id;
            $data['order_unique_id'] = $request->order_unique_id;
            $data['comment'] = $request->comment;
            $comment = deliveryComments::create($data);
            if($comment->count() >0)
            {
                return response()->json(['status'=>200, 'message' => 'Comment successfully done..!']);
            }
            else{
                return response()->json(['status'=>404, 'message' => 'Sorry! Please try again.']);
            }
        }
    }

    public function getDeliveryComments(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'order_unique_id' => 'required'         
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            $getcomments = deliveryComments::where('order_unique_id',$request['order_unique_id'])
                ->get();
            if($getcomments->count() >0)
            {
                $commentArray = [];
                foreach ($getcomments as $value) {
                    $key['id'] = $value['id'];
                    $key['user_id'] = $value['user_id'];
                    $key['order_unique_id'] = $value['order_unique_id'];
                    $key['comment'] = $value['comment'];
                    $key['comment_image'] = url('public/uploads/deliverycomments').'/'.$value['comment_image'];
                    $stamp1 = strtotime($value['created_at']);
                    $key['created_at'] = $stamp1*1000;
                    $commentArray[] = $key;
                }
                return response()->json(['result' =>$commentArray, 'status'=>200, 'message' => 'Comments..!']);
            }
            else{
                return response()->json(['status'=>404, 'message' => 'No comment found..!']);
            }
        }       
    }

    public function addCommentOrderItem(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'order_unique_id' => 'required', 
            'order_product_id' => 'required', 
            'comment' => 'required',
            //'comment_image' => 'required'             
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            if($request->file('comment_image')){
                $image = $request->file('comment_image');
                $data['comment_image'] = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/orderitemcomments');
                $image->move($destinationPath, $data['comment_image']);                
            }
            $data['user_id'] = $request->user_id;
            $data['order_unique_id'] = $request->order_unique_id;
            $data['order_product_id'] = $request->order_product_id;
            $data['comment'] = $request->comment;
            $comment = OrderitemComments::create($data);
            if($comment->count() >0)
            {
                return response()->json(['status'=>200, 'message' => 'Comment successfully done..!']);
            }
            else{
                return response()->json(['status'=>404, 'message' => 'Sorry! Please try again.']);
            }
        }
    }

    public function getOrderItemsComments(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'order_unique_id' => 'required',         
            'order_product_id' => 'required'         
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            $getcomments = OrderitemComments::where('order_unique_id',$request['order_unique_id'])
                ->where('order_product_id',$request['order_product_id'])
                ->get();
            if($getcomments->count() >0)
            {
                $commentArray = [];
                foreach ($getcomments as $value) {
                    $commentBy = Salesman::where('id',$value->user_id)->first();

                    $key['id'] = $value['id'];
                    $key['user_id'] = $value['user_id'];
                    $key['commentBy'] = $commentBy->title;
                    $key['order_unique_id'] = $value['order_unique_id'];
                    $key['order_product_id'] = $value['order_product_id'];
                    $key['comment'] = $value['comment'];
                    $key['comment_image'] = url('public/uploads/orderitemcomments').'/'.$value['comment_image'];
                    $stamp1 = strtotime($value['created_at']);
                    $key['created_at'] = $stamp1*1000;
                    $commentArray[] = $key;
                }
                return response()->json(['result' =>$commentArray, 'status'=>200, 'message' => 'Comments..!']);
            }
            else{
                return response()->json(['status'=>404, 'message' => 'No comment found..!']);
            }
        }       
    }
}