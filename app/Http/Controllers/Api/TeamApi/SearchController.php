<?php

namespace App\Http\Controllers\Api\TeamApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Customer;
use App\Models\Salesman;
use App\Models\OtherUsers;
use App\Models\Assignments;
use App\Models\Order;

class SearchController extends Controller
{
	public function serchcustomer(Request $request){
		$customer = Customer::where('name', 'like', "{$request->name}")
		    ->Orwhere('customer_unique_id', 'like', "{$request->name}")
		    ->Orwhere('phone', 'like', "{$request->name}")
		    ->get();
		if($customer->count() >0)
		{
			$dataArray=array();
			foreach($customer as $value)
			{
			    $data['id'] = $value['id'];
			    $data['customer_unique_id'] = $value['customer_unique_id'];
			    $data['name'] = $value['name'];
			    $data['email'] = $value['email'];
			    $data['phone'] = $value['phone'];
			    $data['address'] = $value['address'];
			    $data['lat'] = $value['lat'];
			    $data['longs'] = $value['longs'];
			    $data['created_by'] = $value['created_by'];
			    $data['account_type'] = $value['account_type'];
			    $data['total_order'] = $value['total_order'];
			    $data['total_points'] = $value['total_points'];
			    $dataArray[] = $data;
		    }
			return response()->json([ 'result'=>$dataArray, 'status'=>200, 'message'=>'Customer found successfully.']);
		}
		else{
			return response()->json(['status'=>404, 'message'=>'No data found!']);
		}
	}

	public function searchOrders(Request $request){
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {          
                    return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        else{
            $getnewOrders = Order::join('assignments','orders.order_unique_id', '=', 'assignments.order_unique_id')
                ->select('orders.order_unique_id','orders.customer_unique_id','orders.created_at','orders.expected_delivery_time','assignments.cutter_id','assignments.tailor_status','assignments.tailor_id','assignments.qc_status','assignments.qc_id')
                ->where('assignments.status','2')
                ->where('orders.order_unique_id', 'like', "%{$request->order_unique_id}%")
                ->groupBy('orders.order_unique_id')
                ->get();
            if($getnewOrders->count() >0)
            {
                $orderArray = [];
                foreach($getnewOrders as $order){
                    $value['order_unique_id'] = $order['order_unique_id'];
                    $stamp = strtotime($order['created_at']);
                    $value['order_date'] = $stamp*1000;
                    $stamp1 = strtotime($order['expected_delivery_time']);
                    $value['expected_delivery_time'] = $stamp1*1000;
                    $getquantity = Order::where('order_unique_id',$value['order_unique_id'])
                        ->get();
                    $quantity = 0;
                    foreach ($getquantity as $val) {
                        $qua = $val['quantity'];
                        $quantity += $qua;
                    }
                    $getcustomer = Customer::where('customer_unique_id',$order->customer_unique_id)
                        ->first();
                    $value['account_type'] = $getcustomer->account_type;
                    
                    $value['quantity'] = $quantity.' Items';
                    $value['cutter_id'] = $order['cutter_id'];
                    $getCutter = Salesman::where('type','4')
                        ->where('id',$value['cutter_id'])->first();
                    $value['cutter_assigned'] = $getCutter['title'];
                    $value['tailor_status'] = $order['tailor_status'];
                    if(!empty($order['tailor_id']) && $order['tailor_status']!='0'){
                        $getTailor = OtherUsers::where('id',$order['tailor_id'])
                            ->where('type','2')->first();
                        $value['tailor_assigned'] = $getTailor['name'];
                    }
                    $value['qc_status'] = $order['qc_status'];
                    if(!empty($order['qc_id']) && $order['qc_status']!='0'){
                        $getQc = Salesman::where('id',$order['qc_id'])->where('type','5')->first();
                        $value['qc_assigned'] = $getQc['title'];
                    }
                    $orderArray[] = $value;
                }
                return response()->json(['result'=>$orderArray, 'status'=>200, 'message' => 'Orders..!']);
            }
            else{
                return response()->json(['status'=>404, 'message' => 'Orders not found..!']);
            }
        }
    }
}