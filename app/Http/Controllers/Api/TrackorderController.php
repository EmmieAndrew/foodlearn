<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Trackorder;
use App\Models\Order;
use Illuminate\Support\Facades\Validator;

class TrackorderController extends Controller
{
    public function TrackOrder(Request $request){
    	$validator = Validator::make($request->all(), [  
            'order_unique_id' => 'required'
        ]);
        if ($validator->fails()) 
        {           
            return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        $id = $request->order_unique_id;
    	$track = Trackorder::join('orders','tracking_delivery.order_unique_id', '=', 'orders.order_unique_id')->select('*')->where('tracking_delivery.order_unique_id',$id)
        ->groupBy('orders.order_unique_id')
        ->get();
        $trackArray = [];
        foreach ($track as $value) {
            $trackDetails["order_unique_id"] = $value["order_unique_id"];
            $trackDetails["customer_unique_id"] = $value["customer_unique_id"];
            $trackDetails["customer_name"] = $value["customer_name"];
            $trackDetails["quantity"] = $value["quantity"];
            if($value["delivered_at"] == null){
                $stamp1 = strtotime($value["expected_delivery_time"]);
            }else{
                $stamp1 = strtotime($value["delivered_at"]);
            }
            $trackDetails["completion_date"] = $stamp1*1000;
            $trackDetails["order_placed"] = $value["order_placed"];
            $trackDetails["order_confirmed"] = $value["order_confirmed"];
            if($value["with_cutter"]=='1' && $value["with_tailor"]=='1' && $value["with_qa"]=='1')
            {
                $trackDetails["being_stitched"]="1";
            }
            else{
                $trackDetails["being_stitched"]="0";
            }
            $trackDetails["redy_for_delivery"] = $value["redy_for_delivery"];
            $trackDetails["delivered"] = $value["delivered"];
            $trackArray[] = $trackDetails;
        }
        
    	return response()->json(['result'=>$trackArray, 'status'=>200, 'message'=>'Tracking Details']);
    }
}
