<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
Use App\Models\Branch;

class BranchController extends Controller
{
    public function index(Request $request){
        $branches = Branch::get();
        //dd($branches->isEmpty());
        if($branches->isEmpty()){
            return response()->json(['status'=>404, 'message' => 'Branch not found..!']);
        }

        return response()->json(['result'=>$branches, 'status'=>200, 'message' => 'Retrieve branch list..!']);

    }
}
