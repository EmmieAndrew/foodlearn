<?php

namespace App\Http\Controllers\Api;

use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CustomerAppointment;
use App\Models\Order;
use Illuminate\Support\Facades\Validator;
use App\Models\CustomerDashboardImage;

class CustomerController extends Controller
{
    public function getimage(Request $request)
    {
        $validator = Validator::make($request->all(), [  
                'user_id' => 'required'
        ]);
        if ($validator->fails()) 
        {			
            return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        $Order = Order::where('customer_id',$request['user_id'])->first();
        if(!empty($Order))
        {
            $order_unique_id = $Order['order_unique_id'];
        }
        else{
            $order_unique_id = "";
        }
        $images = CustomerDashboardImage::get();
        $customers = Customer::select('name')->where('id',$request['user_id'])->first();
        
        foreach($images as $image)
        {
            $path[] =     url('public/images/dashboard').'/'.$image['image'];
        }
        $name = $customers['name'];
        return response()->json(['result'=>$path, 'order_unique_id'=>$order_unique_id, 'name'=>$name, 'status'=>200,'message'=>'Images received successfully']);
    }
    
    public function createAppointment(Request $request)
    {
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required',
            'name' =>'required',
            'address' => 'required',
            'phone' => 'required',
            'date_time' =>'required'
    ]);
    if ($validator->fails()) 
    {			
        return response()->json(['message'=>$validator->errors(),'status'=>400]);     
    }
    $data['customer_id'] = $request->user_id;
    $data['name'] = $request->name;
    $data['address'] = $request->address;
    $data['phone'] = $request->phone;
    $data['appointment_datetime'] = $request->date_time;
    $CustomerAppointment = CustomerAppointment::create($data);
    return response()->json(['user'=>$CustomerAppointment, 'status'=>200,'message'=>'Appointment booked successfully']);
    }

    public function update_profile(Request $request){
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required',
            'name' =>'required',
            'email' => 'required',
            'address' => 'required',
        ]);
        if ($validator->fails()) 
        {           
            return response()->json(['message'=>$validator->errors(),'status'=>400]);      
        }
            $user_id = $request->user_id;
            $name = $request->name;
            $email = $request->email;
            $address = $request->address;
            $lat = $request->lat;
            $longs = $request->longs;
            $customer = Customer::find($user_id);
            $customer->name = $name;
            $customer->email = $email;
            $customer->address = $address;
            $customer->lat = $lat;
            $customer->longs = $longs;
            $customer->update($request->all());
            return response()->json([ 'status'=>200,'message'=>'Profile updated successfully']);
    }

    public function updateProfile(Request $request){
        dd("dd");
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required',
            'name' =>'required',
            'email' => 'required',
            'address' => 'required',
        ]);
        if ($validator->fails()) 
        {           
            return response()->json(['message'=>$validator->errors(),'status'=>400]);      
        }
            $user_id = $request->user_id;
            $name = $request->name;
            $email = $request->email;
            $address = $request->address;
            $lat = $request->lat;
            $longs = $request->longs;
            $customer = Customer::find($user_id);
            dd($customer);
            $customer->name = $name;
            $customer->email = $email;
            $customer->address = $address;
            $customer->lat = $lat;
            $customer->longs = $longs;
            $customer->update($request->all());
            return response()->json([ 'status'=>200,'message'=>'Profile updated successfully']);

        }

    public function editProfile(Request $request){
        $validator = Validator::make($request->all(), [  
            'user_id' => 'required',
        ]);
        if ($validator->fails()) 
        {           
            return response()->json(['message'=>$validator->errors(),'status'=>400]);     
        }
        $profile = Customer::select('name','email','address','phone')
          ->where('id', '=', $request->user_id)
          ->first();
        return response()->json(['result'=>$profile, 'status'=>200]);

    }

}
