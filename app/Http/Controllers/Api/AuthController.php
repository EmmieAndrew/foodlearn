<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Customer;
use Twilio\Rest\Client;
use App\Mail\WelcomeEmail;

use Mail;
class AuthController extends Controller
{
	
	public function login(Request $request){
    	//print_r($request->all());die;
		$validator = Validator::make($request->all(), [
				'phone' => 'required',
		]);
		if ($validator->fails()) 
		{
		  return response()->json(['message'=>$validator->errors(),'status'=>401]);
		}
		$customer = Customer::select('phone','id','customer_unique_id','name')
		  ->where('phone', '=', $request->phone)
		  ->first();
		  //dd($customer);
		$customer_unique_id = $customer['customer_unique_id'];
		if(!empty($customer))
		{
		  $otp = rand(1000, 9999);
		  $message='Hello your OTP! is '.$otp;
		  $client = new \GuzzleHttp\Client();
          $res = $client->request('POST', 'http://www.oursms.net/api/sendsms.php', [
            'form_params' => [
                'username' => 'bamthobe',
                'password' => 'bam123',
                'message' => $message,
                'numbers' => $request->phone,
                'sender' => 'BAM',
                'unicode' => 'E',
                'return' => 'full',
            ]
        ]);
         $res->getStatusCode();
         $res->getHeader('content-type');
		 $res->getBody();
		  Customer::where('phone',$request->phone)->update(['otp' => $otp]);
		  return response()->json(['result' => $res, 'otp' => $otp, 'message'=>'OTP sent successfully','user_id'=>$customer['id'], 'user_name'=>$customer['name'], 'customer_unique_id' => $customer['customer_unique_id'], 'status'=>200, 'register_status' => 'Registered Already..!']);
		  }
		  else 
		  {
		  	$lastcustomer = Customer::select('id')
            ->orderBy('id', 'desc')
		  	->first();
		  	$lastid = $lastcustomer['id'];
		  	$newid = $lastid+1;
		  	$countid = strlen($newid);
		  	if($countid=='1')
		  	{
		  	    $madeid = '000'.$newid;
		  	}
		  	else if($countid=='2')
		  	{
		  	    $madeid = '00'.$newid;
		  	}
		  	else if($countid=='3')
		  	{
		  	    $madeid = '0'.$newid;
		  	}
		  	else{
		  	    $madeid = $newid;
		  	}
		  	$idcode = date('y-m').'-'.$madeid;
            $data['customer_unique_id'] = $idcode;
		  	$data['phone'] = $request->phone;
		  	$otps = rand(1000, 9999);
			$data['otp'] = $otps;
		    $message='Hello your OTP! is '.$otps;
		    $client = new \GuzzleHttp\Client();
            $res = $client->request('POST', 'http://www.oursms.net/api/sendsms.php', [
            'form_params' => [
                'username' => 'bamthobe',
                'password' => 'bam123',
                'message' => $message,
                'numbers' => $request->phone,
                'sender' => 'BAM',
                'unicode' => 'E',
                'return' => 'full',
                ]
            ]);
            $res->getStatusCode();
            // 200
            $res->getHeader('content-type');
            // 'application/json; charset=utf8'
			$res->getBody();
			$customer = Customer::create($data); 
			$customer = Customer::select('phone','id','customer_unique_id')
		  	->where('phone', '=', $request->phone)
		  	->first();
			return response()->json(['otp' => $otps, 'message'=>'OTP sent successfully','user_id'=>$customer['id'], 'customer_unique_id' => $customer['customer_unique_id'], 'status'=>200, 'register_status' => 'New Registration..!']);
		  }     		
	  }
	
	public function checkOtp(Request $request){ 	
		$validator = Validator::make($request->all(), [  
			   'user_id' => 'required', 
			   'otp' => 'required',
			   'device_token' => 'required'
		   ]);
		   if ($validator->fails()) {			
					   return response()->json(['message'=>$validator->errors(),'status'=>401]);     
			}
		   $customer = Customer::where('id', $request['user_id'])->first() ;
		   $user_otp=$request->otp;
		   $database_otp=$customer->otp;
		   if($user_otp==$database_otp){
		   	$device_token =$request->device_token;
		   	$resp= Customer::where('id', $request['user_id'])->update(['device_token'=>$device_token]);
			return response()->json(['message'=>"Successfully logged in",'status'=>200]);
		   }
		   else{
			return response()->json(['message'=>"OTP is incorrect",'status'=>400]); 
		   }
	
	}

	public function register(Request $request){
		$saveArray = $request->all();
		$validator = Validator::make($request->all(), [  
			'name' => 'required', 
			'email' => 'required',
			'address'=>'required',
			'phone'=>'required'
			
		]);
		if ($validator->fails()) {			
					return response()->json(['message'=>$validator->errors(),'status'=>400]);     
		}
		else{
			$user = Customer::where('phone',$request['phone'])
			->first();
            if (!empty($user)) {
            	if(!empty($user->email))
            	{
            		return response()->json(['message'=>'Sorry, please try with other phone number or gmail!','status'=>406]); 
            	}
            	else{
            		$data=array();
            	    $lastcustomer = Customer::select('id')
            	    ->orderBy('id', 'desc')
		  	        ->first();
		  	        $lastid = $lastcustomer['id'];
		  	        $newid = $lastid+1;
		  	        $countid = strlen($newid);
		  	        if($countid=='1')
		  	        {
		  	    	    $madeid = '000'.$newid;
		  	        }
		  	        else if($countid=='2')
		  	        {
		  	    	    $madeid = '00'.$newid;
		  	        }
		  	        else if($countid=='3')
		  	        {
		  	    	    $madeid = '0'.$newid;
		  	        }
		  	        else{
		  	    	    $madeid = $newid;
		  	        }
		  	        $idcode = date('y-m').'-'.$madeid;
            	    $customer_unique_id = $idcode;
		            $name=$request->name;
		            $email=$request->email;
		            $address=$request->address;
		            $phone=$request->phone;
		            $lat=$request->lat;
		            $longs=$request->longs;
		            $created_by='Self';
		            $customer = Customer::where('phone',$phone)->first();
		            $customer->customer_unique_id = $customer_unique_id;
		            $customer->name = $name;
                    $customer->email = $email;
                    $customer->address = $address;
                    $customer->lat = $lat;
                    $customer->longs = $longs;
                    $customer->created_by = $created_by;
                    $customer->update($request->all()); 
		            return response()->json(['result'=>$customer, 'status'=>200,'message'=>'Registerd successfully']);
            	}
            	
            }
            else{
            	$data=array();
            	$lastcustomer = Customer::select('id')
            	->orderBy('id', 'desc')
		  	    ->first();
		  	    $lastid = $lastcustomer['id'];
		  	    $newid = $lastid+1;
		  	    $countid = strlen($newid);
		  	    if($countid=='1')
		  	    {
		  	    	$madeid = '000'.$newid;
		  	    }
		  	    else if($countid=='2')
		  	    {
		  	    	$madeid = '00'.$newid;
		  	    }
		  	    else if($countid=='3')
		  	    {
		  	    	$madeid = '0'.$newid;
		  	    }
		  	    else{
		  	    	$madeid = $newid;
		  	    }
		  	    $idcode = date('y-m').'-'.$madeid;
            	$data['customer_unique_id'] = $idcode;
		        $data['name']=$request->name;
		        $data['email']=$request->email;
		        $data['address']=$request->address;
		        $data['phone']=$request->phone;
		        $data['lat']=$request->lat;
		        $data['longs']=$request->longs;
		        $data['created_by']='Self';
		        $customer = Customer::insertGetId($data); 
		        return response()->json(['result'=>$customer, 'status'=>200,'message'=>'Registerd successfully']);
            }
		}
	}

}
