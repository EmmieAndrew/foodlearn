<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\ReferralCode;
use App\Mail\VerifyMail;
use Mail;
use Illuminate\Http\Request;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'gender' => 'required',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|string|min:6|same:password',
            // 'code'  => 'exists:referral_codes,code', 
            'terms_accepted' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data 
     * @return \App\User
     */
    protected function create(array $data) {
        $referralcode=($data['code']);
        $referralcode_id= ReferralCode::select('id')->where('code',$referralcode)->first();
        $abc =$referralcode_id->id;
        $user = User::create([
            'name' => $data['name'],
            'gender' => $data['gender'],
            'email' => $data['email'],
            'referralcode_id' => $abc,
            'password' => bcrypt($data['password']),
            'verified' => 1,
            // 'verify_token' => str_random(40),
            'terms_accepted' => (isset($data['terms_accepted']))?1:0,
            // 'phone' => (isset($data['phone']))?$data['phone']:NULL,
        ]);

        // Mail::to($user->email)->send(new VerifyMail($user));

        return $user;
    }

    /*
     * Verify User Email
     */
    public function verifyUser($token) {
        $verifyUser = User::where('verify_token', $token)->first();
        if(isset($verifyUser) ) {
            if($verifyUser->status == '0') {
                return redirect('/login')->with('error', trans('message.InactiveUserLogin'));
            }
            if(!$verifyUser->verified) {
                $verifyUser->verified = 1;
                $verifyUser->verify_token = NULL;
                $verifyUser->save();
                $message = "Your email address is verified. Please login to continue.";
            } else {
                $message = "Your email address is already verified. Please login to continue.";
            }
        } else {
            return redirect('/login')->with('error', "Something went wrong. Please try again.");
        }
        return redirect('/login')->with('success', $message);
    }

    /*
     * Handle After Registration
     */
    /*protected function registered(Request $request, $user) {
        $this->guard()->logout();
        return redirect('/login')->with('success', 'We have sent you an activation code. Please check your email and click on the link to verify.');
    }*/

}
