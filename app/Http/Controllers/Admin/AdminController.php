<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use App\User;
use App\Customer;
use App\Models\Order;
use App\Models\Assignments;
use App\Models\Trackorder;
use App\Models\tax;
use App\Models\Branch;
use App\Models\Factory;
use Auth;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Edujugon\PushNotification\PushNotification;

class AdminController extends Controller
{
    use AuthorizesRequests, ValidatesRequests;
    
	public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Total place order
        $orders = Order::where('status','!=','0')->whereDate('created_at', '=', date('Y-m-d'))->with('orderBy')->groupBy('order_unique_id')->get();
        $totalPlaceOrders = $orders->count();
        
        //Branch waise placed order
        $branchePlaceOrders = Branch::with(['orderWithSale' => function($query) {
            $query->whereDate('orders.created_at', Carbon::today())->groupBy('orders.order_unique_id');
        }])->get();
        $braPlaceOrder =array();
            foreach ($branchePlaceOrders as $branch) {
                $orderCount = $branch->orderWithSale->count();
                $braPlaceOrder[$branch->branch] = $orderCount;
            }

        //Ready to assign
        $OrderReadyToAssign = Order::where('assignment',0)->where('status','!=','0')->get();
        $totalReadyToAssign = $OrderReadyToAssign->count();

        //Completed order
        $GetTotalOrderCompleted = Assignments::where('order_status', '1')->where('qc_status','2')->get();
        $totalOrderCompleted = $GetTotalOrderCompleted->count();

        //Defective order
        $defectByCustomer = Order::where('status','5')->groupBy('order_unique_id')->get();
        $defectByQc = Assignments::where('order_status', '0')->where('qc_status', '2')->get();
        $totalDefective = $defectByCustomer->count() + $defectByQc->count();
        
        //With cutter
        $cutterOrder = Assignments::where('status', '1')->get();
        $withCutter = $cutterOrder->count();

        //Revenue
        $orderRev = Order::where('status','!=','0')->whereDate('created_at', Carbon::today())->groupBy('order_unique_id')->get();
        $revenue = 0;
        $cash = 0;
        $credit = 0;
            foreach($orderRev as $order){
                $revenue += $order->total_amount;
                $cash += $order->amount_to_pay;
                $credit += $order->price_remaining;
            }

        //In factory
        $factory = Trackorder::where('with_tailor', '1')
        ->where('with_qa', '0')
        ->orwhere('with_qa', '1')
        ->where('redy_for_delivery', '0')
        ->get();
        $orderInFactory = $factory->count();

        //Delay order
        $bufferDays = tax::select('buffer_days')->first();
        $bufferDay = $bufferDays->buffer_days;
        $in = array('0','3','5');
        $orders = Order::whereNotIn('status',$in)->groupBy('order_unique_id')->get();
        $delayOrder = 0;
            foreach ($orders as $order) {
                $expectedDate = date('Y-m-d',strtotime($order->expected_delivery_time));
                $internalExpected = date('Y-m-d', strtotime($expectedDate. ' - '.$bufferDay.' days'));
                $today = date('Y-m-d');
                if($today == $internalExpected){
                    $count = 1;
                    $delayOrder += $count;
                }
        }

        //Branch Revenue
        $branchWithOrderRev = Branch::with(['orderWithSale' => function($query) {
            $query->whereDate('orders.created_at', Carbon::today())->groupBy('orders.order_unique_id');
        }])->get();

        if($branchWithOrderRev->count() >0)
        { 
            foreach ($branchWithOrderRev as &$branch) {
                $totalRevenue = 0;
                foreach ($branch->orderWithSale as $order) {
                    $total_amount = $order['total_amount'];
                    $totalRevenue += $total_amount;
                }
               $branch['total_branch_rev'] =  $totalRevenue;
            }
        }
        else{
            $branchWithOrderRev = array();
        }

        $data['totalPlaceOrders'] = $totalPlaceOrders;
        $data['braPlaceOrder'] = $braPlaceOrder;
        $data['totalReadyToAssign'] = $totalReadyToAssign;
        $data['totalOrderCompleted'] = $totalOrderCompleted;
        $data['totalDefective'] = $totalDefective;
        $data['withCutter'] = $withCutter;
        $data['branchWithOrderRev'] = $branchWithOrderRev;
        $data['revenue'] = $revenue;
        $data['cash'] = $cash;
        $data['credit'] = $credit;
        $data['orderInFactory'] = $orderInFactory;
        $data['delayOrder'] = $delayOrder;
        
        return view('admin.dashboard')->with('data', $data);
    }

    /*
     * Change admin password
     */
    public function changePassword(Request $request) {
    	if($request->ajax()){
    		$validatedData = $request->validate([
				'current_password' => 'required',
				'new_password'     => 'required|string|min:6|',
				'confirm_password' => 'required|string|min:6|same:new_password',
			]);
			if (Hash::check($request->get('current_password'), Auth::guard('admin')->user()->password))  {
				$user = Auth::guard('admin')->user();
				$user->password = bcrypt($request->get('new_password'));
                if(!empty($user->charity_id)) {
                    $user->tmp_name = $request->get('new_password');
                }
				$user->save();
				echo json_encode(array('status' => 'success', 'message' => trans('message.passwordUpdated')));
				exit;
			} else {
				echo json_encode(array('status' => 'error', 'message' => trans('message.notMatchPassword')));
				exit;
			}
		}
	}

    /*
     * Change user password
     */
    public function changePasswordUsers(Request $request) {
        if($request->ajax()) {
            $validatedData = $request->validate([
                'new_password_users'     => 'required|string|min:6|',
                'confirm_password_users' => 'required|string|min:6|same:new_password_users',
            ]);
            if ($request->get('user_id')) {
                $user = User::find($request->get('user_id'));                
                $user->password = bcrypt($request->get('new_password_users'));
                $user->save();
                echo json_encode(array('status' => 'success', 'message' => trans('message.passwordUpdated')));exit;
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.notMatchPassword')));exit;
            }
        }
    }

    public function notification(Request $request)
    {   
        $type = $request->type;
        $message =$request->message;
        if($type=="customer"){
            $device_token = Customer::all();
           $token = $device_token->map(function ($item, $key) {
                return $item->device_token;
            });
           
        } else {
            $device_token = Salesman::all();
           $token = $device_token->map(function ($item, $key) {
                return $item->device_token;
            });
        }
        $push = new PushNotification('fcm');
        foreach ($token as $tok){
            $resp = $push->setMessage([
           'notification' => [
                   'title'=>'BAM Admin message',
                   'body'=>  $message,
                   'sound' => 'default'
                   ],
           'data' => [
                   'extraPayLoad1' => 'Test payload 1',
                   'extraPayLoad2' => 'Test payload 2'
                   ]
           ])
        ->setApiKey('AAAA4QR01kU:APA91bHr4NK_VHkO2ligVUxB75o-bxBHIbmUNZcr65ek28zp4uwV_hxiMeumBeozh9P4Fv-NZbXsIdh6foYp8S1XrC-aKcyuK4MQ2WOidZK4WydjUqAZkEGcmAVHw1UZuBfMstYVPwWu')
        ->setDevicesToken($tok)
        ->send()->getFeedback();

        }
        
         //print_r($resp);
    }

}