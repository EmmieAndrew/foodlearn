<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use DB;
use Route;
use Auth;

class CharityUserController extends AdminController
{
	
	public function index() 
    {
        return view('admin.charityusers.index');
    }

    public function table_data(Request $request) {
    	$charity_id = Auth::user()->charity_id;
    	$auctions = DB::table('auctions')->join('currencies', 'currency_id', '=', 'currencies.id')->select('auctions.*', 'currencies.name as currency_name')->whereRaw("FIND_IN_SET($charity_id, charities)")->orderBy('created_at', 'DESC')->get();
        return Datatables::of($auctions)
        ->editColumn('title',function($auction) {
            $html =  '<a target="_blank" href="'.route('bidSupport', ['auctionSlug' => $auction->slug]).'">'.$auction->title.'</a>';
            return $html;
        })
        ->editColumn('auctionID',function($auction) {
            $html =  (!empty($auction->auctionid))?$auction->auctionid:'-';
            return $html;
        })
        ->editColumn('status',function($auction) {
            $html = '-';
            if(strtotime($auction->end_on) < strtotime(date('Y-m-d H:i:s'))) {// Closed
                $title = '<div class="tooltipDiv"><p><b>Commencement Date:</b> <span>'.auctionDate($auction->commenced_on).'</span></p><p><b>End Date: </b> <span>'.auctionDate($auction->end_on).'</span></p></div>';
                $html = '<a data-title-head="Closed" href="javascript:void(0)" class="statusRow">Closed on '.auctionDate($auction->end_on).'</a><div class="tooltipInfo" style="display:none;">'.$title.'</div>';
            } elseif(strtotime($auction->commenced_on) > strtotime(date('Y-m-d H:i:s'))) {// Upcoming
                $title = '<div class="tooltipDiv"><p><b>Commencement Date:</b> <span>'.auctionDate($auction->commenced_on).'</span></p><p><b>End Date: </b> <span>'.auctionDate($auction->end_on).'</span></p></div>';
                $html = '<a data-title-head="Closed" href="javascript:void(0)" class="statusRow">Commencing on '.auctionDate($auction->commenced_on).'</a><div class="tooltipInfo" style="display:none;">'.$title.'</div>';
            } else {// Live
                $title = '<div class="tooltipDiv"><p><b>Commencement Date:</b> <span>'.auctionDate($auction->commenced_on).'</span></p><p><b>Ending on:</b> <span>'.auctionDate($auction->end_on).'</span></p></div>';
                $html = '<a data-title-head="Live" href="javascript:void(0)" class="statusRow">Live</a><div class="tooltipInfo" style="display:none;">'.$title.'</div>';
            }
            return $html;
        })
        ->editColumn('minBid',function($auction) {
            $html = getCurrencySymbol($auction->currency_name, 1).formatNumber(getAuctionMinimumBid($auction->id));
            return $html;
        })
        ->editColumn('highestBid',function($auction) {
            $html = getCurrencySymbol($auction->currency_name, 1).formatNumber(getAuctionHighestBid($auction->id));// getAuctionHighestBid Only
            return $html;
        })
        ->editColumn('proceeds',function($auction) {
            $html = 'Auctionee: '.(($auction->auction_proceed > 0)?$auction->auction_proceed.'%':'-').'<br>Charity(ies): '.(($auction->charity_proceed > 0)?$auction->charity_proceed .'%':'-');
            return $html;
        })
        ->editColumn('charities',function($auction) {
            $html = '-';
            if(!empty($auction->charities)) {
                $html = '';
                $charityArr = explode(',', $auction->charities);
                $len = count($charityArr);
                foreach($charityArr as $i => $charityID) {
                	$charity = getCharityData($charityID);
                	if(!empty($charity)) {
                		if(($i == $len - 1) && !($i == 0)) {
                			$html .= ' and ';
                		}
                		if(!($i == 0) && !($i == $len - 1)) {
                			$html .= ', ';
                		}
                		$html .= $charity->title;
                	}
                }
            }
            return $html;
        })
        ->editColumn('amount',function($auction) {
            $html = '-';
            if(strtotime($auction->end_on) < strtotime(date('Y-m-d H:i:s'))) {// Closed
                $highestBid = getAuctionHighestBidOnly($auction->id);// getAuctionHighestBidOnly
                $charityCount = 1;
                if(!empty($auction->charities)) {
                    $charityArr = explode(',', $auction->charities);
                    $charityCount = count($charityArr);
                }
                $charityProceed = (!empty($auction->charity_proceed))?$auction->charity_proceed:0;
                $amount = ($highestBid*($charityProceed/100))/$charityCount;
                $html = getCurrencySymbol($auction->currency_name, 1).formatNumber($amount);
            }
            return $html;
        })
        ->rawColumns(['title', 'status', 'proceeds'])
        ->make(true);
    }

}