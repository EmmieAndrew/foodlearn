<?php

namespace App\Http\Controllers\Admin;

Use App\Models\PromoCode;
Use DB;
Use Route;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use File;
use Illuminate\Support\Facades\Input;

class PromoCodeController extends AdminController
{
     public function __construct()
    {
        $model = new PromoCode();
        $this->tableName =  $model->table;
        $this->ModuleName = 'PromoCode';
    }

    public function index()
    {
       return view('admin.promoCodes.index');
    }



    public function table_data(Request $request){
        $promoCodes  = PromoCode::select('*');
        $datatables = Datatables::of($promoCodes)

            //$dateRange = explode(" - ",$promoCode->date_range);

             ->editColumn('promo',function ($promoCode){
               $html = $promoCode->promo;
               return $html;
            })
             ->editColumn('amount',function ($promoCode){
               $html = number_format($promoCode->amount,2, ".", ",");
               return $html;
            })
             ->editColumn('type',function ($promoCode){
                if($promoCode->type == 0){
                    $html = 'Amount';
                }else{
                    $html = 'Percentage';
                }
               return $html;
            })
             
           ->editColumn('status',function ($promoCode){
                $html = '<label class="switch" id="changeStatus" module-id="'.$promoCode->id.'">';
                if($promoCode->status){
                    $html .= '<input type="checkbox" checked="checked">';
                } else {
                    $html .= '<input type="checkbox">';
                }
                $html .= '<span class="lever round"></span></label>';
                return $html;
            })
            ->addColumn('action',function ($promoCode){
                $html='<a class="btn btn-primary" value="'.$promoCode->id.'" href="'.route('promoCodes.edit',$promoCode->id).'"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        <button class="btn btn-danger btn-delete delete-records" value="'.$promoCode->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
                return $html;
            })
            ->editColumn('start_date',function($promoCode)
            {   
                $dateRange = explode(" - ",$promoCode->date_range);
                $html = formatDate($dateRange[0]);
                return $html;
            })
            ->editColumn('end_date',function($promoCode)
            {   
                $dateRange = explode(" - ",$promoCode->date_range);
                $html = formatDate($dateRange[1]);
                return $html;
            })
         
            ->rawColumns(['promo','amount','type','start_date','end_date','status', 'action']);
            return $datatables->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request){

        $app = app();
        $amtType = array('0'=>'Amount', '1'=>'Percentage');
        $promoCode = $app->make('stdClass');
        $promoCode->id = -1;
        $promoCode->promo = '';
        $promoCode->amount = '';
        $promoCode->type = '';
        $promoCode->date_range = '';
        //$promoCode->code = 'GT'.random_number(8);
        $promoCode->status=1;
       
        return view('admin.promoCodes.edit')->with('heading','Create')->with('promoCode',$promoCode)->with('amtType', $amtType);
    }



     /*
     * Change Status
     */
    public function changeStatus(Request $request) {
        if($request->ajax()) {
            $id   = $request->get('id');
            if($id && is_numeric(($id))) {
                if(PromoCode::where('id', $id)->update(array('status' => $request->get('status')))) {
                    echo json_encode(array('status' => 'success', 'message' => trans('message.statusUpdated')));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
                }
            }
            echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
        }
    }

    /*
     * Edit Record
     */
    public function edit($promoCode_id) {
        if(!empty($promoCode_id) && is_numeric($promoCode_id)) {
            $value = PromoCode::find($promoCode_id);
            $amtType = array('0'=>'Amount', '1'=>'Percentage');
            $id = $value['id'];
            if( $id == $promoCode_id ) {
               $promoCode = DB::table($this->tableName)->select('*')->where('id',$promoCode_id)->first();
             return view('admin.promoCodes.edit')->with('promoCode',$promoCode)->with('heading','Edit')->with('amtType', $amtType);
            } else {
                return redirect()->route('promoCodes.index')->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('promoCodes.index')->with('error',trans('message.invalidId'));
        }
    }

    /*
     * Update Record
     */
    public function update(Request $request,$promoCode_id) {
        if($promoCode_id>0)
            {
                request()->validate([
                    'amount'         => 'required',
                    'promo'        => 'required'
                ]);
                $data['amount']       = $request->get('amount');
                $data['promo']        = $request->get('promo');
                $data['type']       = $request->get('type');
                $data['date_range']       = $request->get('date_range');
                $data['status']       = $request->get('status');
                    if(PromoCode::where('id', $promoCode_id)->update($data)) {
                        return redirect()->route('promoCodes.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
                    } else {
                        return redirect()->route('promoCodes.index')->with('error', $this->ModuleName.trans('message.networkErr'));
                    }
                }else {
                //create new promo.
                $v = Validator::make($request->all(), [
                'promo'         => 'required'      
                ]);
                if ($v->fails()) {
                    return back()->with('errors', $v->errors())->withInput();
                }else{

        
                    $request = $request->input();
                    PromoCode::create($request);
                    return redirect()->route('promoCodes.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg')); 
                }     
            }
         }
        /*
        * Delete record
        */
        public function destroy($id) {
            // print_r($id);die();
        PromoCode::destroy($id);
        return response()->json(['status'=> 'success', 'message' => $this->ModuleName.trans('message.DeletedMsg')]);
        }   
  

        /*
         * Check code existing
         */

        // public function checkCode() {
        //     $userid = Input::get('userid');
        //     $code = Input::get('code');
        //     if($code != "") {
        //         if($userid == '') {
        //             $checkExist = ReferralCode::where('code', '=', $code)->first();

        //         } else {
        //             $checkExist = ReferralCode::where([['code', '=', $code], ['id', '!=', $userid]])->first();
                    
        //         }
        //         if ($checkExist === null) {
        //             echo "true";
        //         } else {
        //             echo "false";
        //         }
        //     }
        // }



}
