<?php

namespace App\Http\Controllers\Admin;

use App\Customer;
use App\Models\Salesman;
use DB;
use Route;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use File;
use Illuminate\Support\Facades\Input;
use App\Admin;
use App\Models\Order;

class CustomersController extends AdminController
{
    public function __construct()
    {
        $model = new Customer();
        $this->tableName = $model->table;
        $this->ModuleName = 'Customer';
    }

    public function index()
    {
        return view('admin.customers.index');
    }

    public function table_data()
    {

        $customers = Salesman::select('*')->get();
        $cutter = Salesman::where('type','2')->get();
        //dd($cutter);
        $datatables = Datatables::of($cutter)
        ->editColumn('image_name',function($cutter){
            $html = '<div class="serviceImg" style="width:35px;height:35px">';
            if($cutter->image_name){
                $html .= '<a href="'.asset('uploads/cutter/'.$cutter->image_name).'"
                data-lightbox="image-'.$cutter->id.'" data-title=""><img src="'.asset('uploads/cutter/'.$cutter->image_name).'"></a>';
                } else {
                    $html .= '<a href="'.asset('/images/common/default-image.jpg').'" data-lightbox="image-'.$cutter->id.'" data-title=""><img src="'.asset('images/common/default-image.jpg').'"></a>';
                }
                $html .= '</div>';
                return $html;
        })
        ->editColumn('title',function ($cutter){
              $html = str_limit($cutter->title,20,'....');
              return $html;
        })
        
        // ->editColumn('type',function ($cutter){
        //       $html = str_limit($cutter->teamtype->typename,20,'....');
        //       return $html;
        // })
        ->editColumn('status',function ($cutter){
            $html = '<label class="switch" id="changeStatus" module-id="'.$cutter->id.'">';
            if($cutter->status){
                $html .= '<input type="checkbox" checked="checked">';
            } else {
                $html .= '<input type="checkbox">';
            }
            $html .= '<span class="lever round"></span></label>';
            return $html;
        })
        ->addColumn('action',function ($cutter){
            $html='<a class="btn btn-primary" value="'.$cutter->id.'" href="'.route('cutters.edit',$cutter->id).'"><i class="fa fa-pencil" aria-hidden="true"></i></a>'.
            '<button class="btn btn-danger btn-delete delete-records" title="Delete" value="'.$cutter->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
            return $html;
        })
        ->editColumn('created_at',function($cutter)
        {
            $html = formatDate($cutter->created_at);
            return $html;
        })
        ->rawColumns(['image_name','title','description','status', 'action']);
        return $datatables->make(true);
    }

    /*
     * For Add
     */
    public function create(Request $request)
    {
        $app = app();
        $cutter = $app->make('stdClass');
        $cutter->id = -1;
        $cutter->image_name = '';
        $cutter->email = '';
        $cutter->title = '';
        $cutter->description = '';
        $cutter->status=1;
        $cutter->phone = '';
        $cutter->type = '2';

        return view('admin.customers.edit', compact('customer'))->with('heading', 'Add Customer');
    }

    /*
     * Get customer for Edit
     */
    public function edit($customer_id)
    {
        if (!empty($customer_id) && is_numeric($customer_id)) {
            $customer = Customer::find($customer_id);
            if (!empty($customer)) {

                return view('admin.customers.edit', compact('customer'))->with('heading', 'Edit Customer');
            } else {
                return redirect()->route('customers.index')->with('error', trans('message.invalidId'));
            }
        } else {
            return redirect()->route('customers.index')->with('error', trans('message.invalidId'));
        }
    }

    /*
     * Get customer Details
     */
    public function customerProfile($customerID)
    {
        if (!empty($customerID) && is_numeric($customerID)) {
            $customer = Customer::select('*')->where('id', $customerID)->first();
            if (!empty($customer)) {
                $last_orderdate = Order::where('customer_id',$customerID)
                    ->orderBy('id', 'DESC')->first();
                $Get_Total = Order::where('customer_id',$customer->id)
                    ->groupBy('order_unique_id')->get();
                $total_orderses = $Get_Total->count();
                return view('admin.customers.customerProfile', compact('customer','last_orderdate','total_orderses'))->with('heading', 'Customer Details');
            } else {
                return redirect()->route('customers.index')->with('error', "Customer does exist.");
            }
        } else {
            return redirect()->route('customers.index')->with('error', "Customer does exist.");
        }
    }

    /*
     * Update customer
     */
    public function update(Request $request, $customer_id)
    {
        if ($customer_id > 0) { // For Edit
            request()->validate([
                // 'name' => 'required',
                // 'email' => 'required|email|unique:customers,email,'. $customer_id .'',
                // 'phone' => 'nullable|numeric',
                // 'status' => 'required',

            ]);

            $request = $request->all();


            unset($request['_token']);
            unset($request['_method']);

            // try {
            Customer::where('id', $customer_id)->update($request);
            return redirect()->route('customers.index')->with('success', 'Updated successfully');
            // } catch (\Exception $ex) {
            //     return redirect()->route('customers.index')->with('error',trans('message.networkErr'));
            // }
        } else { // For Add
            $v = Validator::make($request->all(), [
                //    'name' => 'required',
                // 'email' => 'required|email|unique:customers,email',
                // 'password' => 'required|min:6',
                // 'confirmed_password' => 'required|same:password',
                // 'phone' => 'nullable|numeric',
                // 'status' => 'required',

            ]);

            $request = $request->input();

            $request['password'] = bcrypt($request['password']);

            try {
                Customer::create($request);
                return redirect()->route('customers.index')->with('success', 'Added Successfully');
            } catch (\Exception $ex) {
                return redirect()->route('customers.index')->with('error', trans('message.networkErr'));
            }
        }
    }

    /*
     * Change Status
     */
    public function changeStatus(Request $request)
    {
        $customer_id   = $request->get('customer_id');
        if ($customer_id && is_numeric(($customer_id))) {
            if (Customer::where('id', $customer_id)->update(array('status' => $request->get('status')))) {
                echo json_encode(array('status' => 'success', 'message' => 'Status changed'));
                exit;
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));
                exit;
            }
        }
        echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));
        exit;
    }

    /*
	 * Check email existing
     */
    public function checkEmail()
    {
        $customerid = Input::get('customerid');
        $email = Input::get('email');
        if ($email != "") {
            if ($customerid == '') {
                $checkExist = Customer::where('email', '=', $email)->first();
            } else {
                $checkExist = Customer::where([['email', '=', $email], ['id', '!=', $customerid]])->first();
            }
            if ($checkExist === null) {
                echo "true";
            } else {
                echo "false";
            }
        }
    }

    /*
     * Delete customer
     */
    public function delete(Request $request)
    {
        $id   = $request->get('customer_id');
        // $this->removeProfileImage($id);
        // Customer::destroy($id);
        Customer::where('id', $id)->update(array('status' => '0', 'deleted' => 1));
        return response()->json(['status' => 'success', 'message' => 'Deleted successfully']);
    }

    /*
     * Create admin
     */    
    public function createAdmin(){

        return view('admin.register');
    }

    /*
     * Add admin
     */    
    public function addAdmin(Request $request){

        request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'nullable|numeric|required',
            'password' => 'required',
            'confirm_password' => 'required|same:password',

        ]);
        $input = $request->except('confirm_password');
        $input['password']= bcrypt($input['password']);

        $user = Admin::create($input);

        return redirect(route('admin.dashboard'))->with('success', 'Create successfully');
    }
}
