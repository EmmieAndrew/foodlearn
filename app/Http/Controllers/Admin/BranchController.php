<?php

namespace App\Http\Controllers\Admin;


Use DB;
Use Route;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use File;
use Illuminate\Support\Facades\Input;
use App\Models\Branch;
use App\Models\Salesman;
use App\Models\OtherUsers;
Use App\Models\Factory;

class BranchController extends AdminController
{
    public function __construct()
    {
        $model = new Branch();
        $this->tableName = $model->table;
        $this->ModuleName = 'Branch';
    }

    public function index() {
        $branch = Branch::with('orderWithSale')->get();
        //dd($branch);
        //orderWithSale
        return view('admin.branches.index');
    }

    public function table_data() {
        
        $branches = Branch::select('*')->get();
        return Datatables::of($branches)
        
        ->editColumn('status',function ($branch){
            $html = '<label class="switch">';
            if($branch->status){
                $html .= '<input class="status-branch" onchange="changeBranchStatus('.$branch->id.', 0)" type="checkbox" checked="checked">';
            } else {
                $html .= '<input class="status-branch" onchange="changeBranchStatus('.$branch->id.', 1)" type="checkbox">';
            }
            $html .= '<span class="lever round"></span></label>';
            return $html;
        })
        
        ->addColumn('action',function ($branch){
            $html='<a class="btn btn-primary" title="Edit" href="'.route('branches.edit',$branch->id).'"><i class="fa fa-pencil" aria-hidden="true"></i></a>'.
                 
            
                // '<a target="_blank" class="btn btn-primary" title="View Details" href="'.route('admin.branchProfile', ["branchID" => $branch->id]).'"><i class="fa fa-eye" aria-hidden="true"></i></a>'.
                '<button class="btn btn-danger btn-delete delete-branch" title="Delete" value="'.$branch->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>'
                ;
            return $html;
        })
        ->editColumn('factory',function($branch){
            $getfactory = Factory::where('id',$branch->factory)->first();
            $html = str_limit($getfactory['name'],20,'...');;
            return $html;
        })
        ->editColumn('manager',function($branch){
            $getManager = Salesman::where('id',$branch->salesman_id)->first();
            if($getManager){
                $html = $getManager->title;
            }else{
                $html = '--';
            }
            return $html;
        })
        ->editColumn('created_at',function($branch){
            $html = formatDate($branch->created_at);
            return $html;
        })
        ->editColumn('target',function($branch){
            $html = 'SAR '.number_format($branch->target,2, ".", ",");
            return $html;
        })
        ->addColumn('achieve',function ($branch){
            $totalSale = 0;
            foreach ($branch->orderWithSale as $order) {
                //dd($order->total_price);
                $total = $order->total_price;
                $totalSale += $total;
            }
            $html= 'SAR '.number_format($totalSale,2, ".", ",");
            return $html;
        })        
        ->rawColumns(['status', 'action'])
        ->make(true);
    }

    /*
     * For Add
     */
    public function create(Request $request) {
        $app = app();

        $salesman = Salesman::where('type',3)->pluck('title','id');

        $branch = $app->make('stdClass');
        $branch->id = -1;
        $getFactory = Factory::where('status','1')->pluck('name','id');
        $branch->branch = $branch->branch_details = $branch->factory =$branch->target = $branch->salesman_id = $branch->phone = '';
        $branch->status = '1';

        return view('admin.branches.edit', compact('branch','getFactory','salesman'))
                ->with('heading', 'Add branch');
    }

    /*
     * Get branch for Edit
     */
    public function edit($branch_id) {
        if(!empty($branch_id) && is_numeric($branch_id)) {
            $branch = Branch::find($branch_id);

            $selectedSalesman = Salesman::where('id',$branch->salesman_id)->first();
            $salesman = Salesman::where('type',3)->pluck('title','id');
            if($selectedSalesman){
                $salesman->put($selectedSalesman->id,$selectedSalesman->title.' (Current Manager)');
            }

            if( !empty($branch) ) {

                $getFactory = Factory::pluck('name','id');

                return view('admin.branches.edit', compact('branch','getFactory', 'salesman'))->with('heading', 'Edit branch');
            } else {
                return redirect()->route('branches.index')->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('branches.index')->with('error',trans('message.invalidId'));
        }
    }

    /*
     * Get branch Details
     */
    public function branchProfile($branchID) {
        if(!empty($branchID) && is_numeric($branchID)) {
            $branch = Branch::select('*')->where('id', $branchID)->first();
            if( !empty($branch) ) {
                $getFactory = Factory::where('id',$branch->factory)->first();
                $getSalesman = Salesman::where('type','3')->where('id',$branch->salesman)->first();
                $getDriver = Salesman::where('type','7')->where('id',$branch->driver)->first();
                $getCutter = Salesman::where('type','4')->where('id',$branch->cutter)->first();
                return view('admin.branches.branchProfile', compact('branch','getFactory','getSalesman','getDriver','getCutter'))->with('heading', 'branch Details');
            } else {
                return redirect()->route('branches.index')->with('error', "branch does exist.");
            }
        } else {
            return redirect()->route('branches.index')->with('error', "branch does exist.");
        }
    }

    /*
     * Update branch
     */
    public function update(Request $request, $branch_id) {
        if($branch_id > 0) {// For Edit
            request()->validate([
                // 'name' => 'required',
                // 'email' => 'required|email|unique:branches,email,'. $branch_id .'',
                // 'phone' => 'nullable|numeric',
                // 'status' => 'required',
                
            ]);
            
            $request = $request->all();
            
          
            unset($request['_token']);
            unset($request['_method']);

            $branch = Branch::find($branch_id);
            
            //check old manager and new manager id
            if($branch->salesman_id != $request['salesman_id']){
                Salesman::where('id', $branch->salesman_id)->update(['type'=>'3']);
            }
            try {
                Branch::where('id', $branch_id)->update($request);
                Salesman::where('id', $request['salesman_id'])->update(['type'=>'7']);

                return redirect()->route('branches.index')->with('success', 'Updated successfully');
            } catch (\Exception $ex) {
                return redirect()->route('branches.index')->with('error',trans('message.networkErr'));
            }
        } else {// For Add
            $v = Validator::make($request->all(), [
             //    'name' => 'required',
                // 'email' => 'required|email|unique:branches,email',
                // 'password' => 'required|min:6',
                // 'confirmed_password' => 'required|same:password',
                // 'phone' => 'nullable|numeric',
                // 'status' => 'required',
                
            ]);
            
            $request = $request->input();


            $branch = Branch::orderBy('id', 'DESC')->get()->first();
            if($branch){
                $branchID = sprintf("%02d", $branch->id+1);
            }else{
                $branchID = sprintf("%02d", 1);
            }
            $request['branch_id'] = 'BAM'.$branchID;

            try {
                Branch::create($request);
                Salesman::where('id', $request['salesman_id'])->update(['type'=>'7']);

                return redirect()->route('branches.index')->with('success', 'Added Successfully');
            } catch (\Exception $ex) {
                return redirect()->route('branches.index')->with('error',trans('message.networkErr'));
            }
        }
    }

    /*
     * Change Status
     */
    public function changeStatus(Request $request) {
        $branch_id   = $request->get('branch_id');
        if($branch_id && is_numeric(($branch_id))) {
            if(Branch::where('id', $branch_id)->update(array('status' => $request->get('status')))) {
                echo json_encode(array('status' => 'success', 'message' => 'Status changed'));exit;
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
            }
        }
        echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
    }

    /*
     * Check email existing
     */
    public function checkEmail() {
        $branchid = Input::get('branchid');
        $email = Input::get('email');
        if($email != "") {
            if($branchid == '') {
                $checkExist = Branch::where('email', '=', $email)->first();
            } else {
                $checkExist = Branch::where([['email', '=', $email], ['id', '!=', $branchid]])->first();
            }
            if ($checkExist === null) {
                echo "true";
            } else {
                echo "false";
            }
        }
    }

    /*
     * Delete branch
     */
    public function delete(Request $request) {
        $id   = $request->get('branch_id');
        Branch::where('id', $id)->delete();
        return response()->json(['status'=> 'success', 'message' => 'Deleted successfully']);
    }

   

    

}
