<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use Illuminate\Support\Facades\Validator;
use App\Models\Salesman;
use App\Models\Salary;
use App\Models\OtherUsers;
use App\Models\Payroll;
use App\Models\Branch;
use App\Models\Teamtype;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;
use PDF;

class PayrollController extends AdminController
{
    
    public function __construct()
    {
        $model = new Payroll();
        $this->tableName =  $model->table;
        $this->ModuleName = 'payroll';
    }

    public function index()
    {
        $payroll = Payroll::get();
        $payroll = [];
        foreach (range(1, 12) as $m) {
            $payroll[date('m', mktime(0, 0, 0, $m, 1))] = date('F', mktime(0, 0, 0, $m, 1));
        }

        return view('admin.payroll.index')->with('payroll',$payroll);  
    }

    public function table_data(Request $request)
    {
        // if(!$request->payYear){
        //     $currentYear = date('m');
        // }else{
        //     $currentYear = $request->payYear;
        // }
        
        // if(!$request->payMonth){
        //     $currentMonth = date('Y');
        // }else{
        //     $currentMonth = $request->payMonth;
        // }

        if($request->payYear && $request->payMonth){
            $payroll = Payroll::whereMonth('created_at', $request->payMonth)->whereYear('created_at', $request->payYear)->get();
        }else{
            $payroll = Payroll::whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))->get();
        }
        //dd($payroll);
        $datatables = Datatables::of($payroll)

        ->addColumn('action',function ($payroll){

            if($payroll->generate_payroll == 'Yes'){

                $html='<a class="btn btn-primary" value="'.$payroll->id.'" href="'.route('payroll.download',$payroll->id).'" title="Download Pay Slip"><i class="fa fa-download" aria-hidden="true"></i></a>';
            }else{

                $html='<a class="btn btn-primary" value="'.$payroll->id.'" href="'.route('payroll.edit',$payroll->id).'" style="margin:0px"><i class="fa fa-pencil" aria-hidden="true"></i></a>'.
                '<button class="btn btn-danger btn-delete delete-records" title="Delete" value="'.$payroll->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>'.
                '<a class="btn btn-primary" value="'.$payroll->id.'" href="'.route('payroll.generate',$payroll->id).'" style="margin:0px"  title="Generate Pay Slip"><i class="fa fa-file-text" aria-hidden="true"></i></a>';
            }
            return $html;
        })
        ->editColumn('created_at',function($payroll)
        {
            $html = formatDate($payroll->created_at);
            return $html;
        })
        ->editColumn('salary',function($payroll){
            $html = number_format($payroll->salary,2, ".", ",");
            return $html;
        })
        ->editColumn('other_deduction',function($payroll){
            $html = number_format($payroll->other_deduction,2, ".", ",");
            return $html;
        })
        ->editColumn('commission',function($payroll){
            $html = number_format($payroll->commission,2, ".", ",");
            return $html;
        })
        ->editColumn('total',function($payroll){
            $html = number_format($payroll->total,2, ".", ",");
            return $html;
        })
        ->editColumn('updated_at',function($payroll)
        {
            $html = formatDate($payroll->updated_at);
            return $html;
        })
        ->rawColumns(['image_name','title','description','status', 'action']);
        return $datatables->make(true);
    }
    /**
     * Create salesman
     */
    public function create()
    {
        $app = app();
        $payroll = $app->make('stdClass');
        $payroll->id = -1;
        $payroll->emp_id = '';
        $payroll->name = '';
        $payroll->salary = '';
        $payroll->assigned_hours = '';
        $payroll->no_defective_order = '';
        $payroll->acheived_hours = '';
        $payroll->other_deduction = '';
        $payroll->commission = '';
        $payroll->total = '';
        $payroll->evaluation = '';
        $payroll->note = '';

        $getEmpId = Salary::pluck('emp_id','emp_id');
        // $otherId = OtherUsers::pluck('emp_id','emp_id');
        // foreach ($otherId as $value) {
        //     $getEmpId[$value] = $value;
        // }

        return view('admin.payroll.edit')->with('heading','Create')->with('payroll',$payroll)->with('getEmpId', $getEmpId);
    }
    /*
    * Change Status
    */
    public function changeStatus(Request $request) {
        if($request->ajax()) {
            $id   = $request->get('id');
            if($id && is_numeric(($id))) {
                if(Payroll::where('id', $id)->update(array('status' => $request->get('status')))) {
                    echo json_encode(array('status' => 'success', 'message' => 'updated successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
                }
            }
            echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
        }
    }


    /**
     Edit Record
     */
    public function edit($payroll_id) {
        if(!empty($payroll_id) && is_numeric($payroll_id)) {
            $value = Payroll::find($payroll_id);
            $getEmpId = Salary::pluck('emp_id','emp_id');

            $id = $value['id'];
            if( $id == $payroll_id ) {

               $payroll = DB::table($this->tableName)->select('*')->where('id',$payroll_id)->first();
             return view('admin.payroll.edit')->with('payroll',$payroll)->with('getEmpId',$getEmpId)->with('heading','Edit');

            } else {
                return redirect()->route('payroll.index')->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('payroll.index')->with('error',trans('message.invalidId'));
        }
    }
    /*
     * Update Record
     */
    public function update(Request $request,$payroll_id) {
        if($payroll_id>0)
            {
                request()->validate([
                    'emp_id'=> 'required',
                    'name'=> 'required',
                    'salary'=> 'required',
                    'assigned_hours'=> 'required',
                    'acheived_hours'=> 'required',
                    'evaluation'=> 'required',
                    'note'=> 'required',
                    'other_deduction'=> 'required',
                    'no_defective_order'=> 'required',
                    'commission'=> 'required',
                    'total'=> 'required'
                 ]);

                $data = array();

                $data['emp_id'] = $request->get('emp_id');
                $data['name'] = $request->get('name');
                $data['salary'] = $request->get('salary');
                $data['assigned_hours']  = $request->get('assigned_hours');
                $data['acheived_hours']       = $request->get('actual_hours');
                $data['evaluation'] = $request->get('evaluation');
                $data['note'] = $request->get('note');
                $data['other_deduction'] = $request->get('other_deduction');
                $data['no_defective_order'] = $request->get('no_defective_order');
                $data['commission'] = $request->get('commission');
                $data['total'] = $request->get('total');


                    if(Payroll::where('id', $payroll_id)->update($data)) {
                        return redirect()->route('payroll.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
                    } else {
                        return redirect()->route('payroll.index')->with('error', $this->ModuleName.trans('message.networkErr'));
                    }
                }else { //for Create
                    $v = Validator::make($request->all(), [
                    'emp_id'=> 'required',
                    'name'=> 'required',
                    'salary'=> 'required',
                    'assigned_hours'=> 'required',
                    'actual_hours'=> 'required',
                    ]);
                    $data = array();
                    $request = $request->input();

                    Payroll::create($request);
                    // Update employee defective order when create payroll
                    $employee = Salesman::where('emp_id',$request['emp_id'])->first();
                    if(isset($employee)){
                        Salesman::where('id', $employee->id)->update(['defective_order' => 0]);
                    }

                    if(!$employee){
                        $othEmployee = OtherUsers::where('emp_id',$request['emp_id'])->first();
                        if(isset($othEmployee)){
                            OtherUsers::where('id', $othEmployee->id)->update(['defective_order' => 0]);
                        }

                    }
                    //End defective order

                return redirect()->route('payroll.index')->with('Success', 'Created Successfully');      
            }
        }

     /*
     * Delete Record
     */
    public function destroy(Request $request,$id){
        if($request->ajax()) {
            if($id && is_numeric($id)) {
                if(Payroll::destroy($id)) {
                    echo json_encode(array('status' => 'success', 'message' => 'Deleted successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => 'Deleted successfully'));exit;
                }
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
            }
        }
     

    }
    /*
     * Show Record
     */
    public function show()
    {
        return redirect()->route('payroll.index')->with('error',trans('message.invalidId'));
    }

    public function delete(Request $request) {
        $id   = $request->get('product_id');
       // $this->removeProfileImage($id);
       // Customer::destroy($id);
        $product = Product::where('id', $id)->first();
        $product->delete();
        return response()->json(['status'=> 'success', 'message' => 'Deleted successfully']);
    }

    public function ajaxEmpData() {
        $input = request()->all();
        //dd($input['emp_id']);
        $employee = Salesman::where('emp_id',$input['emp_id'])->with('salary')->first();
        if(isset($employee)){
            $name = $employee->title;
            $defectiveOrder = $employee->defective_order;
        }

        if(!$employee){
            $employee = OtherUsers::where('emp_id',$input['emp_id'])->with('salary')->first();
            $name = $employee->name;
            $defectiveOrder = $employee->defective_order;
        }
        $assign_target = $employee->monthly_target;
        if($employee->salary){
            $salary = $employee->salary->salary;
        }else{
            $salary = 0;
        }
        return response()->json(['name' => $name, 'assign_target' => $assign_target, 'salary' => $salary, 'defectiveOrder' => $defectiveOrder]);
    }

    public function generate($id){
        $payroll = Payroll::find($id);
        if($payroll){
            if(Payroll::where('id', $payroll->id)->update(['generate_payroll' => 'Yes'])) {
                    return redirect()->route('payroll.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
                } else {
                    return redirect()->route('payroll.index')->with('error', $this->ModuleName.trans('message.networkErr'));
                }
        }
        return redirect()->route('payroll.index')->with('error', $this->ModuleName.trans('message.networkErr'));
    }

    public function downloadPaySlip($id){

        $data = Payroll::find($id);
        //dd($data);

        //$pdf = PDF::loadView('admin.payroll.payslip_pdf', $data);  
       // return $pdf->download('medium.pdf');
        return view('admin.payroll.payslip_pdf')->with('data', $data);

    }
}
