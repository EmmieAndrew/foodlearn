<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use Illuminate\Support\Facades\Validator;
use App\Models\Salesman;
use App\Models\OtherUsers;
use App\Models\Salary;
use App\Models\Branch;
use App\Models\Teamtype;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;

class SalaryController extends AdminController
{
    
    public function __construct()
    {
        $model = new Salary();
        $this->tableName =  $model->table;
        $this->ModuleName = 'salary';
    }

    public function index()
    {
        return view('admin.salary.index');  
    }

    public function table_data(Request $request)
    {
        $salary = Salary::get()->all();

        $datatables = Datatables::of($salary)

        ->editColumn('status',function ($salary){
            $html = '<label class="switch" id="changeStatus" module-id="'.$salary->id.'">';
            if($salary->status){
                $html .= '<input type="checkbox" checked="checked">';
            } else {
                $html .= '<input type="checkbox">';
            }
            $html .= '<span class="lever round"></span></label>';
            return $html;
        })
        ->addColumn('action',function ($salary){
            $html='<a class="btn btn-primary" value="'.$salary->id.'" href="'.route('salary.edit',$salary->id).'"><i class="fa fa-pencil" aria-hidden="true"></i></a>'.
            '<button class="btn btn-danger btn-delete delete-records" title="Delete" value="'.$salary->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
            return $html;
        })
        ->editColumn('created_at',function($salary)
        {
            $html = formatDate($salary->created_at);
            return $html;
        })
        ->editColumn('salary',function($salary)
        {
            $html = number_format($salary->salary,2, ".", ",");
            return $html;
        })
        ->editColumn('name',function($salary)
        {
            $empName = $salary->empDetails;
            $otherEmpName = $salary->otherEmpDetails;
            if($empName){
                $html = $empName->title;
            }else{
                if($otherEmpName){
                    $html = $otherEmpName->name;
                }else{
                    $html = '--';
                }
            }
            return $html;
        })
        ->editColumn('role',function($salary)
        {
            $empRole = $salary->empDetails;
            $otherEmpRole = $salary->otherEmpDetails;
            if($empRole){
                $html = $empRole->teamtype->typename;
            }else{
                if($otherEmpRole){
                    $html = $otherEmpRole->teamtype->typename;
                }else{
                    $html = '--';
                }
            }
            return $html;
        })
        ->rawColumns(['emp_id','name','role','salary','created_at','status', 'action']);
        return $datatables->make(true);
    }
    /**
     * Create salary
     */
    public function create()
    {

        $app = app();

        $getAllSalesman = Salesman::with('teamtype')->get()->all();
        $getAllOtherUser = OtherUsers::with('teamtype')->get()->all();

        //dd($getAll);
        $getSalary = Salary::get()->all();
        $getAllId = array();

        //Get all salesman and check in already in salary or not
        foreach ($getAllSalesman as $value) {
            if(isset($getSalary)){
                foreach ($getSalary as $salData) {
                    $getAllId[$value->emp_id] = $value->emp_id.' || '. $value->title.' ('.$value->teamtype->typename.')';
                    unset($getAllId[$salData->emp_id]);
                }
            }
        }

        //Get all Other Users and check in already in salary or not
        foreach ($getAllOtherUser as $value) {
            if(isset($getSalary)){
                foreach ($getSalary as $salData) {
                    $getAllId[$value->emp_id] = $value->emp_id.' || '. $value->title.' ('.$value->teamtype->typename.')';
                    unset($getAllId[$salData->emp_id]);
                }
            }
        }

        $salary = $app->make('stdClass');
        $salary->id = -1;
        //$salary->name = '';
        $salary->emp_id = '';
        $salary->salary = '';
        $salary->status=1;
        return view('admin.salary.edit')->with('heading','Create')->with('salary',$salary)->with('getAllId',$getAllId);
    }
    /*
    * Change Status
    */
    public function changeStatus(Request $request) {
        if($request->ajax()) {
            $id   = $request->get('id');
            if($id && is_numeric(($id))) {
                if(Salary::where('id', $id)->update(array('status' => $request->get('status')))) {
                    echo json_encode(array('status' => 'success', 'message' => 'updated successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
                }
            }
            echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
        }
    }


    /**
     Edit Record
     */
    public function edit($salary_id) {
        if(!empty($salary_id) && is_numeric($salary_id)) {
            $value = Salary::find($salary_id);

            $id = $value['id'];
            $getAll = Salesman::with('teamtype')->get()->all();
            $getSalary = Salary::where('id', '!=', $value->id)->get()->all();
            $getAllId = array();
            foreach ($getAll as $value) {
                if(isset($getSalary)){
                    foreach ($getSalary as $salData) {
                        $getAllId[$value->emp_id] = $value->emp_id.' || '. $value->title.' ('.$value->teamtype->typename.')';
                        unset($getAllId[$salData->emp_id]);
                    }
                }

            }

            if( $id == $salary_id ) {
               $salary = DB::table($this->tableName)->select('*')->where('id',$salary_id)->first();
             return view('admin.salary.edit')->with('salary',$salary)->with('getAllId',$getAllId)->with('heading','Edit');
            } else {
                return redirect()->route('salary.index')->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('salary.index')->with('error',trans('message.invalidId'));
        }
    }
    /*
     * Update Record
     */
    public function update(Request $request,$salary_id) {
        if($salary_id>0)
            {
                request()->validate([
                    //'name'         => 'required'
                 ]);
                $data = array();
                //$data['emp_id'] = $request->get('emp_id');
                //$data['name'] = $request->get('name');
                $data['salary'] = $request->get('salary');

                    if(Salary::where('id', $salary_id)->update($data)) {
                        return redirect()->route('salary.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
                    } else {
                        return redirect()->route('salary.index')->with('error', $this->ModuleName.trans('message.networkErr'));
                    }
                }else { //for Create
                    $v = Validator::make($request->all(), [
                    //'name'         => 'required',
                    //'emp_id'         =>'required',
                  
                    ]);

                $request = $request->input();

                // $lastEmp = Salary::orderBy('id', 'desc')->first();                
                // if($lastEmp){
                //     $empID = sprintf("%04d", $lastEmp->id+1);
                // }else{
                //     $empID = sprintf("%04d", 1);
                // }
                //$request['emp_id'] = $empID;

                Salary::create($request);

                return redirect()->route('salary.index')->with('Success', 'Created Successfully');      
            }
        }

     /*
     * Delete Record
     */
    public function destroy(Request $request,$id){
        if($request->ajax()) {
            if($id && is_numeric($id)) {
                // $destinationPath = public_path('/uploads/salary');
                // $deleteImg = Salary::where('id', $id)->first()->image_name;
                if(Salary::destroy($id)) {
                    // if(isset($deleteImg) && !empty($deleteImg)) {
                    //     File::delete($destinationPath.'/'.$deleteImg);
                    // }
                    echo json_encode(array('status' => 'success', 'message' => 'Deleted successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => 'Deleted successfully'));exit;
                }
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
            }
        }
     

    }
    /*
     * Show Record
     */
    public function show()
    {
        return redirect()->route('salary.index')->with('error',trans('message.invalidId'));
    }

    public function delete(Request $request) {
        dd("sss");
        $id   = $request->get('product_id');
       // $this->removeProfileImage($id);
       // Customer::destroy($id);
        $product = Product::where('id', $id)->first();
        $product->delete();
        return response()->json(['status'=> 'success', 'message' => 'Deleted successfully']);
    }
}
