<?php

namespace App\Http\Controllers\Admin;

Use App\Models\Product;
Use DB;
Use Route;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use File;
use Session;
use Illuminate\Support\Facades\Input;

class CataloguesController extends AdminController
{
    public function __construct()
    {
        $model = new Product();
        $this->tableName = $model->table;
        $this->ModuleName = 'Product';
    }

    public function inventories() {
        return view('admin.catalogues.dashboard');
    }

    public function home(Request $request) {
        $value = $request->id;
        Session::put('type', $value);
        return view('admin.catalogues.index');
    }

    ///*disable index url here in this controller*///
	/*public function index() {
        return view('admin.catalogues.index');
    }*/

    public function table_data() {
        $type = Session::get('type');
        $products = Product::where('product_type',$type)->select('*')->get();
        return Datatables::of($products)
        ->editColumn('product_image',function($products){
            $html = '<div class="serviceImg" style="width:35px;height:35px">';
            if($products->product_image){
                $html .= '<a href="'.asset('uploads/catalogues/'.$products->product_image).'"
                data-lightbox="image-'.$products->id.'" data-title=""><img src="'.asset('uploads/catalogues/'.$products->product_image).'"></a>';
                } else {
                    $html .= '<a href="'.asset('/images/common/default-image.jpg').'" data-lightbox="image-'.$products->id.'" data-title=""><img src="'.asset('images/common/default-image.jpg').'"></a>';
                }
                $html .= '</div>';
                return $html;
        })
        ->addColumn('action',function ($products){
            $html='<a class="btn btn-primary" title="Edit" value="'.$products->id.'" href="'.route('catalogues.edit',$products->id).'">
            <i class="fa fa-pencil" aria-hidden="true"></i></a>
            <a target="_blank" class="btn btn-primary" title="View Product" href="'.route('catalogues.productDetails', ["orderId" => $products->id]).'">
            <i class="fa fa-eye" aria-hidden="true"></i></a>';
            return $html;
        })
        
        ->editColumn('product_price',function($products){
            $html = number_format($products->product_price,2, ".", ",");
            return $html;
        })
        ->editColumn('created_at',function($products){
            $html = formatDate($products->created_at);
            return $html;
        })
        ->editColumn('updated_at',function($products){
            $html = formatDate($products->updated_at);
            return $html;
        })
        ->rawColumns(['product_image','product_code','product_name','product_color','product_description','product_length','product_price','selling_option','action'])
        ->make(true);
    }


    /*
     * For Add
     */
    public function create(Request $request) {
        $app = app();
        $products = $app->make('stdClass');
        $products->id = -1;
        $products->product_name = $products->product_code = $products->product_color  = $products->product_description = $products->product_length = $products->product_price = $products->product_image = $products->selling_option = '';
        return view('admin.catalogues.add', compact('products'))->with('heading', 'Add Catalogue');
    }

    /*
     * Get customer for Edit
     */
    public function edit($product_id) {
        if(!empty($product_id) && is_numeric($product_id)) {
            $type = Session::get('type');
            $products = Product::find($product_id);
            if( !empty($products) ) {
                return view('admin.catalogues.edit', compact('products'))->with('heading', 'Edit Catalogue');;
            } else {
                return redirect()->route('catalogues.home', ['id' => $type])->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('catalogues.home', ['id' => $type])->with('error',trans('message.invalidId'));
        }
    }

    /*
     * Update customer
     */
    public function update(Request $request,$product_id) {
        if($product_id>0){
            $type = Session::get('type');
            $data = array();
            $data['selling_option'] = $request->get('selling_option');
            $data['product_length'] = $request->get('product_length');
            $data['product_price'] = $request->get('product_price');
            // dd($data);
            if(Product::where('id', $product_id)->update($data)) {
                return redirect()->route('catalogues.home', ['id' => $type])->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
            } else {
                return redirect()->route('catalogues.home', ['id' => $type])->with('error', $this->ModuleName.trans('message.networkErr'));
            }
        }
        else { //for Create
            $v = Validator::make($request->all(), [
                'product_name' => 'required',
                'product_code' => 'required',
                'product_color' => 'required',
                'product_length' => 'required',
                'product_price' => 'required',
                'selling_option' => 'required',
                ]);
                $data = array();
                $allowedMimeTypes = array(
                    'image/jpeg',
                    'image/png',
                    'image/gif',
                    'image/svg',
                    'image/svg+xml',
                    'image/bmp'
                );
                $imageName = NULL;
                if($request->hasFile('product_image')) {
                    $mimetype = $request->file('product_image')->getClientMimeType();
                    if(!in_array($mimetype, $allowedMimeTypes)){
                        return redirect()->route('catalogues.edit')->with('success', 'Please select a valid image type');
                        exit;
                    } else{
                        $image = $request->file('product_image');
                        $imageName = time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/uploads/catalogues');
                        $image->move($destinationPath, $imageName);
                    }
                }
                $request['product_image'] = $imageName;
                $type = Session::get('type');
                $request['product_type'] = $type;
                $request = $request->input();
                Product::create($request);
                return redirect()->route('catalogues.home', ['id' => $type])->with('Success', 'Created Successfully');      
            }
        }
    
       public function destroy(Request $request,$id){
        if($request->ajax()) {
            if($id && is_numeric($id)) {
                $destinationPath = public_path('/uploads/catalogues');
                $deleteImg = Product::where('id', $id)->first()->image_name;
                if(Product::destroy($id)) {
                    if(isset($deleteImg) && !empty($deleteImg)) {
                        File::delete($destinationPath.'/'.$deleteImg);
                    }
                    echo json_encode(array('status' => 'success', 'message' => 'Deleted successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => 'Deleted successfully'));exit;
                }
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
            }
        }
     

    }

    public function productDetails($orderId)
    {
        // dd($orderId);
        if(!empty($orderId)) {
            $order = Product::where('id', $orderId)->first();
            if( !empty($order) ) {
                return view('admin.catalogues.details', compact('order','order'))->with('heading', 'Inventory Details');
            } else {
                return redirect()->route('orders.index')->with('error', "Inventory does exist.");
            }
        } else {
            return redirect()->route('orders.index')->with('error', "orders does exist.");
        }
    }
    /*
     * Show Record
     */
    public function show()
    {
        return redirect()->route('catalogues.index')->with('error',trans('message.invalidId'));
    }

    public function delete(Request $request) {
        $id = $request->get('product_id');
       // $this->removeProfileImage($id);
       // Customer::destroy($id);
        $product = Product::where('id', $id)->first();
        $product->delete();
        return response()->json(['status'=> 'success', 'message' => 'Deleted successfully']);
    }

}