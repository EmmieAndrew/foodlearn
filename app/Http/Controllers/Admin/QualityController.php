<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use Illuminate\Support\Facades\Validator;
use App\Models\Salesman;
use App\Models\Payroll;
use App\Models\Salary;
use App\Models\Teamtype;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;

class QualityController extends AdminController
{
    
    public function __construct()
    {
        $model = new Salesman();
        $this->tableName =  $model->table;
        $this->ModuleName = 'Salesman';
    }

    public function index()
    {
        return view('admin.quality.index');  
    }

    public function table_data(Request $request)
    {
        $quality = Salesman::with('teamtype')
        ->where('type','5')
        ->orWhere('type','6')
        ->get();
        //dd($cutter);
        $datatables = Datatables::of($quality)
        ->editColumn('image_name',function($quality){
            $html = '<div class="serviceImg" style="width:35px;height:35px">';
            if($quality->image_name){
                $html .= '<a href="'.asset('uploads/quality/'.$quality->image_name).'"
                data-lightbox="image-'.$quality->id.'" data-title=""><img src="'.asset('uploads/quality/'.$quality->image_name).'"></a>';
                } else {
                    $html .= '<a href="'.asset('/images/common/default-image.jpg').'" data-lightbox="image-'.$quality->id.'" data-title=""><img src="'.asset('images/common/default-image.jpg').'"></a>';
                }
                $html .= '</div>';
                return $html;
        })
        ->editColumn('title',function ($quality){
              $html = str_limit($quality->title,20,'....');
              return $html;
        })
        // ->editColumn('type',function ($cutter){
        //       $html = str_limit($cutter->teamtype->typename,20,'....');
        //       return $html;
        // })
        ->editColumn('status',function ($quality){
            $html = '<label class="switch" id="changeStatus" module-id="'.$quality->id.'">';
            if($quality->status){
                $html .= '<input type="checkbox" checked="checked">';
            } else {
                $html .= '<input type="checkbox">';
            }
            $html .= '<span class="lever round"></span></label>';
            return $html;
        })
        ->addColumn('action',function ($quality){
            $html='<a class="btn btn-primary" value="'.$quality->id.'" href="'.route('quality.edit',$quality->id).'"><i class="fa fa-pencil" aria-hidden="true"></i></a>'.
            '<button class="btn btn-danger btn-delete delete-records" title="Delete" value="'.$quality->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
            return $html;
        })
        ->editColumn('created_at',function($quality)
        {
            $html = formatDate($quality->created_at);
            return $html;
        })
        ->editColumn('factory_manager',function($quality)
        {
            $manager = Salesman::where('id',$quality->factory_manager)->first();
            if($manager){
                $html = $manager->title;
            }else{
                $html = '--';
            }
            return $html;
        })
        ->editColumn('evaluation',function ($quality){

            $allEvaluation = Payroll::where('emp_id',$quality->emp_id)->get();
            $totalEvaluation =0;

            if($allEvaluation->isNotEmpty()){
                $count = $allEvaluation->count();
                
                foreach ($allEvaluation as $evaluation) {
                   $totalEvaluation += $evaluation->evaluation;
                }

                $html = $totalEvaluation/$count;
            }else{
               $html = $totalEvaluation; 
            }

              return $html;
        })
        ->rawColumns(['image_name','title','description','status', 'action']);
        return $datatables->make(true);
    }
    /**
     * Create quality
     */
    public function create()
    {
        $app = app();
        $factory_assign_manager = Salesman::where('type',6)
        // ->where('factory_id','!=',0)
        ->where('status',1)
        ->pluck('title','id');
        $quality = $app->make('stdClass');
        $quality->id = -1;
        $quality->image_name = '';
        $quality->email = '';
        $quality->title = '';
        $quality->description = '';
        $quality->status=1;
        $quality->phone = '';
        $quality->factory_manager = '';
        $quality->monthly_target = '';
        $quality->type = '';

        return view('admin.quality.edit')->with('heading','Create')->with('quality',$quality)->with('factory_assign_manager', $factory_assign_manager);
    }
    /*
    * Change Status
    */
    public function changeStatus(Request $request) {
        if($request->ajax()) {
            $id   = $request->get('id');
            if($id && is_numeric(($id))) {
                if(Salesman::where('id', $id)->update(array('status' => $request->get('status')))) {
                    echo json_encode(array('status' => 'success', 'message' => 'updated successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
                }
            }
            echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
        }
    }


    /**
     Edit Record
     */
    public function edit($quality_id) {
        if(!empty($quality_id) && is_numeric($quality_id)) {
            $value = Salesman::find($quality_id);
            //$userType = Teamtype::where('id','3')->orwhere('id','5')->pluck('typename','id');
            $factory_assign_manager = Salesman::where('type',6)->where('factory_id','!=',0)->pluck('title','id');
            
            $id = $value['id'];
            if( $id == $quality_id ) {
               $quality = DB::table($this->tableName)->select('*')->where('id',$quality_id)->first();
             return view('admin.quality.edit')->with('quality',$quality)->with('heading','Edit')->with('factory_assign_manager', $factory_assign_manager);
             //->with('userType',$userType);
            } else {
                return redirect()->route('quality.index')->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('quality.index')->with('error',trans('message.invalidId'));
        }
    }
    /*
     * Update Record
     */
    public function update(Request $request,$quality_id) {

        if($quality_id>0)
            {
                request()->validate([
                    'title'         => 'required'
                 ]);
                $data = array();
                $allowedMimeTypes = array(
                    'image/jpeg',
                    'image/png',
                    'image/gif',
                    'image/svg',
                    'image/svg+xml',
                    'image/bmp'
                );
                if($request->hasFile('image_name')) {
                    $mimetype = $request->file('image_name')->getClientMimeType();
                    if(!in_array($mimetype, $allowedMimeTypes)){
                        return redirect()->route('quality.index')->with('error', $this->ModuleName.trans('message.imgTypeError'));
                    } else{
                        $image = $request->file('image_name');
                        $data['image_name'] = time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/uploads/quality');
                        $checkPrevious = Salesman::where('id', $quality_id)->first()->image_name;
                        if(isset($checkPrevious) && !empty($checkPrevious)) {
                            File::delete($destinationPath.'/'.$checkPrevious);
                        }
                        $image->move($destinationPath, $data['image_name']);
                    }
                }
                $data['title'] = $request->get('title');
                $data['email'] = $request->get('email');
                $data['phone'] = $request->get('phone');
                $data['description']  = $request->get('description');
                $data['status']       = $request->get('status');
                $data['factory_manager'] = $request->get('factory_manager');
                $data['monthly_target'] = $request->get('monthly_target');

                    if(Salesman::where('id', $quality_id)->update($data)) {
                        return redirect()->route('quality.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
                    } else {
                        return redirect()->route('quality.index')->with('error', $this->ModuleName.trans('message.networkErr'));
                    }
            }else { //for Create
                $v = Validator::make($request->all(), [
                'title'         => 'required',
                'email'         =>'required|email',
                'phone'         =>'required|numeric',
              
                ]);
                $data = array();
                $allowedMimeTypes = array(
                    'image/jpeg',
                    'image/png',
                    'image/gif',
                    'image/svg',
                    'image/svg+xml',
                    'image/bmp'
                );
                $imageName = NULL;
                if($request->hasFile('image_name')) {
                    $mimetype = $request->file('image_name')->getClientMimeType();
                    if(!in_array($mimetype, $allowedMimeTypes)){
                        return redirect()->route('quality.edit')->with('success', 'Please select a valid image type');
                        exit;
                        
                    } else{
                        $image = $request->file('image_name');
                        $imageName = time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/uploads/quality');
                        $image->move($destinationPath, $imageName);
                    }
                }
            $request['image_name'] = $imageName;
            $request = $request->input();             

            //Create dynamic emp_id
            $lastEmp = Salary::orderBy('id', 'desc')->first();                
            if($lastEmp){
                $empID = sprintf("%04d", $lastEmp->id+1);
            }else{
                $empID = sprintf("%04d", 1);
            }
            $salaryRequest['emp_id'] = $empID;
            //$salaryRequest['name'] = $request['title'];
            $request['emp_id'] = $empID;

                            
            //Add user type.
            $request['type'] = 5;

            //create password
            $passwordcreate = mt_rand(1000, 9999);
            $request['password'] = \Hash::make($passwordcreate);

            Salesman::create($request);
            Salary::create($salaryRequest);

            //Welcome mail
            $user['email']= $request['email'];
            $user['Dpassword'] = $passwordcreate;
            Mail::to($request['email'])->send(new WelcomeMail($user));
            // End welcome mail

            return redirect()->route('quality.index')->with('Success', 'Created Successfully');      
        }
    }

     /*
     * Delete Record
     */
    public function destroy(Request $request,$id){
        if($request->ajax()) {
            if($id && is_numeric($id)) {
                $destinationPath = public_path('/uploads/quality');
                $deleteImg = Salesman::where('id', $id)->first()->image_name;
                if(Salesman::destroy($id)) {
                    if(isset($deleteImg) && !empty($deleteImg)) {
                        File::delete($destinationPath.'/'.$deleteImg);
                    }
                    echo json_encode(array('status' => 'success', 'message' => 'Deleted successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => 'Deleted successfully'));exit;
                }
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
            }
        }
     

    }
    /*
     * Show Record
     */
    public function show()
    {
        return redirect()->route('quality.index')->with('error',trans('message.invalidId'));
    }

    public function delete(Request $request) {
        $id   = $request->get('product_id');
       // $this->removeProfileImage($id);
       // Customer::destroy($id);
        $product = Product::where('id', $id)->first();
        $product->delete();
        return response()->json(['status'=> 'success', 'message' => 'Deleted successfully']);
    }
}
