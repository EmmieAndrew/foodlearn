<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use Illuminate\Support\Facades\Validator;
use App\Models\OtherUsers;
use App\Models\Payroll;
use App\Models\Teamtype;
use App\Models\Salesman;
use App\Models\Salary;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;

class OtherUsersController extends AdminController
{
    
    public function __construct()
    {
        $model = new OtherUsers();
        $this->tableName =  $model->table;
        $this->ModuleName = 'OtherUsers';
    }

    public function index()
    {
        return view('admin.otherUsers.index');  
    }

    public function table_data(Request $request)
    {
        //$salesmans = OtherUsers::Select('*');
        $OtherUsers = OtherUsers::with('teamtype')
                                ->where('type','1')->orwhere('type','2')
                                ->get();
                        //dd('$salesmans');
        $datatables = Datatables::of($OtherUsers)
        ->editColumn('image_name',function($OtherUsers){
            $html = '<div class="serviceImg" style="width:35px;height:35px">';
            if($OtherUsers->image_name){
                $html .= '<a href="'.asset('uploads/other/'.$OtherUsers->image_name).'"
                data-lightbox="image-'.$OtherUsers->id.'" data-title=""><img src="'.asset('uploads/other/'.$OtherUsers->image_name).'"></a>';
                } else {
                    $html .= '<a href="'.asset('/images/common/default-image.jpg').'" data-lightbox="image-'.$OtherUsers->id.'" data-title=""><img src="'.asset('images/common/default-image.jpg').'"></a>';
                }
                $html .= '</div>';
                return $html;
        })
        ->editColumn('name',function ($OtherUsers){
              $html = str_limit($OtherUsers->name,20,'....');
              return $html;
        })
        ->editColumn('type',function ($OtherUsers){
              $html = str_limit($OtherUsers->teamtype->typename,20,'....');
              return $html;
        })
        ->editColumn('evaluation',function ($OtherUsers){

            $allEvaluation = Payroll::where('emp_id',$OtherUsers->emp_id)->get();
            $totalEvaluation =0;

            if($allEvaluation->isNotEmpty()){
                $count = $allEvaluation->count();
                
                foreach ($allEvaluation as $evaluation) {
                   $totalEvaluation += $evaluation->evaluation;
                }

                $html = $totalEvaluation/$count;
            }else{
               $html = $totalEvaluation; 
            }

              return $html;
        })
        ->editColumn('status',function ($OtherUsers){
            $html = '<label class="switch" id="changeStatus" module-id="'.$OtherUsers->id.'">';
            if($OtherUsers->status){
                $html .= '<input type="checkbox" checked="checked">';
            } else {
                $html .= '<input type="checkbox">';
            }
            $html .= '<span class="lever round"></span></label>';
            return $html;
        })
        ->addColumn('action',function ($OtherUsers){
            $html='<a class="btn btn-primary" value="'.$OtherUsers->id.'" href="'.route('otherUsers.edit',$OtherUsers->id).'"><i class="fa fa-pencil" aria-hidden="true"></i></a>'.
            '<button class="btn btn-danger btn-delete delete-records" title="Delete" value="'.$OtherUsers->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
            return $html;
        })
        ->editColumn('created_at',function($OtherUsers)
        {
            $html = formatDate($OtherUsers->created_at);
            return $html;
        })
        ->editColumn('factory_manager',function($OtherUsers)
        {
            $manager = Salesman::where('id',$OtherUsers->factory_manager)->first();
            if($manager){
                $html = $manager->title;
            }else{
                $html = '--';
            }
            return $html;
        })
        ->rawColumns(['image_name','name','status', 'action']);
        return $datatables->make(true);
    }
    /**
     * Create OtherUsers
     */
    public function create()
    {
        $userType = Teamtype::
                    where('id','1')
                    ->orwhere('id','2')
                    ->pluck('typename','id');
        $app = app();
        $factory_assign_manager = Salesman::where('type',6)
        // ->where('factory_id','!=',0)
        ->where('status',1)
        ->pluck('title','id');        
        $OtherUsers = $app->make('stdClass');
        $OtherUsers->id = -1;
        $OtherUsers->image_name = '';
        $OtherUsers->email = '';
        $OtherUsers->name = '';
        $OtherUsers->status=1;
        $OtherUsers->phone = '';
        $OtherUsers->monthly_target = '';
        $OtherUsers->factory_manager = '';
        $OtherUsers->type = '';
        $OtherUsers->igama_renewal_date = '';
        return view('admin.otherUsers.edit')->with('heading','Create')->with('OtherUsers',$OtherUsers)->with('userType',$userType)->with('factory_assign_manager',$factory_assign_manager);
    }
    /*
    * Change Status
    */
    public function changeStatus(Request $request) {
        if($request->ajax()) {
            $id   = $request->get('id');
            if($id && is_numeric(($id))) {
                if(OtherUsers::where('id', $id)->update(array('status' => $request->get('status')))) {
                    echo json_encode(array('status' => 'success', 'message' => 'updated successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
                }
            }
            echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
        }
    }


    /**
     Edit Record
     */
    public function edit($other_id) {
        if(!empty($other_id) && is_numeric($other_id)) {
            $value = OtherUsers::find($other_id);
            $userType = Teamtype::where('id','1')
            ->orwhere('id','2')->pluck('typename','id');
            
            $factory_assign_manager = Salesman::where('type',6)->where('factory_id','!=',0)->pluck('title','id');

            $id = $value['id'];
            if( $id == $other_id ) {
               $OtherUsers = DB::table($this->tableName)->select('*')->where('id',$other_id)->first();
             return view('admin.otherUsers.edit')->with('OtherUsers',$OtherUsers)->with('heading','Edit')->with('userType',$userType)->with('factory_assign_manager',$factory_assign_manager);
            } else {
                return redirect()->route('otherUsers.index')->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('otherUsers.index')->with('error',trans('message.invalidId'));
        }
    }
    /*
     * Update Record
     */
    public function update(Request $request,$other_id) {
        if($other_id>0)
            {
                request()->validate([
                    'name'         => 'required'
                 ]);
                $data = array();
                $allowedMimeTypes = array(
                    'image/jpeg',
                    'image/png',
                    'image/gif',
                    'image/svg',
                    'image/svg+xml',
                    'image/bmp'
                );
                if($request->hasFile('image_name')) {
                    $mimetype = $request->file('image_name')->getClientMimeType();
                    if(!in_array($mimetype, $allowedMimeTypes)){
                        return redirect()->route('otherUsers.index')->with('error', $this->ModuleName.trans('message.imgTypeError'));
                    } else{
                        $image = $request->file('image_name');
                        $data['image_name'] = time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/uploads/salesmans');
                        $checkPrevious = OtherUsers::where('id', $other_id)->first()->image_name;
                        if(isset($checkPrevious) && !empty($checkPrevious)) {
                            File::delete($destinationPath.'/'.$checkPrevious);
                        }
                        $image->move($destinationPath, $data['image_name']);
                    }
                }
                $data['name'] = $request->get('name');
                $data['email'] = $request->get('email');
                $data['phone'] = $request->get('phone');
                $data['status']       = $request->get('status');
                $data['monthly_target'] = $request->get('monthly_target');
                $data['type'] = $request->get('type');
                $data['factory_manager'] = $request->get('factory_manager');
                $data['igama_renewal_date'] = $request->get('igama_renewal_date');
                    if(OtherUsers::where('id', $other_id)->update($data)) {
                        return redirect()->route('otherUsers.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
                    } else {
                        return redirect()->route('otherUsers.index')->with('error', $this->ModuleName.trans('message.networkErr'));
                    }
                }else { //for Create
                    $v = Validator::make($request->all(), [
                    'name'         => 'required',
                    'email'         =>'required|email',
                    'phone'         =>'required|numeric',
                  
                    ]);
                    $data = array();
                    $allowedMimeTypes = array(
                        'image/jpeg',
                        'image/png',
                        'image/gif',
                        'image/svg',
                        'image/svg+xml',
                        'image/bmp'
                    );
                    $imageName = NULL;
                    if($request->hasFile('image_name')) {
                        $mimetype = $request->file('image_name')->getClientMimeType();
                        if(!in_array($mimetype, $allowedMimeTypes)){
                            return redirect()->route('otherUsers.edit')->with('success', 'Please select a valid image type');
                            exit;
                            
                        } else{
                            $image = $request->file('image_name');
                            $imageName = time().'.'.$image->getClientOriginalExtension();
                            $destinationPath = public_path('/uploads/other');
                            $image->move($destinationPath, $imageName);
                        }
                    }
                $request['image_name'] = $imageName;
                $request = $request->input();

                //create password
                // $passwordcreate = mt_rand(1000, 9999);
                // $request['password'] = \Hash::make($passwordcreate);             

                //Create dynamic emp_id
                $lastEmp = Salary::orderBy('id', 'desc')->first();                
                if($lastEmp){
                    $empID = sprintf("%04d", $lastEmp->id+1);
                }else{
                    $empID = sprintf("%04d", 1);
                }
                $salaryRequest['emp_id'] = $empID;
                //$salaryRequest['name'] = $request['name'];
                $request['emp_id'] = $empID;

                OtherUsers::create($request);
                Salary::create($salaryRequest);

                //Welcome mail
                // $user['email']= $request['email'];
                // $user['Dpassword'] = $passwordcreate;
                // Mail::to($request['email'])->send(new WelcomeMail($user));
                // End welcome mail

                return redirect()->route('otherUsers.index')->with('Success', 'Created Successfully');      
            }
        }

     /*
     * Delete Record
     */
    public function destroy(Request $request,$id){
        if($request->ajax()) {
            if($id && is_numeric($id)) {
                $destinationPath = public_path('/uploads/salesmans');
                $deleteImg = OtherUsers::where('id', $id)->first()->image_name;
                if(OtherUsers::destroy($id)) {
                    if(isset($deleteImg) && !empty($deleteImg)) {
                        File::delete($destinationPath.'/'.$deleteImg);
                    }
                    echo json_encode(array('status' => 'success', 'message' => 'Deleted successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => 'Deleted successfully'));exit;
                }
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
            }
        }
     

    }
    /*
     * Show Record
     */
    public function show()
    {
        return redirect()->route('salesmans.index')->with('error',trans('message.invalidId'));
    }

    public function delete(Request $request) {
        $id   = $request->get('product_id');
       // $this->removeProfileImage($id);
       // Customer::destroy($id);
        $product = Product::where('id', $id)->first();
        $product->delete();
        return response()->json(['status'=> 'success', 'message' => 'Deleted successfully']);
    }
}
