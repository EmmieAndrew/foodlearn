<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
Use App\Models\Salesman;
Use App\Models\Payroll;
Use App\Models\Salary;
Use App\Models\Factory;
Use App\Models\OtherUsers;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use App\Models\Teamtype;
use Illuminate\Support\Facades\Validator;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;

class TailorController extends AdminController
{
       
    public function __construct()
    {
        $model = new Salesman();
        $this->tableName =  $model->table;
        $this->ModuleName = 'Salesman';
    }

    public function index()
    {
        return view('admin.tailor.index');  
    }

    public function table_data(Request $request)
    {
        $tailor = Salesman::where('type',1)->get();
        $datatables = Datatables::of($tailor)
        ->editColumn('image_name',function($tailor){
            $html = '<div class="serviceImg" style="width:35px;height:35px">';
            if($tailor->image_name){
                $html .= '<a href="'.asset('uploads/tailor/'.$tailor->image_name).'"
                data-lightbox="image-'.$tailor->id.'" data-title=""><img src="'.asset('uploads/tailor/'.$tailor->image_name).'"></a>';
                } else {
                    $html .= '<a href="'.asset('/images/common/default-image.jpg').'" data-lightbox="image-'.$tailor->id.'" data-title=""><img src="'.asset('images/common/default-image.jpg').'"></a>';
                }
                $html .= '</div>';
                return $html;
        })
        ->editColumn('title',function ($tailor){
              $html = str_limit($tailor->title,20,'....');
              return $html;
        })
        ->editColumn('address',function ($tailor){
              $html = str_limit($tailor->address,20,'....');
              return $html;
        })
        ->editColumn('status',function ($tailor){
            $html = '<label class="switch" id="changeStatus" module-id="'.$tailor->id.'">';
            if($tailor->status){
                $html .= '<input type="checkbox" checked="checked">';
            } else {
                $html .= '<input type="checkbox">';
            }
            $html .= '<span class="lever round"></span></label>';
            return $html;
        })
        ->addColumn('action',function ($tailor){
            $html='<a class="btn btn-primary" value="'.$tailor->id.'" href="'.route('tailor.edit',$tailor->id).'"><i class="fa fa-pencil" aria-hidden="true"></i></a>'.'<button class="btn btn-danger btn-delete delete-records" title="Delete" value="'.$tailor->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
            return $html;
        })
        ->editColumn('created_at',function($tailor)
        {
            $html = formatDate($tailor->created_at);
            return $html;
        })
        ->rawColumns(['image_name','name','status','country','city', 'action']);
        return $datatables->make(true);
    }
    /**
     * Create tailor
     */
    public function create(Request $request)
    {
        $app = app();
        $tailor = $app->make('stdClass');
        $tailor->id = -1;
        $tailor->image_name = '';
        $tailor->title = '';
        $tailor->email = '';
        $tailor->status='1';
        $tailor->type='2';
        $tailor->phone = '';
        $tailor->country = '';
        $tailor->city = '';
        return view('admin.tailor.edit')->with('heading','Create')->with('tailor',$tailor);
        
    }
    /*
    * Change Status
    */
    public function changeStatus(Request $request) {
        if($request->ajax()) {
            $id   = $request->get('id');
            if($id && is_numeric(($id))) {
                if(Salesman::where('id', $id)->update(array('status' => $request->get('status')))) {
                    echo json_encode(array('status' => 'success', 'message' => 'updated successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
                }
            }
            echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
        }
    }


    /**
     Edit Record
     */
    public function edit($tailor_id) {
        if(!empty($tailor_id) && is_numeric($tailor_id)) {
            $value = Salesman::find($tailor_id);
            //$userType = Teamtype::where('id','6')->pluck('typename','id');
            $id = $value['id'];
            if( $id == $tailor_id ) {
               $tailor = DB::table($this->tableName)->select('*')->where('id',$tailor_id)->first();
             return view('admin.tailor.edit')->with('tailor',$tailor)->with('heading','Edit');
             //->with('userType',$userType);
            } else {
                return redirect()->route('tailor.index')->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('tailor.index')->with('error',trans('message.invalidId'));
        }
    }
    /*
     * Update Record
     */
    public function update(Request $request,$tailor_id) {
        if($tailor_id>0)
            {
                request()->validate([
                   // 'title'         => 'required'
                 ]);
                $data = array();
                $allowedMimeTypes = array(
                    'image/jpeg',
                    'image/png',
                    'image/gif',
                    'image/svg',
                    'image/svg+xml',
                    'image/bmp'
                );
                if($request->hasFile('image_name')) {
                    $mimetype = $request->file('image_name')->getClientMimeType();
                    if(!in_array($mimetype, $allowedMimeTypes)){
                        return redirect()->route('tailor.index')->with('error', $this->ModuleName.trans('message.imgTypeError'));
                    } else{
                        $image = $request->file('image_name');
                        $data['image_name'] = time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/uploads/tailor');
                        $checkPrevious = Salesman::where('id', $tailor_id)->first()->image_name;
                        if(isset($checkPrevious) && !empty($checkPrevious)) {
                            File::delete($destinationPath.'/'.$checkPrevious);
                        }
                        $image->move($destinationPath, $data['image_name']);
                    }
                }
                $data['title'] = $request->get('title');
                $data['email'] = $request->get('email');
                $data['phone'] = $request->get('phone');
                $data['status'] = $request->get('status');
                $data['country'] = $request->get('country');
                $data['city'] = $request->get('city');
                    if(Salesman::where('id', $tailor_id)->update($data)) {
                        return redirect()->route('tailor.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
                    } else {
                        return redirect()->route('tailor.index')->with('error', $this->ModuleName.trans('message.networkErr'));
                    }
                }else { //for Create
                    // dd("dd");
                    $v = Validator::make($request->all(), [
                   // 'title'         => 'required',
                  
                    ]);
                    $data = array();
                    $allowedMimeTypes = array(
                        'image/jpeg',
                        'image/png',
                        'image/gif',
                        'image/svg',
                        'image/svg+xml',
                        'image/bmp'
                    );
                    $imageName = NULL;
                    if($request->hasFile('image_name')) {
                        $mimetype = $request->file('image_name')->getClientMimeType();
                        if(!in_array($mimetype, $allowedMimeTypes)){
                            return redirect()->route('tailor.edit')->with('success', 'Please select a valid image type');
                            exit;
                            
                        } else{
                            $image = $request->file('image_name');
                            $imageName = time().'.'.$image->getClientOriginalExtension();
                            $destinationPath = public_path('/uploads/tailor');
                            $image->move($destinationPath, $imageName);
                        }
                    }
                $data['image_name'] = $imageName;
                $data['title'] = $request->get('title');
                $data['email'] = $request->get('email');
                $data['phone'] = $request->get('phone');
                $data['status'] = $request->get('status');
                $data['country'] = $request->get('country');
                $data['city'] = $request->get('city');
                // $request = $request->input();
                // dd($data);
                Salesman::create($data);

                //Welcome mail
                // $user['email']= $request['email'];
                // $user['Dpassword']= $passwordcreate;
                // Mail::to($request['email'])->send(new WelcomeMail($user));
                // End welcome mail

                return redirect()->route('tailor.index')->with('Success', 'Created Successfully');      
            }
        }

     /*
     * Delete Record
     */
    public function destroy(Request $request,$id){
        if($request->ajax()) {
            if($id && is_numeric($id)) {
                $destinationPath = public_path('/uploads/tailor');
                $deleteImg = Salesman::where('id', $id)->first()->image_name;
                if(Salesman::destroy($id)) {
                    if(isset($deleteImg) && !empty($deleteImg)) {
                        File::delete($destinationPath.'/'.$deleteImg);
                    }
                    echo json_encode(array('status' => 'success', 'message' => 'Deleted successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => 'Deleted successfully'));exit;
                }
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
            }
        }
     

    }
    /*
     * Show Record
     */
    public function show()
    {
        return redirect()->route('tailor.index')->with('error',trans('message.invalidId'));
    }


    /*
     * Get tailor Details
     */
    public function tailorProfile($tailorID) {
        if(!empty($tailorID) && is_numeric($tailorID)) {
            $tailor = Salesman::select('*')->where('id', $tailorID)->first();
            if( !empty($tailor) ) {
                return view('admin.tailor.tailorProfile', compact('tailor'))->with('heading', 'Tailor Details');
            } else {
                return redirect()->route('tailor.index')->with('error', "Tailor does exist.");
            }
        } else {
            return redirect()->route('tailor.index')->with('error', "Tailor does exist.");
        }
    }
}
