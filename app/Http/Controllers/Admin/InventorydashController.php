<?php

namespace App\Http\Controllers\Admin;

Use App\Models\Product;
Use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class InventorydashController extends AdminController
{
    public function __construct()
    {
        $model = new Product();
        $this->tableName = $model->table;
        $this->ModuleName = 'Product';
    }

	public function index() {
        return view('admin.catalogues.dashboard');
    }

}