<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use Illuminate\Support\Facades\Validator;
use App\Models\Supplier;

class SupplierController extends AdminController
{
    
    public function __construct()
    {
        $model = new Supplier();
        $this->tableName =  $model->table;
        $this->ModuleName = 'Supplier';
    }

    public function index()
    {
        return view('admin.suppliers.index');  
    }

    public function table_data(Request $request)
    {
        $suppliers = Supplier::Select('*');
        $datatables = Datatables::of($suppliers)
        ->editColumn('image_name',function($supplier){
            $html = '<div class="serviceImg" style="width:35px;height:35px">';
            if($supplier->image_name){
                $html .= '<a href="'.asset('uploads/suppliers/'.$supplier->image_name).'"
                data-lightbox="image-'.$supplier->id.'" data-title=""><img src="'.asset('uploads/suppliers/'.$supplier->image_name).'"></a>';
                } else {
                    $html .= '<a href="'.asset('/images/common/default-image.jpg').'" data-lightbox="image-'.$supplier->id.'" data-title=""><img src="'.asset('images/common/default-image.jpg').'"></a>';
                }
                $html .= '</div>';
                return $html;
        })
        ->editColumn('supplier_name',function ($supplier){
              $html = str_limit($supplier->supplier_name,20,'....');
              return $html;
        })
        ->editColumn('company_name',function ($supplier){
              $html = str_limit($supplier->company_name,20,'....');
              return $html;
        })
        ->editColumn('address',function ($supplier){
              $html = str_limit($supplier->address,20,'....');
              return $html;
        })
        ->editColumn('status',function ($supplier){
            $html = '<label class="switch" id="changeStatus" module-id="'.$supplier->id.'">';
            if($supplier->status){
                $html .= '<input type="checkbox" checked="checked">';
            } else {
                $html .= '<input type="checkbox">';
            }
            $html .= '<span class="lever round"></span></label>';
            return $html;
        })
        ->addColumn('action',function ($supplier){
            $html='<a class="btn btn-primary" value="'.$supplier->id.'" href="'.route('suppliers.edit',$supplier->id).'"><i class="fa fa-pencil" aria-hidden="true"></i></a>'.'<button class="btn btn-danger btn-delete delete-records" title="Delete" value="'.$supplier->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
            return $html;
        })
        ->editColumn('created_at',function($supplier)
        {
            $html = formatDate($supplier->created_at);
            return $html;
        })
        ->editColumn('updated_at',function($supplier)
        {
            $html = formatDate($supplier->updated_at);
            return $html;
        })
        ->rawColumns(['image_name','supplier_name','company_name','address','status', 'action']);
        return $datatables->make(true);
    }
    /**
     * Create supplier
     */
    public function create()
    {
        $app = app();
        $supplier = $app->make('stdClass');
        $supplier->id = -1;
        $supplier->image_name = '';
        $supplier->supplier_name = '';
        $supplier->company_name = '';
        $supplier->email = '';
        $supplier->address = '';
        $supplier->status=1;
        $supplier->primary_phone = '';
        $supplier->secondary_phone = '';
        return view('admin.suppliers.edit')->with('heading','Create')->with('supplier',$supplier);
    }
    /*
    * Change Status
    */
    public function changeStatus(Request $request) {
        if($request->ajax()) {
            $id   = $request->get('id');
            if($id && is_numeric(($id))) {
                if(Supplier::where('id', $id)->update(array('status' => $request->get('status')))) {
                    echo json_encode(array('status' => 'success', 'message' => 'updated successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
                }
            }
            echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
        }
    }


    /**
     Edit Record
     */
    public function edit($supplier_id) {
        if(!empty($supplier_id) && is_numeric($supplier_id)) {
            $value = Supplier::find($supplier_id);
            $id = $value['id'];
            if( $id == $supplier_id ) {
               $supplier = DB::table($this->tableName)->select('*')->where('id',$supplier_id)->first();
             return view('admin.suppliers.edit')->with('supplier',$supplier)->with('heading','Edit');
            } else {
                return redirect()->route('suppliers.index')->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('suppliers.index')->with('error',trans('message.invalidId'));
        }
    }
    /*
     * Update Record
     */
    public function update(Request $request,$supplier_id) {
        if($supplier_id>0)
            {
                request()->validate([
                    
                 ]);
                $data = array();
                $allowedMimeTypes = array(
                    'image/jpeg',
                    'image/png',
                    'image/gif',
                    'image/svg',
                    'image/svg+xml',
                    'image/bmp'
                );
                if($request->hasFile('image_name')) {
                    $mimetype = $request->file('image_name')->getClientMimeType();
                    if(!in_array($mimetype, $allowedMimeTypes)){
                        return redirect()->route('suppliers.index')->with('error', $this->ModuleName.trans('message.imgTypeError'));
                    } else{
                        $image = $request->file('image_name');
                        $data['image_name'] = time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/uploads/suppliers');
                        $checkPrevious = Supplier::where('id', $supplier_id)->first()->image_name;
                        if(isset($checkPrevious) && !empty($checkPrevious)) {
                            File::delete($destinationPath.'/'.$checkPrevious);
                        }
                        $image->move($destinationPath, $data['image_name']);
                    }
                }
                $data['supplier_name'] = $request->get('supplier_name');
                $data['company_name'] = $request->get('company_name');
                $data['email'] = $request->get('email');
                $data['primary_phone'] = $request->get('primary_phone');
                $data['secondary_phone'] = $request->get('secondary_phone');
                $data['address']  = $request->get('address');
                $data['status']       = $request->get('status');
                
                    if(Supplier::where('id', $supplier_id)->update($data)) {
                        return redirect()->route('suppliers.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
                    } else {
                        return redirect()->route('suppliers.index')->with('error', $this->ModuleName.trans('message.networkErr'));
                    }
                }else { //for Create
                    $v = Validator::make($request->all(), [
                    
                  
                    ]);
                    $data = array();
                    $allowedMimeTypes = array(
                        'image/jpeg',
                        'image/png',
                        'image/gif',
                        'image/svg',
                        'image/svg+xml',
                        'image/bmp'
                    );
                    $imageName = NULL;
                    if($request->hasFile('image_name')) {
                        $mimetype = $request->file('image_name')->getClientMimeType();
                        if(!in_array($mimetype, $allowedMimeTypes)){
                            return redirect()->route('suppliers.edit')->with('success', 'Please select a valid image type');
                            exit;
                            
                        } else{
                            $image = $request->file('image_name');
                            $imageName = time().'.'.$image->getClientOriginalExtension();
                            $destinationPath = public_path('/uploads/suppliers');
                            $image->move($destinationPath, $imageName);
                        }
                    }
                $request['image_name'] = $imageName;
                $request = $request->input();
                Supplier::create($request);
                    return redirect()->route('suppliers.index')->with('Success', 'Created Successfully');      
            }
        }

     /*
     * Delete Record
     */
    public function destroy(Request $request,$id){
        if($request->ajax()) {
            if($id && is_numeric($id)) {
                $destinationPath = public_path('/uploads/suppliers');
                $deleteImg = Supplier::where('id', $id)->first()->image_name;
                if(Supplier::destroy($id)) {
                    if(isset($deleteImg) && !empty($deleteImg)) {
                        File::delete($destinationPath.'/'.$deleteImg);
                    }
                    echo json_encode(array('status' => 'success', 'message' => 'Deleted successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => 'Deleted successfully'));exit;
                }
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
            }
        }
     

    }
    /*
     * Show Record
     */
    public function show()
    {
        return redirect()->route('suppliers.index')->with('error',trans('message.invalidId'));
    }
}
