<?php

namespace App\Http\Controllers\Admin;

Use App\Models\Product;
Use App\Models\Inventory;
Use DB;
Use Route;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use File;
use Illuminate\Support\Facades\Input;

class InventoryController extends AdminController
{
    public function __construct()
    {
        $model = new Inventory();
        $this->tableName = $model->table;
        $this->ModuleName = 'inventory';
    }

	public function index() {
        return view('admin.inventory.index');
    }

    public function table_data() {
        
        $products = Inventory::select('*')->with('product')->with('salesman')->get();
        return Datatables::of($products)
        ->editColumn('product_image',function($products){
            $products = $products->product;

            $html = '<div class="serviceImg" style="width:35px;height:35px">';
            if($products['product_image']){
                $html .= '<a href="'.asset('uploads/catalogues/'.$products['product_image']).'"
                data-lightbox="image-'.$products['id'].'" data-title=""><img src="'.asset('uploads/catalogues/'.$products['product_image']).'"></a>';
                } else {
                    $html .= '<a href="'.asset('/images/common/default-image.jpg').'" data-lightbox="image-'.$products['id'].'" data-title=""><img src="'.asset('images/common/default-image.jpg').'"></a>';
                }
                $html .= '</div>';
                return $html;
        })
        
        ->editColumn('product_name',function($products){
            $products = $products->product;

            $html = $products['product_name'];
            return $html;
        }) 

        ->editColumn('product_code',function($products){
            $products = $products['product'];

            $html = $products['product_code'];
            return $html;
        }) 

        ->editColumn('request_by',function($products){
            $products = $products['salesman'];

            if($products){
                $html = $products['title'];
            }else{
                $html = '--';
            }
            return $html;
        }) 

        ->editColumn('created_at',function($products){
            $html = formatDate($products->created_at);
            return $html;
        })
        // ->editColumn('updated_at',function($products){
        //     $html = formatDate($products->updated_at);
        //     return $html;
        // })
        ->rawColumns(['product_image','product_name','product_type','comment','request_by','created_at'])
        ->make(true);
    }


    /*
     * For Add
     */
    public function create(Request $request) {
        $app = app();
        $products = $app->make('stdClass');
        $products['id'] = -1;
        $products['product_name'] = $products->product_type = $products->product_details  = $products->product_description = $products->product_length = $products->product_price = $products['product_image'] = '';
        return view('admin.catalogues.edit', compact('products'))->with('heading', 'Add Catalogue');
    }

    /*
     * Get customer for Edit
     */
    public function edit($product_id) {
        if(!empty($product_id) && is_numeric($product_id)) {
            $products = Product::find($product_id);
            if( !empty($products) ) {
                return view('admin.catalogues.edit', compact('products'))->with('heading', 'Edit Catalogue');;
            } else {
                return redirect()->route('catalogues.index')->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('catalogues.index')->with('error',trans('message.invalidId'));
        }
    }

    /*
     * Update customer
     */
    public function update(Request $request,$product_id) {
        if($product_id>0)
            {
                request()->validate([
                    'product_name' => 'required',
                    'product_price' => 'required'
                 ]);
                $data = array();
                $allowedMimeTypes = array(
                    'image/jpeg',
                    'image/png',
                    'image/gif',
                    'image/svg',
                    'image/svg+xml',
                    'image/bmp'
                );
                if($request->hasFile('product_image')) {
                    $mimetype = $request->file('product_image')->getClientMimeType();
                    if(!in_array($mimetype, $allowedMimeTypes)){
                        return redirect()->route('catalogues.index')->with('error', $this->ModuleName.trans('message.imgTypeError'));
                    } else{
                        $image = $request->file('product_image');
                        $data['product_image'] = time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/uploads/catalogues');
                        $checkPrevious = Product::where('id', $product_id)->first()->product_image;
                        if(isset($checkPrevious) && !empty($checkPrevious)) {
                            File::delete($destinationPath.'/'.$checkPrevious);
                        }
                        $image->move($destinationPath, $data['product_image']);
                    }
                }
                $data['product_name'] = $request->get('product_name');
                $data['product_type'] = $request->get('product_type');
                $data['product_details'] = $request->get('product_details');
                $data['product_description']  = $request->get('product_description');
                $data['product_length']       = $request->get('product_length');
                $data['product_price'] = $request->get('product_price');
                    if(Product::where('id', $product_id)->update($data)) {
                        return redirect()->route('catalogues.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
                    } else {
                        return redirect()->route('catalogues.index')->with('error', $this->ModuleName.trans('message.networkErr'));
                    }
                }else { //for Create
                    $v = Validator::make($request->all(), [
                    'product_name'         => 'required',
                    'product_type'         =>'required',
                    'product_details'         =>'required',
                    'product_length'         =>'required',
                    'product_price'         =>'product_price',
                  
                    ]);
                    $data = array();
                    $allowedMimeTypes = array(
                        'image/jpeg',
                        'image/png',
                        'image/gif',
                        'image/svg',
                        'image/svg+xml',
                        'image/bmp'
                    );
                    $imageName = NULL;
                    if($request->hasFile('product_image')) {
                        $mimetype = $request->file('product_image')->getClientMimeType();
                        if(!in_array($mimetype, $allowedMimeTypes)){
                            return redirect()->route('catalogues.edit')->with('success', 'Please select a valid image type');
                            exit;
                            
                        } else{
                            $image = $request->file('product_image');
                            $imageName = time().'.'.$image->getClientOriginalExtension();
                            $destinationPath = public_path('/uploads/catalogues');
                            $image->move($destinationPath, $imageName);
                        }
                    }
                $request['product_image'] = $imageName;
                $request = $request->input();
                Product::create($request);
                    return redirect()->route('catalogues.index')->with('Success', 'Created Successfully');      
            }
        }
    
       public function destroy(Request $request,$id){
        if($request->ajax()) {
            if($id && is_numeric($id)) {
                $destinationPath = public_path('/uploads/catalogues');
                $deleteImg = Product::where('id', $id)->first()->image_name;
                if(Product::destroy($id)) {
                    if(isset($deleteImg) && !empty($deleteImg)) {
                        File::delete($destinationPath.'/'.$deleteImg);
                    }
                    echo json_encode(array('status' => 'success', 'message' => 'Deleted successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => 'Deleted successfully'));exit;
                }
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
            }
        }
     

    }
    /*
     * Show Record
     */
    public function show()
    {
        return redirect()->route('catalogues.index')->with('error',trans('message.invalidId'));
    }

    public function delete(Request $request) {
        $id = $request->get('product_id');
       // $this->removeProfileImage($id);
       // Customer::destroy($id);
        $product = Product::where('id', $id)->first();
        $product->delete();
        return response()->json(['status'=> 'success', 'message' => 'Deleted successfully']);
    }

}