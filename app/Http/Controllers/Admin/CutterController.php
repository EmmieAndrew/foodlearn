<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use Illuminate\Support\Facades\Validator;
use App\Models\Salesman;
use App\Models\Payroll;
use App\Models\Salary;
use App\Models\Teamtype;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;

class CutterController extends AdminController
{
    
    public function __construct()
    {
        $model = new Salesman();
        $this->tableName =  $model->table;
        $this->ModuleName = 'Salesman';
    }

    public function index()
    {
        return view('admin.cutter.index');  
    }

    public function table_data(Request $request)
    {
        $cutter = Salesman::where('type','2')->get();
        //dd($cutter);
        $datatables = Datatables::of($cutter)
        ->editColumn('image_name',function($cutter){
            $html = '<div class="serviceImg" style="width:35px;height:35px">';
            if($cutter->image_name){
                $html .= '<a href="'.asset('uploads/cutter/'.$cutter->image_name).'"
                data-lightbox="image-'.$cutter->id.'" data-title=""><img src="'.asset('uploads/cutter/'.$cutter->image_name).'"></a>';
                } else {
                    $html .= '<a href="'.asset('/images/common/default-image.jpg').'" data-lightbox="image-'.$cutter->id.'" data-title=""><img src="'.asset('images/common/default-image.jpg').'"></a>';
                }
                $html .= '</div>';
                return $html;
        })
        ->editColumn('title',function ($cutter){
              $html = str_limit($cutter->title,20,'....');
              return $html;
        })
        ->editColumn('evaluation',function ($cutter){

            $allEvaluation = Payroll::where('emp_id',$cutter->emp_id)->get();
            $totalEvaluation =0;

            if($allEvaluation->isNotEmpty()){
                $count = $allEvaluation->count();
                
                foreach ($allEvaluation as $evaluation) {
                   $totalEvaluation += $evaluation->evaluation;
                }

                $html = $totalEvaluation/$count;
            }else{
               $html = $totalEvaluation; 
            }

              return $html;
        })
        
        // ->editColumn('type',function ($cutter){
        //       $html = str_limit($cutter->teamtype->typename,20,'....');
        //       return $html;
        // })
        ->editColumn('status',function ($cutter){
            $html = '<label class="switch" id="changeStatus" module-id="'.$cutter->id.'">';
            if($cutter->status){
                $html .= '<input type="checkbox" checked="checked">';
            } else {
                $html .= '<input type="checkbox">';
            }
            $html .= '<span class="lever round"></span></label>';
            return $html;
        })
        ->addColumn('action',function ($cutter){
            $html='<a class="btn btn-primary" value="'.$cutter->id.'" href="'.route('cutters.edit',$cutter->id).'"><i class="fa fa-pencil" aria-hidden="true"></i></a>'.
            '<button class="btn btn-danger btn-delete delete-records" title="Delete" value="'.$cutter->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
            return $html;
        })
        ->editColumn('created_at',function($cutter)
        {
            $html = formatDate($cutter->created_at);
            return $html;
        })
        ->editColumn('factory_manager',function($cutter)
        {
            $manager = Salesman::where('id',$cutter->factory_manager)->first();
            if($manager){
                $html = $manager->title;
            }else{
                $html = '--';
            }
            return $html;
        })
        ->rawColumns(['image_name','title','description','status', 'action']);
        return $datatables->make(true);
    }
    /**
     * Create cutter
     */
    public function create()
    {
        $app = app();
        $factory_assign_manager = Salesman::where('type',6)
        ->where('status',1)
        ->pluck('title','id');
        $cutter = $app->make('stdClass');
        $cutter->id = -1;
        $cutter->image_name = '';
        $cutter->email = '';
        $cutter->title = '';
        $cutter->description = '';
        $cutter->status=1;
        $cutter->phone = '';
        // $cutter->factory_manager = '';
        // $cutter->monthly_target = '';
        $cutter->type = '2';

        return view('admin.cutter.edit')->with('heading','Create')->with('cutter',$cutter)->with('factory_assign_manager', $factory_assign_manager);
    }
    /*
    * Change Status
    */
    public function changeStatus(Request $request) {
        if($request->ajax()) {
            $id   = $request->get('id');
            if($id && is_numeric(($id))) {
                if(Salesman::where('id', $id)->update(array('status' => $request->get('status')))) {
                    echo json_encode(array('status' => 'success', 'message' => 'updated successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
                }
            }
            echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
        }
    }


    /**
     Edit Record
     */
    public function edit($cutter_id) {
        if(!empty($cutter_id) && is_numeric($cutter_id)) {
            $value = Salesman::find($cutter_id);
            //$userType = Teamtype::where('id','3')
            //->orwhere('id','5')->pluck('typename','id');
            $factory_assign_manager = Salesman::where('type',2)->pluck('title','id');

            $id = $value['id'];
            if( $id == $cutter_id ) {
               $cutter = DB::table($this->tableName)->select('*')->where('id',$cutter_id)->first();
             return view('admin.cutter.edit')->with('cutter',$cutter)->with('heading','Edit')->with('factory_assign_manager',$factory_assign_manager);
             //->with('userType',$userType);
            } else {
                return redirect()->route('cutter.index')->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('cutter.index')->with('error',trans('message.invalidId'));
        }
    }
    /*
     * Update Record
     */
    public function update(Request $request,$cutter_id) {

        if($cutter_id>0)
            {
                request()->validate([
                    'title'         => 'required'
                 ]);
                $data = array();
                $allowedMimeTypes = array(
                    'image/jpeg',
                    'image/png',
                    'image/gif',
                    'image/svg',
                    'image/svg+xml',
                    'image/bmp'
                );
                if($request->hasFile('image_name')) {
                    $mimetype = $request->file('image_name')->getClientMimeType();
                    if(!in_array($mimetype, $allowedMimeTypes)){
                        return redirect()->route('cutter.index')->with('error', $this->ModuleName.trans('message.imgTypeError'));
                    } else{
                        $image = $request->file('image_name');
                        $data['image_name'] = time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/uploads/cutter');
                        $checkPrevious = Salesman::where('id', $cutter_id)->first()->image_name;
                        if(isset($checkPrevious) && !empty($checkPrevious)) {
                            File::delete($destinationPath.'/'.$checkPrevious);
                        }
                        $image->move($destinationPath, $data['image_name']);
                    }
                }
                $data['title'] = $request->get('title');
                $data['email'] = $request->get('email');
                $data['phone'] = $request->get('phone');
                $data['description']  = $request->get('description');
                $data['status']       = $request->get('status');
                // $data['factory_manager'] = $request->get('factory_manager');
                // $data['monthly_target'] = $request->get('monthly_target');

                    if(Salesman::where('id', $cutter_id)->update($data)) {
                        return redirect()->route('cutters.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
                    } else {
                        return redirect()->route('cutter.index')->with('error', $this->ModuleName.trans('message.networkErr'));
                    }
            }else { //for Create
                $v = Validator::make($request->all(), [
                'title'         => 'required',
                'email'         =>'required|email',
                'phone'         =>'required|numeric',
              
                ]);
                $data = array();
                $allowedMimeTypes = array(
                    'image/jpeg',
                    'image/png',
                    'image/gif',
                    'image/svg',
                    'image/svg+xml',
                    'image/bmp'
                );
                $imageName = NULL;
                if($request->hasFile('image_name')) {
                    $mimetype = $request->file('image_name')->getClientMimeType();
                    if(!in_array($mimetype, $allowedMimeTypes)){
                        return redirect()->route('cutter.edit')->with('success', 'Please select a valid image type');
                        exit;
                        
                    } else{
                        $image = $request->file('image_name');
                        $imageName = time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/uploads/cutter');
                        $image->move($destinationPath, $imageName);
                    }
                }
            $request['image_name'] = $imageName;
            $request = $request->input();
            $request['type'] =2;

            //create password
            $passwordcreate = mt_rand(1000, 9999);
            $request['password'] = \Hash::make($passwordcreate);
            //dd($request);
            Salesman::create($request);

            //Welcome mail
            $user['email']= $request['email'];

            return redirect()->route('cutters.index')->with('Success', 'Created Successfully');      
        }
    }

     /*
     * Delete Record
     */
    public function destroy(Request $request,$id){
        if($request->ajax()) {
            if($id && is_numeric($id)) {
                $destinationPath = public_path('/uploads/cutter');
                $deleteImg = Salesman::where('id', $id)->first()->image_name;
                if(Salesman::destroy($id)) {
                    if(isset($deleteImg) && !empty($deleteImg)) {
                        File::delete($destinationPath.'/'.$deleteImg);
                    }
                    echo json_encode(array('status' => 'success', 'message' => 'Deleted successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => 'Deleted successfully'));exit;
                }
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
            }
        }
     

    }
    /*
     * Show Record
     */
    public function show()
    {
        return redirect()->route('cutter.index')->with('error',trans('message.invalidId'));
    }

    public function delete(Request $request) {
        $id   = $request->get('product_id');
       // $this->removeProfileImage($id);
       // Customer::destroy($id);
        $product = Product::where('id', $id)->first();
        $product->delete();
        return response()->json(['status'=> 'success', 'message' => 'Deleted successfully']);
    }
}
