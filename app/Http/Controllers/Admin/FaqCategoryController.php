<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
Use App\Models\FaqCategory;
use Yajra\Datatables\Datatables;

class FaqCategoryController extends AdminController
{

    public function __construct()
    {
        $model = new FaqCategory();
        $this->tableName =  $model->table;
        $this->ModuleName = 'FAQ category';
    }

    public function index() {
        return view('admin.faqCategories.index');
    }

    public function table_data() {
        $faqCategories = FaqCategory::select('*');
        return Datatables::of($faqCategories)

        	->editColumn('title',function($faqCategories){
                $html = $faqCategories->name;
                return $html;   
            })
            ->addColumn('action',function ($faqCategories){
                $html='<a class="btn btn-primary" title="Edit" onclick="showEditModel(\''.$faqCategories->id.'\');" value="'.$faqCategories->id.'" module-id="'.$faqCategories->id.'"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                    <button class="btn btn-danger btn-delete delete-records" title="Delete" value="'.$faqCategories->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
                return $html;
           	})
           	->editColumn('created_at',function($currencies)
            {
                $html = formatDate($currencies->created_at);
                return $html;
            })
            ->editColumn('updated_at',function($currencies)
            {
                $html = formatDate($currencies->updated_at);
                return $html;
            })
           	->rawColumns(['title', 'action'])
            ->make(true);
    }

    /*
     * Add Record
     */
    public function store(Request $request) {
        // print_r($request->all());die;
        if($request->ajax()) {
            $validatedData = $request->validate([
                'title'    => 'required'
            ]);
            $data = array();
            unset($request['_token']);
            unset($request['_method']);
            unset($request['FaqCategoryFormAction']);
            $data['name'] = $request->get('title');
            
            if(FaqCategory::create($data)) {
                echo json_encode(array('status' => 'success', 'message' => $this->ModuleName.trans('message.AddedMsg')));exit;
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
            }
        }
    }

    /*
     * Delete Record
     */
    public function destroy(Request $request,$id) {
    	if($request->ajax()) {
    		if($id && is_numeric($id)) {
		        if(FaqCategory::destroy($id)) {
		        	echo json_encode(array('status' => 'success', 'message' => $this->ModuleName.trans('message.DeletedMsg')));exit;
		        } else {
		        	echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
		        }
		    } else {
		    	echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
		    }
	    }
    }

    /*
     * Update Record
     */
   	public function update(Request $request,$faqcategoryID) {
    	if($request->ajax()) {
    		if($faqcategoryID && is_numeric($faqcategoryID)) {
	    		$validatedData = $request->validate([
					'title'    => 'required',
                ]);
				unset($request['_token']);
	        	unset($request['_method']);
	        	unset($request['FaqCategoryFormAction']);
                $request = $request->all();
                $data['name'] = $request['title'];
               
                try { 
                    FaqCategory::where('id', $faqcategoryID)->update($data);
                } catch(\Illuminate\Database\QueryException $ex){ 
                    echo json_encode(array('status' => 'error', 'message' => $ex->getMessage()));exit;
                }
                echo json_encode(array('status' => 'success', 'message' => $this->ModuleName.trans('message.UpdatedMsg')));exit;
	        }
	        echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
    	}
    }

    /*
     * Get Record
     */
   	public function getEditRecord(Request $request) {
        $result = FaqCategory::select('*')->find($request->get('id'));
        if($result->first()){
            echo json_encode(array('status'=> 'success','result'=>$result));exit;
        }
        echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
    }

    

}
