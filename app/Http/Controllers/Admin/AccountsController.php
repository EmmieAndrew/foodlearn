<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use Illuminate\Support\Facades\Validator;
use App\Models\Order;
use App\Models\Account;
use App\Models\Salary;
use App\Models\Product;
use App\Models\Payorders;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;


class AccountsController extends AdminController
{
    
    public function __construct()
    {
        $model = new Account();
        $this->tableName =  $model->table;
        $this->ModuleName = 'Account';
    }

    public function index()
    {
        return view('admin.accounts.index');  
    }

    public function summary(Request $request)
    {
        $orders = 0;
        $otherIncome = 0;
        $pettyCashIncome = 0;
        $inventoryCash = 0;
        $payableCash = 0;
        $otherExpenses = 0;
        $payrolls = 0;

        if($request->from && $request->to){

            $from = date('Y-m-d', strtotime($request->from));
            $to = date('Y-m-d', strtotime($request->to));

            //calculate sales amount
            $ordersData = Order::whereBetween('created_at', array($from, $to))->get();
            if($ordersData){
                foreach ($ordersData as $value) {
                    $orders += $value->total_amount;
                }
            }

            //calculate income rather than Petty Cash
            $notPettyCash = Account::whereBetween('account_date', array($from, $to))
                            ->where('type',1)->where('category', 'Other')->get();
            if($notPettyCash){
                foreach ($notPettyCash as $value) {
                    $otherIncome += $value->amount;
                }
            }

            //calculate income Petty Cash
            $pettyCash = Account::whereBetween('account_date', array($from, $to))
                            ->where('type',1)->where('category', 'Petty Cash')->get();
            if($pettyCash){
                foreach ($pettyCash as $value) {
                    $pettyCashIncome += $value->amount;
                }
            }

            //calculate Inventory
            $inventory = Product::get();
            if($inventory){
                foreach ($inventory as $value) {
                    $inventoryCash += $value->product_length * $value->product_price;
                }
            }

            //calculate Account Payable
            $payable = Payorders::get();
            if($payable){
                foreach ($payable as $value) {
                    $payableCash += $value->remaining_amount;
                }
            }

            //calculate Expenses
            $expenses = Account::whereBetween('account_date', array($from, $to))
                            ->where('type',0)
                            ->where('category','!=', 'Purchase Order')
                            ->where('category','!=', 'Payroll')->get();
            if($expenses){
                foreach ($expenses as $value) {
                    $otherExpenses += $value->amount;
                }
            }

            //calculate payroll
            $payroll = Account::whereBetween('account_date', array($from, $to))
                            ->where('type',0)
                            ->where('category', 'Payroll')->get();
            if($payroll){
                foreach ($payroll as $value) {
                    $payrolls += $value->amount;
                }
            }

        }

        $cash = $orders+$otherIncome;

        return json_encode(array('cash' => $cash, 'pettyCashIncome' => $pettyCashIncome, 'inventoryCash' => $inventoryCash, 'payableCash' => $payableCash, 'otherExpenses' => $otherExpenses, 'payrolls' => $payrolls));
    }


    public function table_data(Request $request)
    {
        if($request->from && $request->to){
            $from = date('Y-m-d', strtotime($request->from));
            $to = date('Y-m-d', strtotime($request->to));

            $accounts = Account::Select('*')
                        ->whereBetween('account_date', array($from, $to));
                        //->get();
                        //dd($accounts);
        }else{
            $accounts = Account::Select('*');
        }
        //dd($accounts);
        $datatables = Datatables::of($accounts)

        ->editColumn('amount',function ($account){
              $html = number_format($account->amount,2, ".", ",");
              return $html;
        })

        ->editColumn('expense',function ($account){
            if($account->type == 1){
                $html = $account->category;
            }else{
                $html ='--';
            }
              return $html;
        })
        ->editColumn('income',function ($account){
            if($account->type == 0){
                $html = $account->category;
            }else{
                $html ='--';
            }
              return $html;
        })
        // ->addColumn('action',function ($account){
        //     $html='<a class="btn btn-primary" value="'.$account->id.'" href="'.route('accounts.edit',$account->id).'"><i class="fa fa-pencil" aria-hidden="true"></i></a>'.
        //     '<button class="btn btn-danger btn-delete delete-records" title="Delete" value="'.$account->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
        //     return $html;
        // })
        ->editColumn('account_date',function($account)
        {
            $html = formatDate($account->account_date);
            return $html;
        })
        ->editColumn('action',function($account)
        {
            if($account->attachment){
                $html = '<a href="/uploads/finance_management/'.$account->attachment.'" target="_blank"><i class="fa fa-paperclip" aria-hidden="true"></i></a>';
            }else{
                $html = '--';
            }
            return $html;
        })
        ->rawColumns(['id','account_date','type','amount','comment', 'action']);
        return $datatables->make(true);
    }
    /**
     * Create account
     */
    public function create()
    {
        $blablaType = array("Expense", "Income");
        $categoryList = array('Rent'=>'Rent', 'Government'=>'Government', 'Utility'=>'Utility', 'Other expenses'=>'Other expenses');
        $incomeCategoryList = array('Petty Cash'=>'Petty Cash', 'Other'=>'Other income');

        $app = app();
        $account = $app->make('stdClass');
        $account->id = -1;
        $account->category = '';
        $account->type = '';
        $account->amount = '';
        $account->comment = '';
        $account->account_date = '';
        $account->attachment = '';
        return view('admin.accounts.edit')->with('heading','Create')->with('account',$account)->with('blablaType', $blablaType)->with('categoryList', $categoryList)->with('incomeCategoryList', $incomeCategoryList);
    }

    /**
     Edit Record
     */
    public function edit($account_id) {
        if(!empty($account_id) && is_numeric($account_id)) {
            $value = Account::find($account_id);

            $blablaType = array("Expense", "Income");
            $categoryList = array('Rent'=>'Rent', 'Government'=>'Government', 'Utility'=>'Utility', 'Other expenses'=>'Other expenses');
            $incomeCategoryList = array('Petty Cash'=>'Petty Cash', 'Other'=>'Other income');

            $id = $value['id'];
            if( $id == $account_id ) {
               $account = DB::table($this->tableName)->select('*')->where('id',$account_id)->first();
             return view('admin.accounts.edit')->with('account',$account)->with('heading','Edit')
                    ->with('blablaType',$blablaType)->with('categoryList',$categoryList)
                    ->with('incomeCategoryList',$incomeCategoryList);
            } else {
                return redirect()->route('accounts.index')->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('accounts.index')->with('error',trans('message.invalidId'));
        }
    }
    /*
     * Update Record
     */
    public function update(Request $request,$account_id) {
        if($account_id>0)
            {
                request()->validate([
                    //'category' => 'required',
                    'type' => 'required',
                    'amount' => 'required',
                    'account_date' => 'required',
                    'attachment' => 'required',
                 ]);
                $data = array();
                $data['category'] = $request->get('category');
                $data['type'] = $request->get('type');
                $data['amount'] = $request->get('amount');
                $data['comment'] = $request->get('comment');
                $data['account_date'] = $request->get('account_date');
                $data['attachment'] = $request->get('attachment');

                    if(Account::where('id', $account_id)->update($data)) {
                        return redirect()->route('accounts.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
                    } else {
                        return redirect()->route('accounts.index')->with('error', $this->ModuleName.trans('message.networkErr'));
                    }
                }else { //for Create
                    $v = Validator::make($request->all(), [
                    //'category' => 'required',
                    'type' => 'required',
                    'amount' => 'required',
                    'account_date' => 'required',
                    'attachment' => 'required',
                  
                    ]);
                $data = array();
                    $allowedMimeTypes = array(
                        'image/jpeg',
                        'image/png',
                        'image/gif',
                        'image/svg',
                        'image/svg+xml',
                        'image/bmp'
                    );
                    $imageName = NULL;
                    if($request->hasFile('attachment')) {
                        $mimetype = $request->file('attachment')->getClientMimeType();
                        if(!in_array($mimetype, $allowedMimeTypes)){
                            return redirect()->route('accounts.edit')->with('success', 'Please select a valid type');
                            exit;
                            
                        } else{
                            $image = $request->file('attachment');
                            $imageName = time().'.'.$image->getClientOriginalExtension();
                            $destinationPath = public_path('/uploads/finance_management');
                            $image->move($destinationPath, $imageName);
                        }
                    }
                $request['attachment'] = $imageName;

                $request = $request->input();
                if($request['incomeCategorySelect'] != null){

                    $request['category'] = $request['incomeCategorySelect']; 
                }else{

                    $request['category'] = $request['categorySelect']; 
                }

                Account::create($request);

                return redirect()->route('accounts.index')->with('Success', 'Created Successfully');      
            }
        }

     /*
     * Delete Record
     */
    public function destroy(Request $request,$id){
        if($request->ajax()) {
            if($id && is_numeric($id)) {
                if(Account::destroy($id)) {
                    echo json_encode(array('status' => 'success', 'message' => 'Deleted successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => 'Deleted successfully'));exit;
                }
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
            }
        }
     

    }
}
