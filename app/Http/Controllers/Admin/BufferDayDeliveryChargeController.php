<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use File;
Use DB;
use Route;
use Illuminate\Support\Facades\Validator;
use App\Models\tax;

class BufferDayDeliveryChargeController extends AdminController
{
    
    public function __construct()
    {
        $model = new tax();
        $this->tableName =  $model->table;
        $this->ModuleName = 'taxes';
    }

    public function index()
    {
        return view('admin.BufferDayDeliveryCharge.index');  
    }

    public function table_data(Request $request)
    {
        $timeduration_delivery = tax::select('*');
        $datatables = Datatables::of($timeduration_delivery)
        // ->editColumn('created_at',function($timeduration_delivery)
        // {
        //     $html = formatDate($timeduration_delivery->created_at);
        //     return $html;
        // })
        // ->editColumn('updated_at',function($timeduration_delivery)
        // {
        //     $html = formatDate($timeduration_delivery->updated_at);
        //     return $html;
        // })
        ->addColumn('action',function ($timeduration_delivery){
            $html='<a class="btn btn-primary" value="'.$timeduration_delivery->id.'" href="'.route('buffer_day_delivery.edit',$timeduration_delivery->id).'"><i class="fa fa-pencil" aria-hidden="true"></i></a>'/*.'<button class="btn btn-danger btn-delete delete-records" title="Delete" value="'.$timeduration_delivery->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>'*/;
            return $html;
        })
        ->rawColumns(['buffer_days','delivery_charges','created_at','action']);
        return $datatables->make(true);
    }
   
    /**
     * Create Model
     */
    public function create()
    {
        $app = app();
        $bufferDelivery = $app->make('stdClass');
        $bufferDelivery->id = -1;
        $bufferDelivery->buffer_days = '';
        $bufferDelivery->delivery_charges = '';
        $bufferDelivery->tax_rate = '';

        return view('admin.BufferDayDeliveryCharge.edit')->with('heading','Create')->with('bufferDelivery',$bufferDelivery);
    }
    /**
     Edit Record
     */
    public function edit($bufferDelivery_id) {
        if(!empty($bufferDelivery_id) && is_numeric($bufferDelivery_id)) {
            $value = tax::find($bufferDelivery_id);
            $id = $value['id'];
            if( $id == $bufferDelivery_id ) {
               $bufferDelivery = DB::table($this->tableName)->select('*')->where('id',$bufferDelivery_id)->first();
             return view('admin.BufferDayDeliveryCharge.edit')->with('bufferDelivery',$bufferDelivery)->with('heading','Edit');
             //->with('userType',$userType);
            } else {
                return redirect()->route('buffer_day_delivery.index')->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('buffer_day_delivery.index')->with('error',trans('message.invalidId'));
        }
    }
    /*
     * Update Record
     */
    public function update(Request $request,$bufferDelivery_id) {
        if($bufferDelivery_id>0)
            {
                request()->validate([
                    'buffer_days'         => 'required',
                    'delivery_charges'         => 'required',
                    'tax_rate'         => 'required'
                 ]);
                $data = array();
                $data['buffer_days'] = $request->get('buffer_days');
                $data['delivery_charges'] = $request->get('delivery_charges');
                $data['tax_rate'] = $request->get('tax_rate');

                    if(tax::where('id', $bufferDelivery_id)->update($data)) {
                        return redirect()->route('buffer_day_delivery.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
                    } else {
                        return redirect()->route('buffer_day_delivery.index')->with('error', $this->ModuleName.trans('message.networkErr'));
                    }
                }else { //for Create
                    $v = Validator::make($request->all(), [
                    'buffer_days'         => 'required',
                    'delivery_charges'         => 'required',
                    'tax_rate'         => 'required'
                  
                    ]);
                $data = array();
                $request = $request->input();

                tax::create($request);

                return redirect()->route('buffer_day_delivery.index')->with('Success', 'Created Successfully');      
            }
        }

    public function destroy(Request $request,$id){
        if($request->ajax()) {
            if($id && is_numeric($id)) {
                $destinationPath = public_path('/uploads/modelpricing');
                $deleteImg = tax::where('id', $id)->first()->image;
                if(tax::destroy($id)) {
                    if(isset($deleteImg) && !empty($deleteImg)) {
                        File::delete($destinationPath.'/'.$deleteImg);
                    }
                    echo json_encode(array('status' => 'success', 'message' => 'Deleted successfully'));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => 'Deleted successfully'));exit;
                }
            } else {
                echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
            }
        }
     

    }
  
    /*
     * Show Record
     */
    public function show()
    {
        return redirect()->route('buffer_day_delivery.index')->with('error',trans('message.invalidId'));
    }


}
