<?php

namespace App\Http\Controllers\Admin;

Use DB;
Use Route;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use File;
use Illuminate\Support\Facades\Input;
Use App\Models\Loyalty;

class LoyaltyController extends AdminController
{
     public function __construct()
    {
        $model = new Loyalty();
        $this->tableName =  $model->table;
        $this->ModuleName = 'loyalty_programs';
    }

    public function index()
    {
       return view('admin.loyaltyPrograms.index');
    }



    public function table_data(Request $request){
        $loyaltyPrograms  = Loyalty::select('*');
        $datatables = Datatables::of($loyaltyPrograms)
            
             ->editColumn('user_id',function ($loyalty){
                //dd($loyalty->customerData->name);
               $html = $loyalty->customerData['name'];
               //$html = $loyalty->user_id;
               return $html;
            })
            ->editColumn('reedem_code',function ($loyalty){
                if($loyalty->reedem_code){
                    $html = $loyalty->reedem_code;
                }else{
                    $html = '--';
                }
               return $html;
            })
             
           // ->editColumn('status',function ($promoCode){
           //      $html = '<label class="switch" id="changeStatus" module-id="'.$promoCode->id.'">';
           //      if($promoCode->status){
           //          $html .= '<input type="checkbox" checked="checked">';
           //      } else {
           //          $html .= '<input type="checkbox">';
           //      }
           //      $html .= '<span class="lever round"></span></label>';
           //      return $html;
           //  })
            // ->addColumn('action',function ($loyalty){
            //     $html='<a class="btn btn-primary" value="'.$loyalty->id.'" href="'.route('loyalty.edit',$loyalty->id).'"><i class="fa fa-pencil" aria-hidden="true"></i></a>
            //             <button class="btn btn-danger btn-delete delete-records" value="'.$loyalty->id.'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
            //     return $html;
            // })
            ->editColumn('created_at',function($loyalty)
            {
                $html = formatDate($loyalty->created_at);
                return $html;
            })
         
            //->rawColumns(['order_id','user_id','transaction_type','points', 'reedem_code','action']);
            ->rawColumns(['order_id','user_id','transaction_type','points', 'reedem_code']);
            return $datatables->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request){

        $app = app();
        $promoCode = $app->make('stdClass');
        $promoCode->id = -1;
        $promoCode->promo = '';
        $promoCode->amount = '';
        //$promoCode->code = 'GT'.random_number(8);
        $promoCode->status=1;
       
        return view('admin.loyaltyPrograms.edit')->with('heading','Create')->with('promoCode',$promoCode);
    }



     /*
     * Change Status
     */
    public function changeStatus(Request $request) {
        if($request->ajax()) {
            $id   = $request->get('id');
            if($id && is_numeric(($id))) {
                if(Loyalty::where('id', $id)->update(array('status' => $request->get('status')))) {
                    echo json_encode(array('status' => 'success', 'message' => trans('message.statusUpdated')));exit;
                } else {
                    echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
                }
            }
            echo json_encode(array('status' => 'error', 'message' => trans('message.networkErr')));exit;
        }
    }

    /*
     * Edit Record
     */
    public function edit($promoCode_id) {
        if(!empty($promoCode_id) && is_numeric($promoCode_id)) {
            $value = Loyalty::find($promoCode_id);
            $id = $value['id'];
            if( $id == $promoCode_id ) {
               $promoCode = DB::table($this->tableName)->select('*')->where('id',$promoCode_id)->first();
             return view('admin.loyaltyPrograms.edit')->with('promoCode',$promoCode)->with('heading','Edit');
            } else {
                return redirect()->route('promoCodes.index')->with('error',trans('message.invalidId'));
            }
        } else {
            return redirect()->route('promoCodes.index')->with('error',trans('message.invalidId'));
        }
    }

    /*
     * Update Record
     */
    public function update(Request $request,$promoCode_id) {
        if($promoCode_id>0)
            {
                request()->validate([
                    'amount'         => 'required',
                    'promo'        => 'required'
                ]);
                $data['amount']       = $request->get('amount');
                $data['promo']        = $request->get('promo');
                $data['status']       = $request->get('status');
                    if(Loyalty::where('id', $promoCode_id)->update($data)) {
                        return redirect()->route('promoCodes.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg'));
                    } else {
                        return redirect()->route('promoCodes.index')->with('error', $this->ModuleName.trans('message.networkErr'));
                    }
                }else {
                //create new promo.
                $v = Validator::make($request->all(), [
                'amount'        => 'required',
                'promo'         => 'required'      
                ]);
                if ($v->fails()) {
                    return back()->with('errors', $v->errors())->withInput();
                }else{

        
                    $request = $request->input();
                    Loyalty::create($request);
                    return redirect()->route('promoCodes.index')->with('success', $this->ModuleName.trans('message.UpdatedMsg')); 
                }     
            }
         }
        /*
        * Delete record
        */
        public function destroy($id) {
        Loyalty::destroy($id);
        return response()->json(['status'=> 'success', 'message' => $this->ModuleName.trans('message.DeletedMsg')]);
        }
}
