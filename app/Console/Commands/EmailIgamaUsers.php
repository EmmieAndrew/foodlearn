<?php

namespace App\Console\Commands;

use App\Notifications\NotifyIgamaUser;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models\Salesman;

class EmailIgamaUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:igama-users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Email igama users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $limit = Carbon::now()->subDay(7);
        $igama_user = Salesman::where('igama_renewal_date', '<', $limit)->get();
        foreach ($igama_user as $user) {
            $user->notify(new NotifyIgamaUser());
        }
    }
}
