<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payorders extends Model
{
    public $table = 'payorders';
    protected $primaryKey = 'id';

    protected $fillable = [
        'date','refrence_number','supplier_id','product_code','product_name','quantity','purchase_unit_price','selling_unit_price','purchase_amount','paid_amount','remaining_amount','due_date','note','image','tax'
    ];

    public function getSupplier(){
        return $this -> belongsTo('App\Models\Supplier','supplier_id','id');
    }

}
