<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OtherUsers extends Model
{
    public $table = 'other_users';
    protected $primaryKey = 'id';
    protected $fillable = [
        'image_name', 'name','email','igama_renewal_date','phone','monthly_target','status','type','defective_order','emp_id','factory_manager'
    ];
    public function teamtype(){
    	return $this -> belongsTo('App\Models\Teamtype','type','id');
    }
    public function salary()
    {
        return $this->belongsTo('App\Models\Salary','emp_id','emp_id');
    }    
}
