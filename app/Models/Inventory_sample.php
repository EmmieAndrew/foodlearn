<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Inventory_sample extends Model
{
    
    public $table = 'inventory_samples';
   
    protected $fillable = [
        'request','product_id','place','salesman_id','type','name','color','length','comment'
    ];
    
    // public function Product()
    // {
    //     return $this->hasMany('App\Models\Order');
    // }
}