<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactUsQuery extends Model
{
    public $table = 'contactus';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name','email','title','message'
    ];
}
