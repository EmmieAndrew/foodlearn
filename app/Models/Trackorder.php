<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trackorder extends Model
{
    public $table = 'tracking_delivery';
    protected $fillable = [
        'order_unique_id','order_placed','order_confirmed','with_cutter','with_tailor','with_qa','redy_for_delivery','delivered','cutter_id','tailor_id','qc_id'
    ];
}
