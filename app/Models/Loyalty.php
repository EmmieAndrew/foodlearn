<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Loyalty extends Model
{
	use SoftDeletes;
	
    public $table = 'loyalty_programs';
   
    protected $fillable = [
        'user_id','transaction_type','points','reedem_code','order_id'
    ];


    public function customerData(){
        return $this -> belongsTo('App\Models\Customer','user_id','id');
    }    
}
