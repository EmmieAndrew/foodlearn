<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    public $table = 'salary';
    protected $primaryKey = 'id';

    protected $fillable = [
        'emp_id','salary','status'
    ];

    public function empDetails()
    {
        return $this->belongsTo('App\Models\Salesman','emp_id','emp_id');
    }

    public function otherEmpDetails()
    {
        return $this->belongsTo('App\Models\OtherUsers','emp_id','emp_id');
    }    
}
