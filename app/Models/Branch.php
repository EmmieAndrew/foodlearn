<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    public $table = 'branches';
    protected $primaryKey = 'id';

    protected $fillable = [
        'branch', 'branch_details', 'factory','target','status','branch_id','phone','salesman_id'
    ];

    public function orderWithSale()
    {
        return $this->hasManyThrough('App\Models\Order', 'App\Models\Salesman' ,'branch', 'order_by', 'id', 'id');
    }    
}
