<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Measurement extends Model
{
    public $table = 'measurements';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name','image', 'price'
    ];
}
