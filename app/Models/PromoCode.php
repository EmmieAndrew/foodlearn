<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{
    public $table = 'promo';
    protected $fillable = [
        'promo','amount','status','type','date_range'
    ];
}
