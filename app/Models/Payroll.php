<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payroll extends Model
{
    public $table = 'payroll';
    protected $primaryKey = 'id';

    protected $fillable = [
        'emp_id', 'name','salary','assigned_hours','acheived_hours','no_defective_order','commission','other_deduction','total','evaluation','note'
    ];
    // public function teamtype(){
    // 	return $this -> belongsTo('App\Models\Teamtype','type','id');
    // }

    // public function getall()
    // {
    // 	return $this->belongsTo('App\Models\OtherUsers');
    // }
    // public function branchdata()
    // {
    //     return $this->belongsTo('App\Models\Branch','branch','id');
    // }
}
