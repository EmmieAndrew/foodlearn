<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class addMoreAddress extends Model
{
    
    public $table = 'customers_addresses';
    protected $fillable = [
        'customer_id','customer_unique_id','address2','lat2','longs2'
    ];
   
}