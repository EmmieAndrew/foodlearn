<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class WebsiteContent extends Model
{
    
    public $table = 'website_contents';
   
    protected $fillable = [
        'title','app_store','play_store	','content','address'
    ];
}