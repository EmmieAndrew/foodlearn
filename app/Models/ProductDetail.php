<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class ProductDetail extends Model
{
    
    public $table = 'product_details';
   
    protected $fillable = [
        'product_details','product_description','product_type'
    ];

     public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }
}