<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaqCategory extends Model
{
   	public $table = 'faq_categories';
    protected $primaryKey = 'id';
    protected $fillable = ['name'];
}
