<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Measurement_order extends Model
{
    public $table = 'measurement_orders';
    protected $primaryKey = 'id';

    protected $fillable = ['customer_id','season','fabric','product_color','shoulder_details','arm_details','chest_detail','hips_details','placket_details','collars','plackets','pockets','arms'];
}