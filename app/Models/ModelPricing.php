<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModelPricing extends Model
{
    public $table = 'model_pricing';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name','image', 'price'
    ];
}
