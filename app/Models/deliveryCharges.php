<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class deliveryCharges extends Model
{
    
    public $table = 'delivery_charges';
    protected $fillable = [
        'charges'
    ];
   
}