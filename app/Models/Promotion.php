<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    public $table = 'promotions';
   
    protected $fillable = [
        'title','description','valid_till'
    ];

     
}
