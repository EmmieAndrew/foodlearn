<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class CustomerDashboardImage extends Model
{
    
    public $table = 'customer_dashboard_images';
   
    protected $fillable = [
        'image'
    ];
}