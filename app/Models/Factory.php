<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Factory extends Model
{
    public $table = 'factories';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name', 'address', 'status','fac_id','phone','manager_id'
    ];

    public function getBranches(){
        return $this -> hasMany('App\Models\Branch','factory','id');
    }    
}
