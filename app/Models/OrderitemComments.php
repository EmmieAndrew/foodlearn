<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Salesman;
use Edujugon\PushNotification\PushNotification;

class OrderitemComments extends Model
{
    public $table = 'orderitemcomments';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id','order_unique_id','order_product_id','comment_image','comment'
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
           	//$id = $model->order_by;
        	//die('gg');
           	$device_token =	Salesman::select('device_token')->where('type',3)->get();
           	$token = $device_token->map(function ($item, $key) {
                return $item->device_token;
            });
            $push = new PushNotification('fcm');
        	foreach ($token as $tok){
            $resp = $push->setMessage([
           'notification' => [
                   'title'=>'New message',
                   'body'=>  "Someone comment on the order",
                   'sound' => 'default'
                   ],
           'data' => [
                   'extraPayLoad1' => 'Test payload 1',
                   'extraPayLoad2' => 'Test payload 2'
                   ]
           ])
        ->setApiKey('AAAA4QR01kU:APA91bHr4NK_VHkO2ligVUxB75o-bxBHIbmUNZcr65ek28zp4uwV_hxiMeumBeozh9P4Fv-NZbXsIdh6foYp8S1XrC-aKcyuK4MQ2WOidZK4WydjUqAZkEGcmAVHw1UZuBfMstYVPwWu')
        ->setDevicesToken($tok)
        ->send()->getFeedback();

        }
        });
    }
}
