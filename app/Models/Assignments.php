<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Assignments extends Model
{
     public $table = 'assignments';
   
    protected $fillable = [
        'user_id','order_unique_id','cutter_id','status','old_tailor_id','tailor_id','tailor_status','tailor_assigned_at','qc_id','qc_status','qc_assigned_at','cutter_completion_date','tailor_completion_date','qc_passed_at','assigned_at','order_status'
    ];

    public function getCuter(){
        return $this -> belongsTo('App\Models\Salesman','cutter_id','id');
    }

    public function getTailor(){
        return $this -> belongsTo('App\Models\OtherUsers','tailor_id','id');
    }

    public function getQc(){
        return $this -> belongsTo('App\Models\Salesman','qc_id','id');
    }    
}
