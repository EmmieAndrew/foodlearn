@extends('admin.layouts.app')
@section('title','Private Policy')
@section('content')
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span><?php echo $private_body_display->title; ?></span></h1>
        <div class="breadcrumb">
            <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
            <span>></span>
            <span class="active"><?php echo $private_body_display->title; ?></span>
        </div>
        <div class="new-customer-btn single-btn">
            <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#privateEdit" id="private_edit" ><i class="fa fa-pencil" aria-hidden="true"></i> Edit <?php echo $private_body_display->title; ?></button>
        </div>
    </div>
</div>

<div class="mng-customer-table">
    <h1 class="page-common-head"><span><?php echo $private_body_display->title; ?></span></h1>
    @if ($errors->has('content'))
        <span class="invalid-feedback">
            <strong id="email-server-err-exists">{{ $errors->first('content') }}</strong>
        </span>
    @endif
    <?php echo $private_body_display->content; ?>
</div>
</script>
<input id="url" type="hidden" value="{{Request::url('admin')}}">
<form method="POST" action="{{ url('admin/ContentManagement/privateUpdate') }}" id="private-edit-form">
{{ csrf_field() }}
<div class="modal fade p-l-res" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="privateEdit">
  <div class="modal-dialog modal-lg modal-delete">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title"><?php echo $private_body_display->title; ?></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="col-xs-12 form-group">
            <div class="row">                
                <label for="inputName" class="col-xs-12 control-label">Heading <span>*</span></label>
                <div class="col-xs-12">                   
                    <input type="text"  class="form-control has-error" id="title" name = "title" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('title'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('title') }}</strong>
                    </span>
                    @endif
                </div>                
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="form-group col-xs-12 col-sm-12 col-md-12">
            <div class="row">                
                <label for="inputName" class="col-xs-12 control-label">Content <span>*</span></label>
                <div class="col-xs-12">                        
                    <textarea class="form-control has-error" id="content" name="content">                       
                    </textarea>
                    
                    <p id="existing-answers"></p>
                    <p class="allTypeError"></p>
                    @if ($errors->has('content'))
                    <span class="invalid-feedback" id="errorTextArea">
                        <strong>{{ $errors->first('content') }}</strong>
                    </span>
                    @endif
                </div>                
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
         <button type="submit" class="btn btn-primary primary deletePromotions">Save</button>         
        </div>
      </div>
    </div>
  </div>
</div>
</form>
<script>
    CKEDITOR.replace( 'content' );
</script>
@if(session()->has('success'))
    <script type="text/javascript">
        $.growl.notice({title: "Success!", message:  '{!! session('success') !!}' });
    </script>
@endif
@stop
