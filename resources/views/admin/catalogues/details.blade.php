@extends('admin.layouts.app')
@section('title','Details')
@section('content')
    <div class="page-heading">
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span>Inventory Details</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','inventories') }}">Inventories</a></span>
                <span>></span>
                <span class="active">{{ (!empty($order->first()->product_code))?$order->first()->product_code:$heading }}</span>
            </div>
            <div class="table-responsive">
                <table class="table" id="resorts-table">
                    <thead>
                        <tr>
                            <th>Product Code</th>
                            <th>Product Type</th>
                            <th>Product Name</th>
                            <th>Product Color</th>
                            <th>Product Length</th>
                            <th>Selling Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td>@if($order['product_type'] == 1)
                                Summer
                                @elseif($order['product_type'] == 2)
                                Winter
                                @elseif($order['product_type'] == 3)
                                Other
                                @endif
                            </td>
                            <td>{{$order->product_name}}</td>
                            <td>{{$order->product_color}}</td>
                            <td>{{$order->product_length}}</td>
                            <td>{{$order->selling_option}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>      
        </div>
    </div>
    <div class="clearfix"></div></br>
    <script type="text/javascript" src="{{ asset('js/backend/catalogues.js') }}"></script>
@stop