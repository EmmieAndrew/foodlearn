@extends('admin.layouts.app')
@section('title','Catalogues')
@section('content')
<form method="POST" enctype="multipart/form-data" action="{{ url('admin/catalogues/'.$products->id) }}" id="catalogues-edit-form">
    {{ csrf_field() }}
    {{ method_field('PATCH') }}
    <div class="page-heading">
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span>catalogue</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','inventories') }}">catalogues</a></span>
                <span>></span>
                <span class="active">{{ (!empty($products->product_name))?$products->product_name:$heading }}</span>
            </div>
            <div class="new-customer-btn cancel-button">
                <a class="btn btn-primary pull-right" href="{{ url('admin','inventories') }}"> <i class="fa fa-times" aria-hidden="true"></i>Cancel</a>
            </div>
            <button type="submit" class="btn btn-primary save_btn">
                 <i class="fa fa-floppy-o" aria-hidden="true"></i>Save
            </button>
        </div>
    </div>

    <div class="col-xs-12">
        <h4 class="modal-subheading"><span>Basic Details</span></h4>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Selling Option <span>*</span></label>
                <div class="col-xs-12">
                    <select class="form-control has-error" id="selling_option" name ="selling_option">
                            <option value="All">All</option>
                            <option value="BAM">BAM</option>
                            <option value="B2B">B2B</option>
                        </select>
                    @if ($errors->has('selling_option'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('selling_option') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Product Length<span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="product_length" name = "product_length" value="{{$products->product_length}}"/>
                    <p></p>
                    @if ($errors->has('product_length'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('product_length') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Product Price<span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="product_price" name = "product_price" value="{{$products->product_price}}"/>
                    <p></p>
                    @if ($errors->has('product_price'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('product_price') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
      
    <input type="hidden" id="formAction" value="edit" />
    <input type="hidden" id="product_id" value="{{$products->id}}" />

</form>
</div>


<link rel="stylesheet" type="text/css" href="{{ asset('css/imgareaselect/imgareaselect-default.css') }}" />

<script type="text/javascript" src="{{ asset('js/backend/catalogues.js')}}"></script>
@stop