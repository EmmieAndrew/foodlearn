<!DOCTYPE html>
<html>
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>BAM - Login</title>
        <link rel="shortcut icon" type="image/xicon" href="{{ asset('images/common/favicon.ico') }}" />
        <link href="{{ asset('css/login-style.css') }}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome-4.2.min.css') }}" />
        <script type="text/javascript" src="{{ asset('js/common/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/common/jquery.validate.min.js') }}" async></script>
    </head>
    <body class="">
        <form method="POST" action="{{ route('admin.auth.loginAdmin') }}" id="login-form">
            {{ csrf_field() }}
            <div class="loginfixed">
                <div class="loginfixed-inner">
                    <div class="loginfixed-tb">
                        <div class="loginfixed-cell">
                            <div class="logoblockMain">
                                <a href="{{ url('/') }}"> <img src="{{ asset('images/common/logo.jpg') }}"></a>
                            </div>
                            @if (session('errorMsg'))
                                <span class="login-common-err invalid-feedback">
                                    <span>
                                        {{ session('errorMsg') }}
                                    </span>
                                    <img src="{{ asset('images/common/close-red.png') }}" onclick="closeloginerror();">
                                </span>
                            @endif
                            <div class="login-block">
                                <div class="login-form">
                                    <h2>LOGIN</h2>
                                    <p>Please login to continue.</p>
                                    <div class="full-block">
                                        <div class="input-block full-block-email">
                                            <input id="email" type="email" class="email-icon form-control" name="email" value="{{ old('email') }}" autofocus placeholder="Email Address" />
                                        </div>
                                    </div>
                                    <div class="full-block full-block-pwd">
                                        <div class="input-block">
                                            <input id="password" type="password" class="key-icon form-control" name="password"  placeholder="Password" />
                                        </div>
                                    </div>
                                    <div class="full-block">
                                        <div class="input-block">                                   
                                            <div class="login-lock">
                                                <button class="login-btn" type="submit">{{ __('Login') }}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
        <footer>
            <div class="footertxt">
                © BAM. {{ date('Y') }}. All rights reserved.
            </div>
        </footer>
    </body>
    <script>
        $(document).ready(function() {
            $("#login-form").validate({   
                rules:{
                    email: {
                        required: true,
                        email: true,
                    },
                    password: {
                        required: true,
                    }
                },
                messages:{
                    email: {
                        required: 'Please enter your email.',
                        email:    'Please enter valid email address.',
                    },
                    password: {
                        required: 'Please enter your password.',
                    }
                },
                errorElement:"span",
                errorClass:"invalid-feedback",
                highlight: function(element) { 
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });        
        });
        function closeloginerror(){
            $(".login-common-err").slideUp();
        }
    </script>
</html>
