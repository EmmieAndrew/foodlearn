@extends('admin.layouts.app')
@section('title','Tailor')
@section('content')
    <div class="page-heading">
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span>Tailors</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','tailor') }}">Tailors</a></span>
                <span>></span>
                <span class="active">{{ (!empty($tailor->title))?$tailor->title:$heading }}</span>
            </div>
            
        </div>
    </div>
    
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Name</label>
                    <div class="col-xs-12">
                        <p>{{ (!empty($tailor->title))?ucwords($tailor->title):'-' }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">E-mail</label>
                    <div class="col-xs-12 varifiedtxtblock">
                        <p>{{ (!empty($tailor->email))?$tailor->email:'-' }}</p>
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Phone</label>
                    <div class="col-xs-12 varifiedtxtblock">
                        <p>{{ (!empty($tailor->phone))?$tailor->phone:'-' }}</p>
                        
                    </div>
                </div>
            </div>
        </div>
        

        
        
     
    
       <!--  <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Address</label>
                    <div class="col-xs-12">
                        <p>{{ (!empty($tailor->address))?$tailor->address:'-' }}</p>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Created By</label>
                    <div class="col-xs-12">
                        <p>{{(!empty($tailor->created_by))?$tailor->created_by:'-' }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Account Type</label>
                    <div class="col-xs-12">
                        <p>{{ (!empty($tailor->account_type))?$tailor->account_type:'-' }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Last order date</label>
                    <div class="col-xs-12">
                        <p>{{ (!empty($tailor->last_order_date))?$tailor->last_order_date:'-' }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Total order</label>
                    <div class="col-xs-12">
                        <p>{{ (!empty($tailor->total_order))?$tailor->total_order:'-' }}</p>
                        
                    </div>
                </div>
            </div>
        </div>
       
    </div>
     -->
      
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Status</label>
                    <div class="col-xs-12">
                        <p>{{ $tailor->status ? 'Active' : 'Inactive' }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 img_upload-div">
            <label  class="control-label" for="image_file">Profile Image</label>
            <div class="input-cover-modal">
                <div class="image_preview error-outer">
                    <img id="show-image-preview" src="{{ (!empty($tailor->image_name) && file_exists(public_path('uploads/tailor/'.$tailor->profile_image)))?asset('uploads/tailor/'.$tailor->image_name):asset('images/web/noprofileimage.png') }}" alt="">
                </div>
            </div>
        </div>
       
        
    


@stop