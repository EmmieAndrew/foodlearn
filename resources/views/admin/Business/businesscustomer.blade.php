@extends('admin.layouts.app')
@section('title','Business')
@section('content')
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Business</span></h1>
        <div class="breadcrumb">
            <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
            <span>></span>
            <span><a href="{{ url('admin','business') }}">Business</a></span>
            <span>></span>
            <span class="active">{{ (!empty($business->name))?$business->name:$heading }}</span>
        </div>

    </div>
</div>

<div class="col-xs-12 col-sm-6 col-md-6">
    <div class="row">
        <div class="form-group error">
            <label for="inputName" class="col-xs-12 control-label">Name</label>
            <div class="col-xs-12">
                <p>{{ (!empty($business->name))?ucwords($business->name):'-' }}</p>
            </div>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
    <div class="row">
        <div class="form-group error">
            <label for="inputName" class="col-xs-12 control-label">E-mail</label>
            <div class="col-xs-12 varifiedtxtblock">
                <p>{{ (!empty($business->email))?$business->email:'-' }}</p>

            </div>
        </div>
    </div>
</div>


<div class="clearfix"></div>

<div class="col-xs-12 col-sm-6 col-md-6">
    <div class="row">
        <div class="form-group error">
            <label for="inputName" class="col-xs-12 control-label">Phone</label>
            <div class="col-xs-12 varifiedtxtblock">
                <p>{{ (!empty($business->phone))?$business->phone:'-' }}</p>

            </div>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
    <div class="row">
        <div class="form-group error">
            <label for="inputName" class="col-xs-12 control-label">business Unique Id</label>
            <div class="col-xs-12">
                <p>{{(!empty($business->customer_unique_id))?$business->customer_unique_id:'-' }}</p>
            </div>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
    <div class="row">
        <div class="form-group error">
            <label for="inputName" class="col-xs-12 control-label">Address</label>
            <div class="col-xs-12">
                <p>{{ (!empty($business->address))?$business->address:'-' }}</p>
            </div>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
    <div class="row">
        <div class="form-group error">
            <label for="inputName" class="col-xs-12 control-label">Created By</label>
            <div class="col-xs-12">
                <p>{{(!empty($business->created_by))?$business->created_by:'-' }}</p>
            </div>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
    <div class="row">
        <div class="form-group error">
            <label for="inputName" class="col-xs-12 control-label">Account Type</label>
            <div class="col-xs-12">
                <p>{{ (isset($business->account_type) && $business->account_type==1)? 'Normal':'VIP' }}</p>
            </div>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
    <div class="row">
        <div class="form-group error">
            <label for="inputName" class="col-xs-12 control-label">Last order date</label>
            <div class="col-xs-12">
                <p>{{ (!empty($last_orderdate->created_at))?$last_orderdate->created_at:'-' }}</p>
            </div>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
    <div class="row">
        <div class="form-group error">
            <label for="inputName" class="col-xs-12 control-label">Total order</label>
            <div class="col-xs-12">
                <p>{{ (!empty($total_orderses))?$total_orderses:'-' }}</p>

            </div>
        </div>
    </div>
</div>

</div>


<div class="col-xs-12 col-sm-6 col-md-6">
    <div class="row">
        <div class="form-group error">
            <label for="inputName" class="col-xs-12 control-label">Status</label>
            <div class="col-xs-12">
                <p>{{ $business->status ? 'Active' : 'Inactive' }}</p>
            </div>
        </div>
    </div>
</div>






@stop