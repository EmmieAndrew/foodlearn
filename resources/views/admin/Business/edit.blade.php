@extends('admin.layouts.app')
@section('title','Manage Business')
@section('content')
<form method="POST" id="changeModuleForm" enctype="multipart/form-data" action="{{ url('admin/business/'.$business->id) }}">
{{ csrf_field() }}
{{ method_field('PATCH') }}

<style>
    label.checkbox-input-info {
    display: flex;
    align-items: center;
}
label.checkbox-input-info input{margin-right: 10px;}
</style>

<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Create Business Customer</span></h1>
        <div class="breadcrumb">
            <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
            <span>></span>
            <span><a href="{{ url('admin','business') }}">B2B Customers</a></span>
            <span>></span>
            <span class="active">{{$heading}} Business Customer</span>
        </div>
        <div class="new-customer-btn cancel-button">
            <a class="btn btn-primary pull-right" href="{{ url('admin','business') }}"> <i class="fa fa-times" aria-hidden="true"></i>Cancel</a>
        </div>
        <button type="submit" class="btn btn-primary save_btn">
            <i class="fa fa-floppy-o" aria-hidden="true"></i>{{ __('Save') }}
        </button>
    </div>
</div>

<input type="hidden" name="id" id="id"  value="{{$business->id}}">

<div class="handi-form p-l-res">

    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Shop Name <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-errorname" name = "shopname"  value="{{$business->shopname}}" required="" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('shopname'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('shopname') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Username <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-errorname" name = "username"  value="{{$business->username}}" required=""/>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('username'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('username') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

   <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Responsible Person <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-errorname" name = "responsible_name"  value="{{$business->responsible_name}}" required="" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('responsible_name'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('responsible_name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Contact Number <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-errorname" name = "contact_number"  value="{{$business->contact_number}}" required="" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('contact_number'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('contact_number') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>


   <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Shop Email <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-errorname" name = "shop_email"  value="{{$business->shop_email}}" required=""/>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('shop_email'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('shop_email') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

   <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Shop Location <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-errorname" name = "shop_location"  value="{{$business->shop_location}}" required=""/>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('shop_location'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('shop_location') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!--hidden fields-->

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="row">
            <label class="col-xs-12 control-label checkbox-input-info"><input type="checkbox" name="colorCheckbox" value="check">Want to add more details ? Check me (Optional fields)</label>
        </div>
    </div>

    <div class="check box" style="display: none;">

        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Summer Service Price <span></span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-errorname" name = "summer_service_price"  value="{{$business->summer_service_price}}" placeholder="00.00"/>
                        <p class="allTypeError"></p>
                        @if ($errors->has('summer_service_price'))
                        <span class="invalid-feedback">
                            <strong id="email-server-err-exists">{{ $errors->first('summer_service_price') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 ">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Winter Service Price <span></span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-errorname" name = "winter_sewing_price"  value="{{$business->winter_sewing_price}}" placeholder="00.00"/>
                        <p class="allTypeError"></p> 
                        @if ($errors->has('winter_sewing_price'))
                        <span class="invalid-feedback">
                            <strong id="email-server-err-exists">{{ $errors->first('winter_sewing_price') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 ">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Piping <span></span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-errorname" name = "piping"  value="{{$business->piping}}" placeholder="00.00"/>
                        <p class="allTypeError"></p> 
                        @if ($errors->has('piping'))
                        <span class="invalid-feedback">
                            <strong id="email-server-err-exists">{{ $errors->first('piping') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 ">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Washing <span></span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-errorname" name = "washing"  value="{{$business->washing}}" placeholder="00.00"/>
                        <p class="allTypeError"></p> 
                        @if ($errors->has('washing'))
                        <span class="invalid-feedback">
                            <strong id="email-server-err-exists">{{ $errors->first('washing') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 ">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Botana <span></span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-errorname" name = "botana"  value="{{$business->botana}}" placeholder="00.00"/>
                        <p class="allTypeError"></p> 
                        @if ($errors->has('botana'))
                        <span class="invalid-feedback">
                            <strong id="email-server-err-exists">{{ $errors->first('botana') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 ">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Embroidery <span></span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-errorname" name = "embroidery"  value="{{$business->embroidery}}" placeholder="00.00"/>
                        <p class="allTypeError"></p> 
                        @if ($errors->has('embroidery'))
                        <span class="invalid-feedback">
                            <strong id="email-server-err-exists">{{ $errors->first('embroidery') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 ">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Zipper <span></span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-errorname" name = "zipper"  value="{{$business->zipper}}" placeholder="00.00"/>
                        <p class="allTypeError"></p> 
                        @if ($errors->has('zipper'))
                        <span class="invalid-feedback">
                            <strong id="email-server-err-exists">{{ $errors->first('zipper') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

       <div class="col-xs-12 col-sm-6 col-md-6 ">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Sleeping Dress <span></span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-errorname" name = "sleeping_dress"  value="{{$business->sleeping_dress}}" placeholder="00.00"/>
                        <p class="allTypeError"></p> 
                        @if ($errors->has('sleeping_dress'))
                        <span class="invalid-feedback">
                            <strong id="email-server-err-exists">{{ $errors->first('sleeping_dress') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 ">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Other Service<span></span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-errorname" name = "other"  value="{{$business->other}}" placeholder="Enter service name"/>
                        <p class="allTypeError"></p> 
                        @if ($errors->has('other'))
                        <span class="invalid-feedback">
                            <strong id="email-server-err-exists">{{ $errors->first('other') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 ">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Other Service Price<span></span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-errorname" name = "other_price"  value="{{$business->other_price}}" placeholder="Enter other service price"/>
                        <p class="allTypeError"></p> 
                        @if ($errors->has('other_price'))
                        <span class="invalid-feedback">
                            <strong id="email-server-err-exists">{{ $errors->first('other_price') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</form>
<script type="text/javascript" src="{{ asset('js/backend/business.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('input[type="checkbox"]').click(function() {
            var inputValue = $(this).attr("value");
            $("." + inputValue).toggle();
        });
    });
</script>
@stop




