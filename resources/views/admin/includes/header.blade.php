<a href="#" class="sidebar__toggle"><i class="fa fa-bars"></i></a>

<div class="admin-logo">
	<a href="{{ url('admin','dashboard') }}">
		<img class="logo-lg" src="{{ asset('images/common/logo.jpg') }}">
		<img class="logo-sm" src="{{ asset('images/common/logo.jpg') }}">
	</a>
</div>

<div class="header-login">
	<?php $role = Auth::user(); ?>
	<a>
    	<span class="admin-usrname">{{ ucwords($role->name) }}</span>
		<span class="caret"></span>
	</a>
	<ul class="dropdown-menu">
		<div class="drop-down-cover">
			<li> <a href="#" data-toggle="modal" data-target="#changePasswordModel"><i class="fa fa-lock"></i> Change Password</a> </li>
			<li> <a href="{{ route('admin.auth.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i> Logout</a> </li>
			<form id="logout-form" action="{{ route('admin.auth.logout') }}" method="POST" style="display: none;">
	            {{ csrf_field() }}
	        </form>
	  	</div>
	</ul>
</div>
