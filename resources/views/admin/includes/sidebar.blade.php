<div class="sidebar__wrapper" id="content-3">
    <ul class="sidebar__nav">
        @if(!empty(Auth::user()->charity_id))
            <li class="li-logo">
                <img src="{{ asset('uploads/charities/'.getCharityLogo(Auth::user()->charity_id)) }}">
            </li>
            <li>
                <a class="active" href="{{ route('charity.auctionDetails') }}"><img src="{{ asset('images/web/side4.png') }}">Auction Details</a>
            </li>
        @else
            <li>
                <a href="{{ url('admin','dashboard') }}" class="{{ Request::segment(2) == 'dashboard' ? 'active' : '' }}"> <i class="fa fa-dashboard"></i>Dashboard </a>
            </li>
            <!-- <li>
                <a href="{{ url('admin','customers') }}" class="{{ Request::segment(2) == 'customers' ? 'active' : '' }}"> <img src="{{ asset('images/backend/users.svg') }}">Customer Management </a>
            </li> -->
            <li>
                <a href="javascript:void(0)" class="menu {{ Request::segment(2) == 'salesmans'|| Request::segment(2) == 'cutters' || Request::segment(2) == 'quality' || Request::segment(2) == 'otherUsers' || Request::segment(2) == 'tailor' ? 'active open arrow-down' : '' }}"><img src="{{ asset('images/backend/salesman.png') }}" >Users Management<i class="fa pull-right fa-chevron-down"></i></a>
                <ul class="dropwn-sidebar {{ Request::segment(2) == 'salesmans'|| Request::segment(2) == 'cutters' || Request::segment(2) == 'quality' || Request::segment(2) == 'otherUsers' || Request::segment(2) == 'tailor' ? 'open' : '' }}">
                    <!-- <li>
                        <a class="{{ Request::segment(2) == 'salesmans' ? 'active' : '' }}" href="{{ url('admin','salesmans') }}"><img src="{{ asset('images/backend/salesman.png') }}">Learners</a>
                    </li> -->
                    <li>
                        <a class="{{ Request::segment(2) == 'tailor' ? 'active' : '' }}" href="{{ url('admin','tailor')}}"><img src="{{ asset('images/backend/sewing-machine.png') }}">Learners</a>
                    </li>
                    <li>
                        <a class="{{ Request::segment(2) == 'cutters' ? 'active' : '' }}" href="{{ url('admin','cutters')}}"><img src="{{ asset('images/backend/scissors.png') }}">Tutors</a>
                    </li>
                    <!-- <li>
                        <a class="{{ Request::segment(2) == 'quality' ? 'active' : '' }}" href="{{ url('admin','quality')}}"><img src="{{ asset('images/backend/qa.png') }}">Quality Checker</a>
                    </li> -->
                    <!-- <li>
                        <a class="{{ Request::segment(2) == 'otherUsers' ? 'active' : '' }}" href="{{ url('admin','otherUsers')}}"><img src="{{ asset('images/backend/packages.png') }}">Other Users</a>
                    </li> -->
                </ul>
            </li>
            
            <!-- <li>
                <a href="javascript:void(0)" class="menu {{ Request::segment(2) == 'business'|| Request::segment(2) == 'Uniforms' ? 'active open arrow-down' : '' }}"><img src="{{ asset('images/backend/business.png') }}" >B2B<i class="fa pull-right fa-chevron-down"></i></a>
                <ul class="dropwn-sidebar {{ Request::segment(2) == 'business'|| Request::segment(2) == 'Uniforms' ? 'open' : '' }}">
                    <li>
                        <a class="{{ Request::segment(2) == 'business' ? 'active' : '' }}" href="{{ url('admin','business') }}"><img src="{{ asset('images/backend/debit-card.png') }}">Sewing</a>
                    </li>
                    <li>
                        <a class="{{ Request::segment(2) == 'Uniform' ? 'active' : '' }}" href="{{ url('admin','uniform') }}"><img src="{{ asset('images/backend/bank-account.svg') }}">Uniforms</a>
                    </li>
                </ul>
            </li> -->

            <!-- <li>
                <a class="{{ Request::segment(2) == 'measurement' ? 'active' : '' }}" href="{{ url('admin','measurement') }}"><img src="{{ asset('images/backend/measurement.png') }}">Measurements</a>
            </li> -->

            <!-- <li>
                <a class="{{ Request::segment(2) == 'business' ? 'active' : '' }}" href="{{ url('admin','business') }}"><img src="{{ asset('images/backend/business.png') }}">Business</a>
            </li> -->
            
            <li>
                <a href="javascript:void(0)" class="menu {{ Request::segment(2) == 'webpage'|| Request::segment(2) == 'branches' || Request::segment(2) == 'factories' ? 'active open arrow-down' : '' }}"><img src="{{ asset('images/backend/company.png') }}" >Site Management<i class="fa pull-right fa-chevron-down"></i></a>
                <ul class="dropwn-sidebar {{ Request::segment(2) == 'webpage'|| Request::segment(2) == 'branches' || Request::segment(2) == 'factories' ? 'open' : '' }}">
                    <!-- <li>
                        <a class="{{ Request::segment(2) == 'branches' ? 'active' : '' }}" href="{{ url('admin','branches') }}"><img src="{{ asset('images/backend/company.png') }}">Branches</a>
                    </li> -->
                    <li>
                        <a class="{{ Request::segment(2) == 'webpage' ? 'active' : '' }}" href="{{ url('admin','webpage') }}"><img src="{{ asset('images/backend/content.svg') }}">Webpage</a>
                    </li>
                    <!-- <li>
                        <a class="{{ Request::segment(2) == 'factories' ? 'active' : '' }}" href="{{ url('admin','factories') }}"><img src="{{ asset('images/backend/factory.png') }}">Factory Management</a>
                    </li> -->
                </ul>
            </li>
         
            <!-- <li>
                <a href="javascript:void(0)" class="menu {{ Request::segment(2) == 'suppliers'|| Request::segment(2) == 'payorders' ? 'active open arrow-down' : '' }}"><img src="{{ asset('images/backend/termsandcondition.svg') }}" >Supplier Management<i class="fa pull-right fa-chevron-down"></i></a>
                <ul class="dropwn-sidebar {{ Request::segment(2) == 'suppliers'|| Request::segment(2) == 'payorders' ? 'open' : '' }}">
                    <li>
                        <a class="{{ Request::segment(2) == 'suppliers' ? 'active' : '' }}" href="{{ url('admin','suppliers') }}"><img src="{{ asset('images/backend/give-over.png') }}">Supplier List</a>
                    </li>
                    <li>
                        <a class="{{ Request::segment(2) == 'payorders' ? 'active' : '' }}" href="{{ url('admin','payorders') }}"><img src="{{ asset('images/backend/ecommerce.png') }}">Purchase Orders</a>
                    </li>
                </ul>
            </li> -->
            
            <!-- <li>
                <a href="javascript:void(0)" class="menu {{ Request::segment(2) == 'salary'|| Request::segment(2) == 'payroll' ? 'active open arrow-down' : '' }}"><img src="{{ asset('images/backend/bank-account.svg') }}" >Payroll Management<i class="fa pull-right fa-chevron-down"></i></a>
                <ul class="dropwn-sidebar {{ Request::segment(2) == 'salary'|| Request::segment(2) == 'payroll' ? 'open' : '' }}">
                    <li>
                        <a class="{{ Request::segment(2) == 'salary' ? 'active' : '' }}" href="{{ url('admin','salary') }}"><img src="{{ asset('images/backend/debit-card.png') }}">Salary</a>
                    </li>
                    <li>
                        <a class="{{ Request::segment(2) == 'payroll' ? 'active' : '' }}" href="{{ url('admin','payroll') }}"><img src="{{ asset('images/backend/bank-account.svg') }}">Payroll</a>
                    </li>
                </ul>
            </li> -->

            <!-- <li>
                <a href="javascript:void(0)" class="menu {{ Request::segment(2) == 'modelpricing'|| Request::segment(2) == 'timeduration_deliverybuffer_day_delivery' ? 'active open arrow-down' : '' }}"><img src="{{ asset('images/backend/price-tag.png') }}" >Product / Pricing<i class="fa pull-right fa-chevron-down"></i></a>
                <ul class="dropwn-sidebar {{ Request::segment(2) == 'modelpricing'|| Request::segment(2) == 'buffer_day_delivery' ? 'open' : '' }}">
                    <li>
                        <a class="{{ Request::segment(2) == 'modelpricing' ? 'active' : '' }}" href="{{ url('admin','modelpricing') }}"><img src="{{ asset('images/backend/currency.svg') }}">Model pricing</a>
                    </li>
                    <li>
                        <a class="{{ Request::segment(2) == 'buffer_day_delivery' ? 'active' : '' }}" href="{{ url('admin','buffer_day_delivery') }}"><img src="{{ asset('images/backend/watch.png') }}">Tax & Charges</a>
                    </li>
                </ul>
            </li> -->

            <!-- <li>
                <a href="javascript:void(0)" class="menu {{ Request::segment(2) == 'inventory'|| Request::segment(2) == 'inventories' ? 'active open arrow-down' : '' }}"><img src="{{ asset('images/backend/catalogue.png') }}" >Inventory Management<i class="fa pull-right fa-chevron-down"></i></a>
                <ul class="dropwn-sidebar {{ Request::segment(2) == 'inventory'|| Request::segment(2) == 'catalogues' ? 'open' : '' }}">
                    <li>
                        <a class="{{ Request::segment(2) == 'catalogues' ? 'active' : '' }}" href="{{ url('admin','inventories') }}"><img src="{{ asset('images/backend/termsandcondition.svg') }}">Inventory Listing</a>
                    </li>
                    <li>
                        <a class="{{ Request::segment(2) == 'inventory' ? 'active' : '' }}" href="{{ url('admin','inventory') }}"><img src="{{ asset('images/backend/content.svg') }}">Inventory Requests</a>
                    </li>
                </ul>
            </li> -->

            <li>
                <a href="javascript:void(0)" class="menu {{ Request::segment(2) == 'orders'|| Request::segment(2) == 'finished' ? 'active open arrow-down' : '' }}"><img src="{{ asset('images/backend/order.png') }}" >Transactions<i class="fa pull-right fa-chevron-down"></i></a>
                <ul class="dropwn-sidebar {{ Request::segment(2) == 'orders'|| Request::segment(2) == 'finished' ? 'open' : '' }}">
                    <li>
                        <a class="{{ Request::segment(2) == 'orders' ? 'active' : '' }}" href="{{ url('admin','orders') }}"><img src="{{ asset('images/backend/watch.png') }}">Orders In Process</a>
                    </li>
                    <li>
                        <a class="{{ Request::segment(2) == 'finished' ? 'active' : '' }}" href="{{ url('admin','finished') }}"><img src="{{ asset('images/backend/price-tag.png') }}">Order Finished</a>
                    </li>
                </ul>
            </li>          
            <li>
                <a class="{{ Request::segment(2) == 'accounts' ? 'active' : '' }}" href="{{ url('admin','accounts') }}"><img src="{{ asset('images/backend/debit-card.png') }}">Finance Management</a>
            </li>
            <li>
                <a href="javascript:void(0)" class="menu {{ Request::segment(2) == 'orders'|| Request::segment(2) == 'finished' ? 'active open arrow-down' : '' }}"><img src="{{ asset('images/backend/order.png') }}" >Appointments<i class="fa pull-right fa-chevron-down"></i></a>
                <ul class="dropwn-sidebar {{ Request::segment(2) == 'orders'|| Request::segment(2) == 'finished' ? 'open' : '' }}">
                    <li>
                        <a class="{{ Request::segment(2) == 'orders' ? 'active' : '' }}" href="{{ url('admin','orders') }}"><img src="{{ asset('images/backend/watch.png') }}">Upcomming Appointments</a>
                    </li>
                    <li>
                        <a class="{{ Request::segment(2) == 'finished' ? 'active' : '' }}" href="{{ url('admin','finished') }}"><img src="{{ asset('images/backend/price-tag.png') }}">History</a>
                    </li>
                </ul>
            </li>  
            <!-- <li>
                <a class="{{ Request::segment(2) == 'promoCodes' ? 'active' : '' }}" href="{{ url('admin','promoCodes') }}"><img src="{{ asset('images/backend/refer.svg') }}">Promo Codes</a>
            </li> -->
        {{--<li>
                <a class="{{ Request::segment(2) == 'referralCodes' ? 'active' : '' }}" href="{{ url('admin','referralCodes') }}"><img src="{{ asset('images/backend/refer.svg') }}">Referral Codes</a>
            </li>--}}
            <!-- <li>
                <a class="{{ Request::segment(2) == 'loyalty' ? 'active' : '' }}" href="{{ url('admin','loyalty') }}"><img src="{{ asset('images/backend/loyalty.png') }}">Loyalty Goal</a>
            </li> -->
        {{--<li>
                <a class="{{ Request::segment(2) == 'notifications' ? 'active' : '' }}" href="{{ url('admin','notifications') }}"><img src="{{ asset('images/backend/notification.png') }}">Notifications</a>
            </li>--}}
            <li>
                <a class="{{ Request::segment(2) == 'payments' ? 'active' : '' }}" href="{{ url('admin','payments') }}"><img src="{{ asset('images/backend/payment-method.png') }}">Payment</a>
            </li>
            <li>
                <a class="{{ Request::segment(2) == 'query' ? 'active' : '' }}" href="{{ url('admin','query') }}"><img src="{{ asset('images/backend/query.png') }}">Query Management</a>
            </li>
            <!-- <li>
                <a class="{{ Request::segment(2) == 'reports' ? 'active' : '' }}" href="{{ url('admin','reports') }}"><img src="{{ asset('images/backend/growth.png') }}">Reports</a>
            </li> -->
        {{--<li>
                <a class="{{ Request::segment(2) == 'Analytics' ? 'active' : '' }}" href="{{ url('admin','analytics') }}"><img src="{{ asset('images/backend/analy.jpg') }}">Analytics</a>
            </li>
            <li>
                <a class="{{ Request::segment(2) == 'branch_access' ? 'active' : '' }}" href="{{ url('admin','branch_access') }}"><img src="{{ asset('images/backend/branch_details.png') }}">Role Based Access to &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Branches</a>
            </li>
            <li>
                <a class="{{ Request::segment(2) == 'websiteContent' ? 'active' : '' }}" href="{{ url('admin/websiteContent/edit_website_content') }}"><img src="{{ asset('images/backend/content.svg') }}">Website Content</a>
            </li>
            <li>
                <a href="javascript:void(0)" class="menu {{ Request::segment(2) == 'about-us' || Request::segment(2) == 'faq'|| Request::segment(2) == 'faqcategories' || Request::segment(2) == 'terms' || Request::segment(2) == 'privacy-policy'|| Request::segment(2) == 'donationVsBid' ? 'active open arrow-down' : '' }}"><img src="{{ asset('images/backend/content-manage.svg') }}" >Content Management<i class="fa pull-right fa-chevron-down"></i></a>
                <ul class="dropwn-sidebar {{ Request::segment(2) == 'about-us' || Request::segment(2) == 'faq'||Request::segment(2) == 'faqcategories' || Request::segment(2) == 'terms' ||  Request::segment(2) == 'privacy-policy'|| Request::segment(2) == 'donationVsBid' ? 'open' : '' }}">
                    <li>
                        <a class="{{ Request::segment(2) == 'faq' ? 'active' : '' }}" href="{{ url('admin','faq') }}"><img src="{{ asset('images/backend/faq.png') }}">FAQs</a>
                    </li>
                    <li>
                        <a class="{{ Request::segment(2) == 'faqcategories' ? 'active' : '' }}" href="{{ url('admin','faqcategories') }}"><img src="{{ asset('images/backend/faq.png') }}">FAQ Categories</a>
                    </li>
                    <li>
                        <a class="{{ Request::segment(2) == 'terms' ? 'active' : '' }}" href="{{ url('admin', 'terms') }}"><img src="{{ asset('images/backend/termsandcondition.svg') }}">Terms and Conditions</a>
                    </li>
                    <li>
                        <a class="{{ Request::segment(2) == 'about-us' ? 'active' : '' }}" href="{{ url('admin','about-us') }}"><img src="{{ asset('images/backend/about.png') }}">About Us</a>
                    </li>
                    <li>
                        <a class="{{ Request::segment(2) == 'privacy-policy' ? 'active' : '' }}" href="{{ url('admin/privacy-policy') }}"><img src="{{ asset('images/backend/privacypolicy.png') }}">Privacy Policy</a>
                    </li>
                    <li>
                        <!-- <a class="{{ Request::segment(2) == 'donationVsBid' ? 'active' : '' }}" href="{{ url('admin','donationVsBid') }}"><img src="{{ asset('images/backend/charity.svg') }}">Donation Vs Bid</a> -->
                    </li>
                </ul>
            </li>--}}
        @endif
    </ul>
</div>