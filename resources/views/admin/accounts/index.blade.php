@extends('admin.layouts.app')
@section('title','Finance Management')
@section('content')
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Finance Management</span></h1>
        <div class="breadcrumb">
            <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
            <span>></span>
            <span class="active">Manage Finance Management</span>
        </div>
        <div class="new-customer-btn single-btn">
            <a class="btn btn-primary pull-right" id="show-add-form" href="{{ route('accounts.create') }}">
                <i class="fa fa-plus" aria-hidden="true"></i> Add Income / Expense</a>
        </div>
    </div>
</div>


<div class="handi-form">
    <h5>View Income / Expense</h5>
    <div class="container-2">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <label for="daterange" class="col-xs-12 control-label">Date Range </label>
                        <input class="form-control input-daterange-datepicker" type="text" name="daterange" value="{{date('m/d/Y')}}">
                    </div>
                    <!-- <div class="col-xs-12 col-sm-4 col-md-4">
                        <label for="inputName" class="col-xs-12 control-label">Year </label>

                    </div> -->
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <label for="inputName" class="col-xs-12 control-label">&nbsp</label>
                        <a class="btn btn-primary pull-right form-control" id="searchInEx" href="#"><i class="fa fa-search" aria-hidden="true"></i> View Income / Expense</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="mng-customer-table">
    <table class="table table-bordered" id="data-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Date</th>
                <th>Expense</th>
                <th>Income</th>
                <!-- <th>Category</th> -->
                <th>Amount (SAR)</th>
                <th>Comment</th>
                <th><i class="fa fa-paperclip" aria-hidden="true"></i></th>
                <!-- <th>Action</th> -->
            </tr>
        </thead>
    </table>

    <div class="" id="summary" style="display: none;">
        <div class="handi-form">
            <h5 class="summary_heading" style="font-weight: 600;">Summary</h5>
            <div class="container-2">
                <div id="page-wrapper">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <label class="col-xs-12 control-label">Assets</label>
                                <div class="col-xs-12">
                                    <spam class="col-xs-6">Inventory:(Till Date)</spam>
                                    <p class="inventory">--</p>
                                </div>
                                <div class="col-xs-12">
                                    <spam class="col-xs-6">Petty Cash:</spam>
                                    <p class="petty_cash">--</p>
                                </div>
                                <div class="col-xs-12">
                                    <spam class="col-xs-6">Cash:</spam>
                                    <p class="cash">--</p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <label for="inputName" class="col-xs-12 control-label">Liability</label>
                                <div class="col-xs-12">
                                    <spam class="col-xs-6">Account Payable:(Till Date)</spam>
                                    <p class="account_payable">--</p>
                                </div>
                                <div class="col-xs-12">
                                    <spam class="col-xs-6">Expenses:</spam>
                                    <p class="expenses">--</p>
                                </div>
                                <div class="col-xs-12">
                                    <spam class="col-xs-6">Salary:</spam>
                                    <p class="payroll">--</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<input id="data-table-url" type="hidden" value="{!! route('accounts.data') !!}">

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="confirm-modal">
    <div class="modal-dialog modal-md modal-delete">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">DELETE</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this Account?</p>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-danger" data-dismiss="modal"> No</button>
                <button type="button" class="btn btn-primary primary deleteRecord" data-dismiss="modal" onclick="deleteRecord();">Yes</button>
                <input type="hidden" id="confirm-modal-delete-id" value="">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{ asset('js/backend/accounts.js') }}"></script>

@if(session()->has('success'))
<script type="text/javascript">
    $.growl.notice({
        title: "Success!",
        message: 'success'
    });
</script>
@endif
@if(session()->has('error'))
<script type="text/javascript">
    $.growl.error({
        title: "Oops!",
        message: 'error'
    });
</script>
@endif

@push('custom-scripts')
<script type="text/javascript">
    $('.input-daterange-datepicker').daterangepicker({
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });    
</script>
@endpush

@stop