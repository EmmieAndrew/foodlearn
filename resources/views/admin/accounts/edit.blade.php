@extends('admin.layouts.app')
@section('title','Finance Management')
@section('content')
<form method="POST" id="changeModuleForm" enctype="multipart/form-data" action="{{ url('admin/accounts/'.$account->id) }}">
{{ csrf_field() }}
{{ method_field('PATCH') }}
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Finance Management</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','accounts') }}">Finance Management</a></span>
                <span>></span>
                <span class="active">{{$heading}} Finance Management</span>
            </div>
            <div class="new-customer-btn cancel-button">
                <a class="btn btn-primary pull-right" href="{{ url('admin','accounts') }}"> <i class="fa fa-times" aria-hidden="true"></i>Cancel</a>
            </div>
            <button type="submit" class="btn btn-primary save_btn">
                 <i class="fa fa-floppy-o" aria-hidden="true"></i>{{ __('Save') }}
            </button>
    </div>
</div>
<input type="hidden" name="id" id="id"  value="{{$account->id}}">
<div class="handi-form p-l-res">
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Date</label>
                <div class="col-xs-12">
                <input type="date"  class="form-control has-error" id="account_date" name = "account_date"  value="{{$account->account_date}}" />
                </div>
            </div>
        </div>
    </div>    
    <div class="col-xs-12 col-sm-6 col-md-6 ">
      <div class="row">
          <div class="form-group error">
              <label for="inputName" class="col-xs-12 control-label">Type</label>
              <div class="col-xs-12">
              <select class="form-control" id="type" name = "type">
                  @foreach ($blablaType as $key => $value)
                  <option value="{{ $key }}" {{ ( $key == $account->type) ? 'selected' : '' }}> 
                  {{ $value }} 
                  </option>
                  @endforeach    
              </select>    
              </div>
          </div>
      </div>
    </div>     
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Category <span>*</span></label>
                <div class="col-xs-12" id="category">
                    <select class="form-control" id="categorySelect" name = "categorySelect">
                      <option value="">Select category</option>
                      @foreach ($categoryList as $key => $value)
                      <option value="{{ $key }}" {{ ( $key == $account->category) ? 'selected' : '' }}> 
                      {{ $value }} 
                      </option>
                      @endforeach    
                    </select>

                    <select class="form-control" id="incomeCategorySelect" name = "incomeCategorySelect" style="display:none;">
                      <option value="">Select category</option>
                      @foreach ($incomeCategoryList as $key => $value)
                      <option value="{{ $key }}" {{ ( $key == $account->category) ? 'selected' : '' }}> 
                      {{ $value }} 
                      </option>
                      @endforeach    
                    </select>
                </div>
            </div>
        </div>
    </div>   
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Amount</label>
                <div class="col-xs-12">
                <input type="text"  class="form-control has-error" id="amount" name = "amount"  value="{{$account->amount}}" />
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Comment</label>
                <div class="col-xs-12">
                <input type="text"  class="form-control has-error" id="comment" name = "comment"  value="{{$account->comment}}" />
                   
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Attachment</label>
                <div class="col-xs-12">
                <input type="file"  class="form-control has-error" id="attachment" name = "attachment"  value="{{$account->comment}}" />
                   
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<script type="text/javascript" src="{{ asset('js/backend/accounts.js') }}"></script>
@stop




