@extends('admin.layouts.app')
@section('title','Loyalty Programs')
@section('content')
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Loyalty Programs</span></h1>
        <div class="breadcrumb">
            <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
            <span>></span>
            <span class="active">Manage Loyalty Programs</span>
        </div>
        <!-- <div class="new-customer-btn single-btn">
            <a class="btn btn-primary pull-right" id="show-add-form" href="{{ route('loyalty.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Add Loyalty</a>
        </div> -->
    </div>
</div>
<div class="mng-customer-table">
    <table class="table table-bordered" id="data-table">
        <thead>
            <tr>
                <th>Order Id</th>
                <th>Use By</th>
                <th>Transaction</th>
                <th>Points</th>
                <th>Code</th>
                <th>Added On</th>
                <!-- <th>Action</th> -->
            </tr>
        </thead>
    </table>
</div>
<input id="data-table-url" type="hidden" value="{!! route('loyalty.data') !!}">

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="confirm-modal">
    <div class="modal-dialog modal-md modal-delete">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">DELETE</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>There may be User(s) which are associated with this Promo Code.
                Are you sure you want to delete this Promo Code?</p>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-danger" data-dismiss="modal"> No</button>
                <button type="button" class="btn btn-primary primary deleteRecord" data-dismiss="modal" onclick="deleteRecord();">Yes</button>
                <input type="hidden" id="confirm-modal-delete-id" value="">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('js/backend/loyaltyProgram.js') }}"></script>

@if(session()->has('success'))
    <script type="text/javascript">
        $.growl.notice({title: "Success!", message:  '{!! session('success') !!}' });
    </script>
@endif
@if(session()->has('error'))
    <script type="text/javascript">
        $.growl.error({title: "Oops!", message:  '{!! session('error') !!}' });
    </script>
@endif
@stop
