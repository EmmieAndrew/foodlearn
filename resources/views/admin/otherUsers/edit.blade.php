@extends('admin.layouts.app')
@section('title','Other Users')
@section('content')
<form method="POST" id="changeModuleForm" enctype="multipart/form-data" action="{{ url('admin/otherUsers/'.$OtherUsers->id) }}">
{{ csrf_field() }}
{{ method_field('PATCH') }}
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Other Users</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','other') }}">Other Users</a></span>
                <span>></span>
                <span class="active">{{$heading}} Other User</span>
            </div>
            <div class="new-customer-btn cancel-button">
                <a class="btn btn-primary pull-right" href="{{ url('admin','otherUsers') }}"> <i class="fa fa-times" aria-hidden="true"></i>Cancel</a>
            </div>
            <button type="submit" class="btn btn-primary save_btn">
                 <i class="fa fa-floppy-o" aria-hidden="true"></i>{{ __('Save') }}
            </button>
    </div>
</div<input type="hidden" name="id" id="id"  value="{{$OtherUsers->id}}">
<div class="handi-form p-l-res>
">
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Name <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="name" name = "name"  value="{{$OtherUsers->name}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('name'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
     <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Email</label>
                <div class="col-xs-12">
                <input type="text"  class="form-control has-error" id="email" name = "email"  value="{{$OtherUsers->email}}" />
                    
                   
                </div>
            </div>
        </div>
    </div>
     <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Contact Number</label>
                <div class="col-xs-12">
                <input type="text"  class="form-control has-error" id="phone" name = "phone"  value="{{$OtherUsers->phone}}" />
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Monthly Target</label>
                <div class="col-xs-12">
                <input type="text"  class="form-control has-error" id="monthly_target" name = "monthly_target"  value="{{$OtherUsers->monthly_target}}" />
                   
                </div>
            </div>
        </div>
    </div>    
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Factory Manager</label>
                <div class="col-xs-12">
                <select class="form-control has-error" id="factory_manager" name = "factory_manager">
                    @foreach($factory_assign_manager as $managerKey => $manager)

                    <option value="{{$managerKey}}" {{ ( $managerKey == $OtherUsers->factory_manager) ? 'selected' : '' }}>{{$manager}}</option>

                    @endforeach
                </select> 
                    
                </div>
            </div>
        </div>
    </div>     
     <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Type</label>
                <div class="col-xs-12">
                <select class="form-control" id="type" name = "type">
                    <option>Select Type</option>
                    @foreach ($userType as $key => $value)
                    <option value="{{ $key }}" {{ ( $key == $OtherUsers->type) ? 'selected' : '' }}> 
                    {{ $value }} 
                    </option>
                    @endforeach    
                </select>  
                </div>
            </div>
        </div>
    </div>
    
     <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Igama Renewal Date</label>
                <div class="col-xs-12">
                <input type="date"  class="form-control has-error" id="igama_renewal_date" name = "igama_renewal_date"  value="{{$OtherUsers->igama_renewal_date}}" />
                   
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Status <span>*</span></label>
                <div class="col-xs-12">
                    <select class="form-control has-error" id="status" name = "status">
                        <?php
                            if($OtherUsers->status==1) 
                            { 
                            echo "<option value='1' selected>Active</option>
                                    <option value='0'>Inactive</option>"; 
                            } 
                            else
                            {
                            echo "<option value='1' >Active</option>
                                   <option value='0' selected>Inactive</option>"; 
                            }
                        ?>                            
                    </select> 
                    <p class="allTypeError"></p>
                    @if ($errors->has('status'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
   
   
    <div class="col-xs-12 col-sm-12 col-md-12 ">
        <div class="row">
            <div class="col-xs-12 col-sm-12 img_upload-div ">
                <div class="form-group error">
                <label  class="control-label" for="image_name">Image</label>
                <div class="input-cover-modal">
                    <div class="image_preview error-outer">
                     
                        <img id="show-image-preview" src="{{ (!empty($OtherUsers->image_name) && file_exists(public_path('uploads/other/'.$OtherUsers->image_name)))?asset('uploads/other/'.$OtherUsers->image_name):asset('images/common/default-image.jpg') }}" alt="" />
                        <div class="choose_file">
                            <input title="" type='file' id="image_name" name="image_name" />
                            <p><i class="fa fa-upload" aria-hidden="true"></i></p>
                        </div>
                        <input type="hidden" name="image-exist" id="image-exist" val = "no">
                    </div>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('image_name'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('image_name') }}</strong>
                    </span>
                    @endif
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<script type="text/javascript" src="{{ asset('js/backend/otherUsers.js') }}"></script>
@stop




