@extends('admin.layouts.app')
@section('title','pending')
@section('content')
    <div class="page-heading">
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span> Measurement Orders</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','defectiveorder') }}"> Defective Orders</a></span>
                <span>></span>
                 <span class="active">Manage Measurement Orders</span>
              
            </div>
</div>
</div>
<div class="mng-customer-table">
    <table class="table table-bordered" id="data-table">
        <thead>
            <tr>
                <th>order_number</th>
                <th>item_number</th>
                <th>branch code</th>
                <th>salesman</th>
                <th>sewer</th>
                
                <th>Qc</th>
              
                <th>action</th>
            </tr>
        </thead>
    </table>
</div>
<input id="data-table-url" type="hidden" value="{!! route('measurement.data') !!}">

<script type="text/javascript" src="{{ asset('js/backend/measurementorder.js') }}"></script>
@stop