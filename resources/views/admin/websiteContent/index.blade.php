@extends('admin.layouts.app')
@section('title','Webpage Content')
@section('content')
<form method="POST" action="{{ url('admin/webpage/website_content_update') }}" id="website-create-form">
    {{ csrf_field() }}
    {{ method_field('POST') }}
    <div class="page-heading">
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span>Webpage Content</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span class="active">Webpage Content</span>
                
            </div>
           <button type="submit" class="btn btn-primary save_btn">
                 <i class="fa fa-floppy-o" aria-hidden="true"></i>{{ __('Save') }}
            </button>
        </div>
    </div> 
       
    <div class="handi-form p-l-res">
        <input type="hidden"  class="form-control has-error" id="title" name = "title"  value="home" />
        <div class="col-xs-12 col-sm-6 ">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">App Store<span></span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-error" id="apple" name = "apple"  value="{{isset($websiteContent->app_store)?$websiteContent->app_store:''}}" />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 ">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Play Store<span></span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-error" id="google" name = "google"  value="{{isset($websiteContent->play_store)?$websiteContent->play_store:''}}" />
                    </div>
                </div>
            </div>
        </div>
         <div class="col-xs-12 col-sm-6 col-md-6 form-group">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">BAM Content<span></span></label>
                    <div class="col-xs-12">
                    <textarea  class="form-control has-error" id="how_it_works" name = "how_it_works">{{isset($websiteContent->content)?$websiteContent->content:''}}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 form-group">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">BAM contact address<span></span></label>
                    <div class="col-xs-12">
                    <textarea  class="form-control has-error" id="lets_talk" name = "lets_talk">{{isset($websiteContent->address)?$websiteContent->address:''}}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</div>
@if(session()->has('success'))
    <script type="text/javascript">
        $.growl.notice({title: "Success!", message:  '{!! session('success') !!}' });
    </script>
@endif
@if(session()->has('error'))
    <script type="text/javascript">
        $.growl.error({title: "Oops!", message:  '{!! session('error') !!}' });
    </script>
@endif
<script>
    CKEDITOR.replace( 'how_it_works' );
    CKEDITOR.replace( 'lets_talk' );
</script>
@stop