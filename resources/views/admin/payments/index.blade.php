@extends('admin.layouts.app')
@section('title','Payments')
@section('content')
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Payments</span></h1>
        <div class="breadcrumb">
            <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
            <span>></span>
            <span class="active">Manage Payments</span>
        </div>
    </div>
</div>
<div class="handi-form">
    <div class="container-2">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="circle-tile">
                            <div class="circle-tile-heading orange">
                                <img src="{{ asset('images/backend/termsandcondition.svg') }}">
                            </div>
                        <div class="circle-tile-content orange">
                            <div class="circle-tile-description text-faded">
                                Total Order Amount<!-- All Users -->
                            </div>
                            <div class="circle-tile-number text-faded" id="counter">
                                SAR {{$totalAmount}}
                                <span id="sparklineB"></span>
                            </div>
                            <!-- <a href="{{ url('admin','users') }}" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a> -->
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="circle-tile">
                            <div class="circle-tile-heading green">
                                <img src="{{ asset('images/backend/charity.svg') }}">
                            </div>
                        <div class="circle-tile-content green">
                            <div class="circle-tile-description text-faded">
                                Total Paid Amount
                            </div>
                            <div class="circle-tile-number text-faded">
                                SAR {{$totalPaidAmount}}
                            </div>
                            <!-- <a href="{{ url('admin','currencies') }}" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a> -->
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="circle-tile">
                            <div class="circle-tile-heading red">
                                <img src="{{ asset('images/backend/currency.svg') }}">
                            </div>
                        <div class="circle-tile-content red">
                            <div class="circle-tile-description text-faded">
                                Total Remaining Amount
                            </div>
                            <div class="circle-tile-number text-faded" id="counter">
                                SAR {{$totalRemainingAmount}}
                                <span id="sparklineB"></span>
                            </div>
                            <!-- <a href="{{ url('admin','feeds') }}" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() 
    {
         window.history.pushState(null, "", window.location.href); 
            window.onpopstate = function() 
            { 
                window.history.pushState(null, "", window.location.href); 
            };
     });
     
</script>
@stop
