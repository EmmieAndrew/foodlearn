@extends('admin.layouts.app')
@section('title','Salary')
@section('content')
<form method="POST" id="changeModuleForm" enctype="multipart/form-data" action="{{ url('admin/salary/'.$salary->id) }}">
{{ csrf_field() }}
{{ method_field('PATCH') }}
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Salary</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','salary') }}">Salary</a></span>
                <span>></span>
                <span class="active">{{$heading}} Salary</span>
            </div>
            <div class="new-customer-btn cancel-button">
                <a class="btn btn-primary pull-right" href="{{ url('admin','salary') }}"> <i class="fa fa-times" aria-hidden="true"></i>Cancel</a>
            </div>
            <button type="submit" class="btn btn-primary save_btn">
                 <i class="fa fa-floppy-o" aria-hidden="true"></i>{{ __('Save') }}
            </button>
    </div>
</div>
<input type="hidden" name="id" id="id"  value="{{$salary->id}}">
<div class="handi-form p-l-res">
     <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Select Employee</label>
                <div class="col-xs-12">
                    <select id="emp_id" name = "emp_id" style="width: 100%;">
                        @foreach($getAllId as $key => $empData)
                        <option value="{{$key}}" {{ ( $key == $salary->emp_id) ? 'selected' : '' }}> {{$empData}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
     <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Salary</label>
                <div class="col-xs-12">
                <input type="text"  class="form-control has-error" id="salary" name = "salary"  value="{{$salary->salary}}" />
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<script type="text/javascript" src="{{ asset('js/backend/Salary.js') }}"></script>
@stop




