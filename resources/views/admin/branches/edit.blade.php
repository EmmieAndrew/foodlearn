@extends('admin.layouts.app')
@section('title','Branch')
@section('content')
<form method="POST" enctype="multipart/form-data" action="{{ url('admin/branches/'.$branch->id) }}" id="branches-edit-form">
    {{ csrf_field() }}
    {{ method_field('PATCH') }}
    <div class="page-heading">
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span>Branch</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','branches') }}">Branch</a></span>
                <span>></span>
                <span class="active">{{ (!empty($branch->name))?$branch->name:$heading }}</span>
            </div>
            <div class="new-customer-btn cancel-button">
                <a class="btn btn-primary pull-right" href="{{ url('admin','branches') }}"> <i class="fa fa-times" aria-hidden="true"></i>Cancel</a>
            </div>
            <button type="submit" class="btn btn-primary save_btn">
                 <i class="fa fa-floppy-o" aria-hidden="true"></i>Save
            </button>
        </div>
    </div>
    <!-- <div class="handi-form p-l-res"> -->
        <div class="col-xs-12">
            <h4 class="modal-subheading"><span>Basic Details</span></h4>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Branch Name<span>*</span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-error" id="branch" name = "branch" value="{{$branch->branch}}"/>
                        <p></p>
                        @if ($errors->has('branch'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('branch') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Branch Details</label>
                    <div class="col-xs-12">
                        <input type="text" class="form-control has-error" id="branch_details" name = "branch_details" value="{{$branch->branch_details}}" />
                        <p></p>
                        @if ($errors->has('branch_details'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('branch_details') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Factory<span>*</span></label>
                    <div class="col-xs-12">
                        <select class="form-control has-error" name="factory" id="factory">
                            <option>Select Factory</option>
                            @foreach($getFactory as $Factories => $Factory)
                            <option value="{{ $Factories }}" {{ ( $Factories == $branch->factory ) ? 'selected' : '' }}>
                            {{ $Factory }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Contact No.</label>
                    <div class="col-xs-12">
                        <input type="text" class="form-control has-error" id="phone" name = "phone" value="{{$branch->phone}}" />
                        <p></p>
                        @if ($errors->has('phone'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 ">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Select Salesman <span> As Manager *</span></label>
                    <div class="col-xs-12">
                    <select class="form-control" id="salesman_id" name = "salesman_id">
                        <option>Select Manager</option>
                        @foreach ($salesman as $key => $value)
                        <option value="{{ $key }}" {{ ( $key == $branch->salesman_id) ? 'selected' : '' }}> 
                        {{ $value }} 
                        </option>
                        @endforeach    
                    </select>  
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Branch target <span>*</span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-error" id="target" name = "target" value="{{$branch->target}}"/>
                    </div>
                </div>
            </div>
        </div>        
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Status <span>*</span></label>
                    <div class="col-xs-12">
                        <select class="form-control has-error" id="status" name = "status">
                            <option {{ $branch->status ? 'selected' : '' }} value="1">Active</option>
                            <option {{ !$branch->status ? 'selected' : '' }} value="0">Inactive</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="formAction" value="edit" />
        <input type="hidden" id="branch_id" value="{{$branch->id}}" />

    </form>
</div>


<link rel="stylesheet" type="text/css" href="{{ asset('css/imgareaselect/imgareaselect-default.css') }}" />

<script type="text/javascript" src="{{ asset('js/backend/branches.js')}}"></script>
@stop