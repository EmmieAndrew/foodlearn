@extends('admin.layouts.app')
@section('title','Branches')
@section('content')
    <div class="page-heading">
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span>Branches</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','Branches') }}">Branches</a></span>
                <span>></span>
                <span class="active">{{ (!empty($branch->branch))?$branch->branch:$heading }}</span>
            </div>
            
        </div>
    </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Branch</label>
                    <div class="col-xs-12">
                        <p>{{ (!empty($branch->branch))?ucwords($branch->branch):'-' }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Branch Details</label>
                    <div class="col-xs-12 varifiedtxtblock">
                        <p>{{ (!empty($branch->branch_details))?$branch->branch_details:'-' }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Factory</label>
                    <div class="col-xs-12 varifiedtxtblock">
                        <p>{{ (!empty($getFactory->name))?$getFactory->name:'-' }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Salesman Assigned</label>
                    <div class="col-xs-12">
                        <p>{{ (!empty($getSalesman->title))?$getSalesman->title:'-' }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Driver Assigned</label>
                    <div class="col-xs-12">
                        <p>{{(!empty($getDriver->title))?$getDriver->title:'-' }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Cutter Assigned</label>
                    <div class="col-xs-12">
                        <p>{{(!empty($getCutter->title))?$getCutter->title:'-' }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Branch Target</label>
                    <div class="col-xs-12">
                        <p>{{ (!empty($branch->target))?$branch->target:'-' }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Status</label>
                    <div class="col-xs-12">
                        <p>{{ $branch->status ? 'Active' : 'Inactive' }}</p>
                    </div>
                </div>
            </div>
        </div>

@stop