@extends('admin.layouts.app')
@section('title','Suppliers')
@section('content')
<form method="POST" id="changeModuleForm" enctype="multipart/form-data" action="{{ url('admin/charities/'.$charity->id) }}">
{{ csrf_field() }}
{{ method_field('PATCH') }}
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Suppliers</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','charities') }}">Suppliers</a></span>
                <span>></span>
                <span class="active">{{$heading}} Suppliers</span>
            </div>
            <div class="new-customer-btn cancel-button">
                <a class="btn btn-primary pull-right" href="{{ url('admin','charities') }}"> <i class="fa fa-times" aria-hidden="true"></i>Cancel</a>
            </div>
            <button type="submit" class="btn btn-primary save_btn">
                 <i class="fa fa-floppy-o" aria-hidden="true"></i>{{ __('Save') }}
            </button>
    </div>
</div>
<input type="hidden" name="id" id="id"  value="{{$charity->id}}">
<div class="handi-form p-l-res">
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Title <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="title" name = "title"  value="{{$charity->title}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('title'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('title') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Status<span></span></label>
                <div class="col-xs-12">
                    <select class="form-control has-error" id="status" name = "status">
                        <?php
                            if($charity->status==1) 
                            { 
                            echo "<option value='1' selected>Active</option>
                                    <option value='0'>Inactive</option>"; 
                            } 
                            else
                            {
                            echo "<option value='1' >Active</option>
                                   <option value='0' selected>Inactive</option>"; 
                            }
                        ?>                            
                    </select> 
                    <p class="allTypeError"></p>
                    @if ($errors->has('status'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
   <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Telephone<span></span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="telephone" name = "telephone"  value="{{$charity->telephone}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('telephone'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('telephone') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Fax<span></span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="fax" name = "fax"  value="{{$charity->fax}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('fax'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('fax') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Email<span>*</span></label>
                <div class="col-xs-12">
                    <input type="text" class="form-control has-error" id="email" name = "email"  value="{{$charity->email}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Password<span>*</span></label>
                <div class="col-xs-12">
                    <input type="password"  class="form-control has-error" id="password" name = "password" value="{{$charity->tmp_name}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

     <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Address<span></span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="address" name = "address"  value="{{$charity->address}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('address'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('address') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
           <div class="form-group error">
               <label for="inputName" class="col-xs-12 control-label">URL<span></span></label>
               <div class="col-xs-12">
                   <input type="text"  class="form-control has-error" id="link" name = "link"  value="{{$charity->link}}" />
                   <p class="allTypeError"></p> 
                   @if ($errors->has('link'))
                   <span class="invalid-feedback">
                       <strong id="email-server-err-exists">{{ $errors->first('link') }}</strong>
                   </span>
                   @endif
               </div>
           </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Description <span>*</span></label>
                <div class="col-xs-12">
                    <textarea  class="form-control has-error" id="description" name = "description">{{$charity->description}}</textarea>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('description'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('description') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-xs-12 col-sm-12 col-md-12 ">
        <div class="row">
            <div class="col-xs-12 col-sm-12 img_upload-div ">
                <div class="form-group error">
                <label  class="control-label" for="logo">Supplier Logo <span>*</span></label>
                <div class="input-cover-modal">
                    <div class="image_preview error-outer">
                     
                        <img id="show-image-preview" src="{{ (!empty($charity->logo) && file_exists(public_path('uploads/charities/'.$charity->logo)))?asset('uploads/charities/'.$charity->logo):asset('images/common/default-image.jpg') }}" alt="" />
                        <div class="choose_file">
                            <input title="" type='file' id="logo" name="logo" />
                            <p><i class="fa fa-upload" aria-hidden="true"></i></p>
                        </div>
                        <input type="hidden" name="image-exist" id="image-exist" value="{{ (!empty($charity->logo))?'yes':'no' }}">
                    </div>
                    <p class="allTypeError"></p> 
                    <span id="charity-logo-err" class="invalid-feedback">
                        @if ($errors->has('logo'))
                            <strong>{{ $errors->first('logo') }}</strong>
                        @endif
                    </span>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<script type="text/javascript" src="{{ asset('js/backend/charities.js') }}"></script>
<script>
    CKEDITOR.replace( 'description' );
</script>
@stop




