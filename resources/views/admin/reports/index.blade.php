@extends('admin.layouts.app')
@section('title','Reports')
@section('content')
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Reports</span></h1>
        <div class="breadcrumb">
            <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
            <span>></span>
            <span class="active">Manage Reports</span>
        </div>
    </div>
</div>

<div class="handi-form">
    <div class="container-2">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="circle-tile">
                        <h4 class="card-title">Branch Report</h4>
                        <div class="circle-tile-content gray">
                            <div class="col-md-12">
                                <div class="circle-tile-description text-faded">
                                    <input class="form-control input-daterange-datepicker" type="text" name="daterange" value="{{date('m/d/Y')}}">
                                </div>
                            </div>
                            <div class="circle-tile-number text-faded">
                                <a class="btn btn-info" href="#">Export Branch</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="circle-tile">
                        <h4 class="card-title">Order Report</h4>
                        <div class="circle-tile-content black">
                            <div class="col-md-12">
                                <div class="circle-tile-description text-faded">
                                    <input class="form-control input-daterange-fororder" type="text" name="daterange" value="{{date('m/d/Y')}}">
                                </div>
                            </div>
                            <div class="circle-tile-number text-faded">
                                <a class="btn btn-info" href="#">Export Order</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="circle-tile">
                        <h4 class="card-title">Customer Report</h4>
                        <div class="circle-tile-content gray">
                            <div class="col-md-12">
                                <div class="circle-tile-description text-faded">
                                    <input class="form-control input-daterange-forcustomer" type="text" name="daterange" value="{{date('m/d/Y')}}">
                                </div>
                            </div>
                            <div class="circle-tile-number text-faded">
                                <a class="btn btn-info" href="#">Export Customer</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('custom-scripts')
<script type="text/javascript">
    $('.input-daterange-datepicker').daterangepicker({
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });

    $('.input-daterange-forcustomer').daterangepicker({
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });

    $('.input-daterange-fororder').daterangepicker({
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });    
</script>
@endpush
<script>
    $(document).ready(function() 
    {
         window.history.pushState(null, "", window.location.href); 
            window.onpopstate = function() 
            { 
                window.history.pushState(null, "", window.location.href); 
            };
     });
     
</script>
@stop
