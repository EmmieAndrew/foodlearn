@extends('admin.layouts.app')
@section('title', 'Registration')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('admin.addAdmin') }}">
                        {{ csrf_field() }}
                        <div class="page-heading">
                            <div class="pageheding-inner">
                                <h1 class="page-common-head"><span>Admin</span></h1>
                                <div class="breadcrumb">
                                    <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                                    <span>></span>
                                    <span class="active">Create Admin</span>
                                </div>
                                <div class="new-customer-btn cancel-button">
                                    <a class="btn btn-primary pull-right" href="{{ url('admin','dashboard') }}"> <i class="fa fa-times" aria-hidden="true"></i>Cancel</a>
                                </div>
                                <button type="submit" class="btn btn-primary save_btn">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i>Save
                                </button>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <h4 class="modal-subheading"><span>Basic Details</span></h4>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="row">
                                <div class="form-group error">
                                    <label for="inputName" class="col-xs-12 control-label">Name <span>*</span></label>
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control has-error" id="name" name="name" value="" />
                                        <p></p>
                                        @if ($errors->has('name'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="row">
                                <div class="form-group error">
                                    <label for="inputName" class="col-xs-12 control-label">Email <span>*</span></label>
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control has-error" id="email" name="email" value="" />
                                        <p></p>
                                        @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="row">
                                <div class="form-group error">
                                    <label for="inputName" class="col-xs-12 control-label">Phone <span>*</span></label>
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control has-error" id="phone" name="phone" value="" />
                                        <p></p>
                                        @if ($errors->has('phone'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="row">
                                <div class="form-group error">
                                    <label for="inputName" class="col-xs-12 control-label">Address <span>*</span></label>
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control has-error" id="address" name="address" value="" />
                                        <p></p>
                                        @if ($errors->has('address'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="row">
                                <div class="form-group error">
                                    <label for="inputName" class="col-xs-12 control-label">Password <span>*</span></label>
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control has-error" id="password" name="password" value="" />
                                        <p></p>
                                        @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="row">
                                <div class="form-group error">
                                    <label for="inputName" class="col-xs-12 control-label">Confirm Password<span>*</span></label>
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control has-error" id="confirm_password" name="confirm_password" value="" />
                                        <p></p>
                                        @if ($errors->has('confirm_password'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('confirm_password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
