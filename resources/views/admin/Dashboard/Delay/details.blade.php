@extends('admin.layouts.app')
@section('title','Details')
@section('content')
    <div class="page-heading">
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span>Order</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','delay') }}"> Delay Orders</a></span>
                <span>></span>
                <span class="active">{{ (!empty($order->first()->order_unique_id))?$order->first()->order_unique_id:$heading }}</span>
            </div>
            <div class="pull-right">
                @php

                    if(!empty($order->first()->AssignmentData->assigned_at)){

                        $status = '<span class="label label-info">With Cutter</span>';

                        if(!empty($order->first()->AssignmentData->tailor_assigned_at)){

                            $status = '<span class="label label-info">With Tailor</span>';

                        }
                        if(!empty($order->first()->AssignmentData->qc_assigned_at)){

                            $status = '<span class="label label-info">With QC</span>';

                        }
                        if(!empty($order->first()->AssignmentData->order_status == 0)){

                            $status = '<span class="label label-danger">Defective By QC</span>';

                        }
                        if(!empty($order->first()->AssignmentData->qc_assigned_at) && !empty($order->first()->AssignmentData->order_status == 1)){

                            $status = '<span class="label label-warning">Completed</span>';

                        }
                        if(!empty($order->first()->delivered_at)){
                            if($order->first()->delivery =='1' && $order->first()->price_remaining == '0'){
                                $status = '<span class="label label-success">Delivered To Home</span>';

                            }elseif($order->first()->delivery =='1'){

                                $status = '<span class="label label-success">Delivered To Branch</span>';
                            }else{

                                $status = '<span class="label label-success">Delivered To Home</span>';
                            }

                        }                        

                    }else{
                        $status = '<span class="label label-warning">Ready To Assign</span>';
                    }

                @endphp
                <p style="text-align: center;"><strong>Current Stage:</strong> {!!$status!!}</p>
            </div>
            <div class="new-customer-btn single-btn">
                <a class="btn btn-primary pull-right" id="show-add-form" data-toggle="modal" data-target="#commentModal"><i class="fa fa-eye" aria-hidden="true"></i> Comments</a>
            </div>            
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="commentModal" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">View All Comments</h4>
            </div>
            <div class="modal-body">
            @forelse($order->first()->getComments as $comments)
                <div class="col-sm-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong>{{($comments->is_admin)?'Admin' :$comments->commentBy['title']}}</strong> <span class="text-muted">commented on {{\Carbon\Carbon::parse($comments->created_at)->format('d M Y g:i A')}}</span>
                        </div>
                        <div class="panel-body">
                            {{$comments->comment}}
                        </div><!-- /panel-body -->
                    </div><!-- /panel panel-default -->
                </div>
                <span class="success" style="display: none; color: green">Comment added</span>

                @if($comments->comment_image)
                <div class="col-sm-4">
                    <div class="panel panel-default">
                        <img src="{{asset('uploads/comments').'/'.$comments->comment_image}}" class="img-thumbnail">
                    </div>
                </div>
                @endif

            @empty

                <p>No comment</p>

            @endforelse
            </div>
            <div class="modal-footer">
                <form>
                    <input type="text" class="col-sm-10" name="comment" id="admin-comment">
                    <input type="hidden" name="order_unique_id" value="{{$order->first()->order_unique_id}}">
                    <input type="hidden" name="user_id" value="{{Auth::id()}}">
                    <input type="hidden" name="is_admin" value="yes">
                    <button type="button" class="btn btn-default btn-submit">Post</button>
                </form>
            </div>
          </div>
          
        </div>
    </div>
    <!-- End Modal -->
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="row">
            <div class="col-md-6 col-lg-3 col-xlg-3">
            <label for="inputName">Customer Detail:</label>                        
                <div class="card card-body">
                    <div class="row align-items-center">
                        <div class="col-md-12 col-lg-12">
                            <p>Name: {{ (!empty($order->first()->customer_name))?strtoupper($order->first()->customer_name):'-' }}</p>
                            <p>Id: {{ (!empty($order->first()->customer_unique_id))?$order->first()->customer_unique_id:'-' }}</p>
                            <p>Phone No: {{ (!empty($order->first()->customer_id))?$order->first()->customerData->phone:'-' }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 col-xlg-3">
            <label for="inputName">Delivery Detail:</label>
                <div class="card card-body">
                    <div class="row align-items-center">
                        <div class="col-md-12 col-lg-12">
                            <p>Expected Delivery: {{ (!empty($order->first()->expected_delivery_time))? \Carbon\Carbon::parse($order->first()->expected_delivery_time)->format('d M Y'):'-' }}</p>
                            <p>Delivered On: {{ (!empty($order->first()->delivered_at))? \Carbon\Carbon::parse($order->first()->delivered_at)->format('d M Y'):'Not Delivered Yet' }}</p>
                            @if($order->first()->delivery =='1')
                                <p>Delivery To Home: No</p>
                            @else
                                <p>Delivery To Home: Yes</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 col-xlg-3">
            <label for="inputName">Payment Detail:</label>
                <div class="card card-body">
                    <div class="row align-items-center">
                        <div class="col-md-12 col-lg-12">
                            <p>Total Amount: SAR {{ (!empty($order->first()->total_amount))? number_format( $order->first()->total_amount,2, ".", ","):'0' }} ({{($order->first()->payment_method)?$order->first()->payment_method:'-'}})</p>

                            <p>Paid Amount: SAR {{ (!empty($order->first()->amount_to_pay))? number_format($order->first()->amount_to_pay,2, ".", ","):'0' }}</p>
                            
                            @if(!empty($order->first()->price_remaining))
                                <p>Remining Amount: SAR {{ (!empty($order->first()->price_remaining))? number_format( $order->first()->price_remaining,2, ".", ","):'0' }}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 col-xlg-3">
            <label for="inputName">Other Detail:</label>
                <div class="card card-body">
                    <div class="row align-items-center">
                        <div class="col-md-12 col-lg-12">
                            <p>Order Id: {{ (!empty($order->first()->order_unique_id))? $order->first()->order_unique_id:'-' }}</p>
                            <p>Order Date: {{ (!empty($order->first()->created_at))? \Carbon\Carbon::parse($order->first()->created_at)->format('d M Y'):'-' }}</p>
                            <p>Salesman: {{ (!empty($order->first()->order_by))? $order->first()->orderBy['title']:'-' }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div></br>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="row">
            <div class="col-md-6 col-lg-3 col-xlg-3">
            <label for="inputName">Track Order:</label>                        
                <div class="card card-body">
                    <div class="row align-items-center">
                        <div class="col-md-12 col-lg-12">
                            <p>Order Date: {{ (!empty($order->first()->created_at))? \Carbon\Carbon::parse($order->first()->created_at)->format('d M Y g:i A'):'-' }}</p>
                            <p>Salesman: {{ (!empty($order->first()->order_by))? $order->first()->orderBy['title']:'-' }}</p>
                            <p>Assign Cutter: {{ (!empty($order->first()->AssignmentData->assigned_at))? \Carbon\Carbon::parse($order->first()->AssignmentData->assigned_at)->format('d M Y g:i A'):'-' }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 col-xlg-3">
            <label for="inputName"></label>
                <div class="card card-body">
                    <div class="row align-items-center">
                        <div class="col-md-12 col-lg-12">
                            <p>Cutter: {{ (!empty($order->first()->AssignmentData->cutter_id))? $order->first()->AssignmentData->getCuter['title']:'-' }}</p>
                            <p>Completed: {{ (!empty($order->first()->AssignmentData->cutter_completion_date))? \Carbon\Carbon::parse($order->first()->AssignmentData->cutter_completion_date)->format('d M Y g:i A'):'-' }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 col-xlg-3">
            <label for="inputName"></label>
                <div class="card card-body">
                    <div class="row align-items-center">
                        <div class="col-md-12 col-lg-12">
                            <p>Tailor: {{ (!empty($order->first()->AssignmentData->tailor_id))? $order->first()->AssignmentData->getTailor['name']:'-' }}</p>
                            <p>Assign Tailor: {{ (!empty($order->first()->AssignmentData->tailor_assigned_at))? \Carbon\Carbon::parse($order->first()->AssignmentData->tailor_assigned_at)->format('d M Y g:i A'):'-' }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 col-xlg-3">
            <label for="inputName"></label>
                <div class="card card-body">
                    <div class="row align-items-center">
                        <div class="col-md-12 col-lg-12">
                            <p>QC: {{ (!empty($order->first()->AssignmentData->qc_id))? $order->first()->AssignmentData->getQc['title']:'-' }}</p>
                            <p>Assign QC: {{ (!empty($order->first()->AssignmentData->qc_assigned_at))? \Carbon\Carbon::parse($order->first()->AssignmentData->qc_assigned_at)->format('d M Y g:i A'):'-' }}</p>
                            @if(!empty($order->first()->AssignmentData->order_status) &&$order->first()->AssignmentData->order_status == '1')
                                <p>QC Status: <span class="label label-success">Passed</span></p>
                                <p>Completed At: {{ (!empty($order->first()->AssignmentData->qc_passed_at))? \Carbon\Carbon::parse($order->first()->AssignmentData->qc_passed_at)->format('d M Y g:i A'):'-' }}</p>
                            @else
                                <p>QC Status: <span class="label label-danger">Defective</span></p>
                                <p>Defective At: {{ (!empty($order->first()->AssignmentData->qc_passed_at))? \Carbon\Carbon::parse($order->first()->AssignmentData->qc_passed_at)->format('d M Y g:i A'):'-' }}</p>

                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div><br>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Order Detail:</label>
                @foreach($order as $order)

                    @if($order->status == '5')
                        @push('custom-scripts')
                        <style type="text/css">
                            .productDetails{
                                background-image: url("{{asset('/images/backend/defective.jpg')}}");
                                background-size: 80px 80px;
                                background-repeat: no-repeat;
                                background-position: top center;
                            }
                        </style>
                        @endpush
                    @endif

                    @if($order->behaviour == 'Internal')
                        <div class="col-md-6 col-lg-4 col-xlg-4">
                            <div class="card card-body">
                                <div class="row align-items-center">
                                    <div class="col-md-12 col-lg-12 productDetails">
                                        <h4 class="box-title m-b-0">{{$order->product_name}}</h4> <small>Code: {{$order->product_code}}</small>
                                        <address>
                                            <p>Length: {{ (!empty($order->product_length))?$order->product_length:'-' }}</p>
                                            <p>Quantity: {{ (!empty($order->quantity))?$order->quantity:'-' }}</p>
                                            <p>Sketch: 
                                                <a href="{{asset('/uploads/orderSketch/'.$order->sketch)}}" target="_blank">
                                                {{ (!empty($order->sketch))?'View Sketch':'-' }}</a></p>
                                            <p>Model: {{ (!empty($order->model))?$order->ProductModel['name']:'-' }}</p>

                                            @if($order->status == '1')
                                                <p>Status: {{'Received'}}</p>
                                            @elseif($order->status == '2')
                                                <p>Status: {{'Pending'}}</p>
                                            @elseif($order->status == '3')
                                                <p>Status: {{'Delivered'}}</p>
                                            @elseif($order->status == '4')
                                                <p>Status: {{'Reorder'}}</p>
                                            @elseif($order->status == '5')
                                                <p>Status: {{'Defective'}}</p>
                                            @else
                                                <p>Status: -</p>
                                            @endif
                                            <br>
                                        </address>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-md-6 col-lg-4 col-xlg-4">
                            <div class="card card-body">
                                <div class="row align-items-center">
                                    <div class="col-md-12 col-lg-12 productDetails">
                                        <h4 class="box-title m-b-0">{{'External'}}</h4> <small>Code: {{'External'}}</small>
                                        <address>
                                            <p>Length: {{ (!empty($order->product_length))?$order->product_length:'-' }}</p>
                                            <p>Quantity: {{ (!empty($order->quantity))?$order->quantity:'-' }}</p>
                                            <p>Sketch: 
                                                <a href="{{asset('/uploads/orderSketch/'.$order->sketch)}}" target="_blank">
                                                {{ (!empty($order->sketch))?'View Sketch':'-' }}</a></p>
                                            <p>Model: {{ (!empty($order->model))?$order->ProductModel['name']:'-' }}</p>

                                            @if($order->status == '1')
                                                <p>Status: {{'Received'}}</p>
                                            @elseif($order->status == '2')
                                                <p>Status: {{'Pending'}}</p>
                                            @elseif($order->status == '3')
                                                <p>Status: {{'Delivered'}}</p>
                                            @elseif($order->status == '4')
                                                <p>Status: {{'Reorder'}}</p>
                                            @elseif($order->status == '5')
                                                <p>Status: {{'Defective'}}</p>
                                            @else
                                                <p>Status: -</p>
                                            @endif                                                
                                            <br>
                                        </address>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    <div class="clearfix"></div></br>
    <script type="text/javascript" src="{{ asset('js/backend/orders.js') }}"></script>
@stop