@extends('admin.layouts.app')
@section('title','Delay Orders')
@section('content')
    <div class="page-heading">
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span> Delay Order</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','delay') }}"> Delay Orders</a></span>
                <span>></span>
                 <span class="active">Manage Delay Orders</span>
              
            </div>
</div>
</div>
<div class="mng-customer-table">
    <table class="table table-bordered" id="data-table">
        <thead>
            <tr>
                <th>order_number</th>
                <th>branch</th>
                <th>order_date</th>
                <th>status</th>
                <th>action</th>
            </tr>
        </thead>
    </table>
</div>
<input id="data-table-url" type="hidden" value="{!! route('delay.data') !!}">

<script type="text/javascript" src="{{ asset('js/backend/delay.js') }}"></script>
@stop