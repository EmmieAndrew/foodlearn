@extends('admin.layouts.app')
@section('title','GoodTime')
@section('content')
<section class="common-banner"> 
    <img src="images/web/banner-bg.jpg">
    <div class="c-banner-text">
        <div class="c-banner-inner">
            <h3>No Page<span> Found</span></h3>
        </div>
    </div>
</section>
<section class="error-page">
    <div class="wrapper">
    <div class="errormsg">
        <div class="errordiv404">
            <h1>4<span>0</span>4</h1>
        </div>
        <div class="errortxt">
            <h2>Page not found</h2>
            <p>Are you lost somewhere?</p>
            <div class="gotohome"><a ui-sref="home" href="/">GO BACK TO HOME</a></div>
        </div>
    </div>
    </div>
</section>
@stop