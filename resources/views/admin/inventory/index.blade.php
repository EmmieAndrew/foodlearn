@extends('admin.layouts.app')
@section('title','Inventory')
@section('content')
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Inventory Request</span></h1>
        <div class="breadcrumb">
            <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
            <span>></span>
            <span class="active">Manage Inventory</span>
        </div>
    </div>
</div>

<div class="mng-customer-table">
    <table class="table table-bordered" id="data-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Product Image</th>
                <th>Product Name</th>
                <th>Product Code</th>
                <th>Request Comment</th>
                <th>Request By</th>
                <th>Created On</th>
            </tr>
        </thead>
    </table>
</div>

<input id="data-table-url" type="hidden" value="{!! route('inventory.data') !!}">

<script type="text/javascript" src="{{ asset('js/backend/inventory.js')}}"></script>

@if(session()->has('success'))
    <script type="text/javascript">
        $.growl.notice({title: "Success!", message:  'Success' });
    </script>
@endif
@if(session()->has('error'))
    <script type="text/javascript">
        $.growl.error({title: "Oops!", message:  'Error' });
    </script>
@endif
@stop