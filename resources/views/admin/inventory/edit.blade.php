@extends('admin.layouts.app')
@section('title','Catalogues')
@section('content')
<form method="POST" enctype="multipart/form-data" action="{{ url('admin/catalogues/'.$products->id) }}" id="catalogues-edit-form">
    {{ csrf_field() }}
    {{ method_field('PATCH') }}
    <div class="page-heading">
        <div class="pageheding-inner">
            <h1 class="page-common-head"><span>catalogue</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','catalogues') }}">catalogues</a></span>
                <span>></span>
                <span class="active">{{ (!empty($products->product_name))?$products->product_name:$heading }}</span>
            </div>
            <div class="new-customer-btn cancel-button">
                <a class="btn btn-primary pull-right" href="{{ url('admin','catalogues') }}"> <i class="fa fa-times" aria-hidden="true"></i>Cancel</a>
            </div>
            <button type="submit" class="btn btn-primary save_btn">
                 <i class="fa fa-floppy-o" aria-hidden="true"></i>Save
            </button>
        </div>
    </div>
    <!-- <div class="handi-form p-l-res"> -->
        <div class="col-xs-12">
            <h4 class="modal-subheading"><span>Basic Details</span></h4>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Product Name <span>*</span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-error" id="product_name" name = "product_name" value="{{$products->product_name}}"/>
                        <p></p>
                        @if ($errors->has('product_name'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('product_name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Product Type</label>
                    <div class="col-xs-12">
                        <input type="text" class="form-control has-error" id="product_type" name = "product_type" value="{{$products->product_type}}" />
                        <p></p>
                        @if ($errors->has('product_type'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('product_type') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Product Details<span>*</span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-error" id="product_details" name = "product_details" value="{{$products->product_details}}"/>
                        <p></p>
                        @if ($errors->has('product_details'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('product_details') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        
       
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Product Descreption<span>*</span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-error" id="product_description" name = "product_description" value="{{$products->product_description}}"/>
                        <p></p>
                        @if ($errors->has('product_description'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('product_description') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Product Length<span>*</span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-error" id="product_length" name = "product_length" value="{{$products->product_length}}"/>
                        <p></p>
                        @if ($errors->has('product_length'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('product_length') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="form-group error">
                    <label for="inputName" class="col-xs-12 control-label">Product Price<span>*</span></label>
                    <div class="col-xs-12">
                        <input type="text"  class="form-control has-error" id="product_price" name = "product_price" value="{{$products->product_price}}"/>
                        <p></p>
                        @if ($errors->has('product_price'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('product_price') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 ">
        <div class="row">
            <div class="col-xs-12 col-sm-12 img_upload-div ">
                <div class="form-group error">
                <label  class="control-label" for="product_image">Image</label>
                <div class="input-cover-modal">
                    <div class="image_preview error-outer">
                     
                        <img id="show-image-preview" src="{{ (!empty($products->product_image) && file_exists(public_path('uploads/salesmans/'.$products->product_image)))?asset('uploads/salesmans/'.$products->product_image):asset('images/common/default-image.jpg') }}" alt="" />
                        <div class="choose_file">
                            <input title="" type='file' id="product_image" name="product_image" />
                            <p><i class="fa fa-upload" aria-hidden="true"></i></p>
                        </div>
                        <input type="hidden" name="image-exist" id="image-exist" val = "no">
                    </div>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('product_image'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('product_image') }}</strong>
                    </span>
                    @endif
                </div>
                </div>
            </div>
        </div>
    </div>
      
        <input type="hidden" id="formAction" value="edit" />
        <input type="hidden" id="product_id" value="{{$products->id}}" />

    </form>
</div>


<link rel="stylesheet" type="text/css" href="{{ asset('css/imgareaselect/imgareaselect-default.css') }}" />

<script type="text/javascript" src="{{ asset('js/backend/catalogues.js')}}"></script>
@stop