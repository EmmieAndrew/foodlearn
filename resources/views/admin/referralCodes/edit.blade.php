@extends('admin.layouts.app')
@section('title','Promo Codes')
@section('content')
<form method="POST" id="changeModuleForm" enctype="multipart/form-data" action="{{ url('admin/referralCodes/'.$referralCode->id) }}">
{{ csrf_field() }}
{{ method_field('PATCH') }}
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Promo Codes</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','referralCodes') }}">Referral Code</a></span>
                <span>></span>
                <span class="active">{{$heading}} Referral Code</span>
            </div>
            <div class="new-customer-btn cancel-button">
                <a class="btn btn-primary pull-right" href="{{ url('admin','referralCodes') }}"> <i class="fa fa-times" aria-hidden="true"></i>Cancel</a>
            </div>
            <button type="submit" class="btn btn-primary save_btn">
                 <i class="fa fa-floppy-o" aria-hidden="true"></i>{{ __('Save') }}
            </button>
    </div>
</div>
<input type="hidden" name="id" id="id"  value="{{$referralCode->id}}">
<div class="handi-form p-l-res">
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Name <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="name" name = "name"  value="{{$referralCode->name}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('name'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Email <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="email" name = "email"  value="{{$referralCode->email}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Code <span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="code" name = "code"  value="{{$referralCode->code}}" />
                    <p class="allTypeError"></p> 
                    @if ($errors->has('code'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('code') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Status <span>*</span></label>
                <div class="col-xs-12">
                    <select class="form-control has-error" id="status" name = "status">
                        <?php
                            if($referralCode->status==1) 
                            { 
                            echo "<option value='1' selected>Active</option>
                                    <option value='0'>Inactive</option>"; 
                            } 
                            else
                            {
                            echo "<option value='1' >Active</option>
                                   <option value='0' selected>Inactive</option>"; 
                            }
                        ?>                            
                    </select> 
                    <p class="allTypeError"></p>
                    @if ($errors->has('status'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
      <!--   <input type="hidden" name="referral_id" id="referral_id" value="{{$referralCode->id}}" /> -->
    </div>
    
  </div>
</form>
<script type="text/javascript" src="{{ asset('js/backend/referralCodes.js') }}"></script>
@stop




