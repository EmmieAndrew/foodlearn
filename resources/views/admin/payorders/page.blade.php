@extends('admin.layouts.app')
@section('title','Purchase Orders')
@section('content')
<form method="POST" id="changeModuleForm" enctype="multipart/form-data" action="{{ url('admin/payorders/'.$payorders->id) }}">
{{ csrf_field() }}
{{ method_field('PATCH') }}
<div class="page-heading">
    <div class="pageheding-inner">
        <h1 class="page-common-head"><span>Purchase Orders</span></h1>
            <div class="breadcrumb">
                <span><a href="{{ url('admin','dashboard') }}">Dashboard</a></span>
                <span>></span>
                <span><a href="{{ url('admin','payorders') }}">Purchase Orders</a></span>
                <span>></span>
                <span class="active">{{$heading}} Purchase Orders</span>
            </div>
            <div class="new-customer-btn cancel-button">
                <a class="btn btn-primary pull-right" href="{{ url('admin','payorders') }}"> <i class="fa fa-times" aria-hidden="true"></i>Back</a>
            </div>
            <button type="submit" class="btn btn-primary save_btn">
                 <i class="fa fa-floppy-o" aria-hidden="true"></i>{{ __('__Create') }}
            </button>
    </div>
</div>
<!-- <input type="hidden" name="id" id="id"  value="{{$payorders->id}}"> -->
   <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Product code<span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="product_code" name ="product_code"  value="{{$payorders->product_code}}" placeholder="Product code"/>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('product_code'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('product_code') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Product name<span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="product_name" name ="product_name"  value="{{$payorders->product_name}}"  placeholder="Product name"/>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('product_name'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('product_name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Quantity<span> in meter *</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="quantity" name ="quantity"  value="{{$payorders->quantity}}" placeholder="Quantity in meter"/>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('quantity'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('quantity') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Purchase Unit price<span> per meter *</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="purchase_unit_price" name ="purchase_unit_price"  value="{{$payorders->purchase_unit_price}}" placeholder="0.00"/>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('purchase_unit_price'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('purchase_unit_price') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Selling Unit price<span> per meter *</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="selling_unit_price" name ="selling_unit_price"  value="{{$payorders->selling_unit_price}}" placeholder="0.00"/>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('selling_unit_price'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('selling_unit_price') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Tax<span> in percentage *</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error" id="tax" name ="tax"  value="{{$payorders->tax}}" placeholder="0.00"/>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('tax'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('tax') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Purchase amount</label>
                <div class="col-xs-12">
                <input type="text"  class="form-control has-error" id="purchase_amount" name = "purchase_amount"  value="{{$payorders->purchase_amount}}" placeholder="0.00" readonly />
                    
                   
                </div>
            </div>
        </div>
    </div>
     <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Paid amount</label>
                <div class="col-xs-12">
                <input type="text"  class="form-control has-error" id="paid_amount" name = "paid_amount"  value="{{$payorders->paid_amount}}" placeholder="0.00"/>
                    
                   
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Remaining amount</label>
                <div class="col-xs-12">
                <input type="text"  class="form-control has-error" id="remaining_amount" name ="remaining_amount"  value="{{$payorders->remaining_amount}}" placeholder="0.00" readonly />
                </div>
            </div>
        </div>
    </div>       
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Due date<span>*</span></label>
                <div class="col-xs-12">
                    <input type="text"  class="form-control has-error duedatepicker" id="due_date" name ="due_date"  value="{{($payorders->due_date)? \Carbon\Carbon::parse($payorders->due_date)->format('m/d/Y') :date('m/d/Y')}}" placeholder="due date"/>
                    <p class="allTypeError"></p> 
                    @if ($errors->has('due_date'))
                    <span class="invalid-feedback">
                        <strong id="email-server-err-exists">{{ $errors->first('due_date') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Note</label>
                <div class="col-xs-12">
                <input type="text"  class="form-control has-error" id="note" name="note"  value="{{$payorders->note}}" placeholder="write note here..."/>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="row">
            <div class="form-group error">
                <label for="inputName" class="col-xs-12 control-label">Attachment</label>
                <div class="col-xs-12">
                <input type="file"  class="form-control has-error" id="image" name="image"/>
                <a href="{{asset('uploads/payorders/'.$payorders->image)}}" target="_blank">{{($payorders->image)? 'View attachment': '--'}}</a>
                </div>
            </div>
        </div>
    </div>