<h1>Introduction:</h1><br>
BAM (“BAM”, "Reda Khass", “we”, “us” and “our”), a company established in the Kingdom Of Saudi Arabia, with trade license number 1010362682 of Reda Khass LLC 6311 prince muhammad ibn saad ibn abdulazizi - Al Malqa Dist. RIYADH 13524 - 2768 - KSA operates www.BAMThobe.com (the “Site”). This Privacy Policy explains how we will use the information that we collect about you when you use this Site. Your use of this Site will also be subject to our Terms of Use and, if you purchase any Product, our Returns Policy. Terms defined in the Terms of Use will also be applicable to this Privacy Policy.
1) Our privacy policy describes the ways in which we collect, store, use and protect your personal information and it is important for you to review this privacy policy.
By "personal information" we mean information that can be associated with a specific person and can be used to identify that person.
2) We do not consider anonymized information to constitute personal information as it cannot be used to identify a specific person. We collect personal information from you when you use BAMThobe mobile application or its related websites and services ("App").
By providing us with your personal information you expressly consent to us processing your personal information in accordance with the terms of our privacy policy.
3) We may amend our privacy policy at any time by posting a revised version on the App.
The revised version will be effective at the time we post it and, following such posting, your continued use of the App will constitute your express consent to us continuing to process your personal information in accordance with the terms of our revised privacy policy.
Our privacy policy covers the following topics:
1. our collection of your personal information
2. our use of your personal information
3. accessing, reviewing and amending your personal information
5. no spam or spoof emails
6. protecting your personal information
7. how you can contact us about privacy questions
Our collection of your personal information:
The personal information we collect will include your name, email address, postal address, delivery address (if different), telephone number and mobile number. We may also collect personal information provided by you together with or as part of any Submission. We do not collect payment details, payment card details or bank account details.

1) As part of your registration on the App, you will be asked to provide us with certain personal information, such as your name, email address and/or mobile number and other similar information as well as some additional information.
Additionally, in order for us to verify your identity, we may need to request from you Otp SMS to your register mobile number.
Following your registration on the App, you should not post any personal information (including any financial information) anywhere on the App.
2) Please note that we may use your Internet protocol (or IP) address (which is a unique number assigned to your computer server or your Internet service provider (or ISP)) to analyze user trends and improve the administration of the App.
Finally, we may collect additional information from or about you in other ways not specifically described here.
For example, we may collect information related to your contact with our customer support team.
Where we aggregate personal information for statistical purposes, such aggregated personal information shall be anonymized.
Our use of your personal information:
1) We only use your personal information to provide services and customer support to you; to measure and improve our services to you; to prevent illegal activities and implement our user agreement with you ("User Agreement").
2) Though we make every effort to preserve your privacy, we may need to disclose your personal information to law enforcement agencies, government agencies or other third parties where we are compelled so to do by court order or similar legal procedure; where we are required to disclose your personal information to comply with law; where we are cooperating with an ongoing law enforcement investigation or where we have a good faith belief that our disclosure of your personal information is necessary to prevent physical harm or financial loss, to report suspected illegal activity or to investigate a possible violation of our User Agreement.
3) We do not sell or rent any of your personal information to third parties in the normal course of doing business and will only share your personal information with third parties in accordance with this privacy policy.
4) By registering on the App, you give us your express consent to receive promotional emails and messages about our services and emails and messages announcing changes to, and new features on, the App.
5) Additionally, we do use comments made by you about the App for marketing purposes and by making such comments you expressly consent to our using such comments for marketing purposes.
We will collect certain information (such as time and date of your visit to the Site, pages viewed, IP address and browser used to access the Site) whenever you visit the Site. We use this information to understand how our Site is used and to improve the Site. This information is anonymized and cannot be linked back to you.
We will use your personal information to:
establish and administer your account, process and deliver your Orders and to provide you with services and information;
improve the Site layout, Content and functionality; and
carry out research on our users’ demographics and tastes.
We may use your personal information to send you information about our Products and services and those of our associated companies. You can choose to opt out of this at any time by clicking on the “unsubscribe” link in any marketing email that we send to you.
We will share your personal information with:
third parties who provide us with services, in order to provide us with those services (for example, marketing, website administration, Order delivery purposes, and fraud protection services); and
any governmental or legal authority if required by law.
We may transfer our databases containing your personal information if we sell our business or part of it. Other than as set out in this Privacy Policy, we shall NOT sell or disclose your personal information to third parties without obtaining your prior consent. The Site may contain links to other sites or frames of other sites. Please be aware that we are not responsible for the privacy practices or content of those third other sites, nor for any third party to whom we transfer your personal information in accordance with our Privacy Policy.
Accessing, reviewing and amending your personal information:
1) You can access and review your personal information in the Profile section of the App.
If your personal information changes in any way or is incorrectly presented on the App you should immediately update or correct your personal information (as applicable) by accessing the Profile section on the App or, alternatively, by contacting our customer support team.
2) Please note that we shall retain your personal information during and following the end of your use of the App as required to comply with law, for technical troubleshooting requirements, to prevent fraud, to assist in any legal investigations and to take any other actions otherwise permitted by law.
No spam or spoof emails:
1) We do not tolerate spam. To report App related spam or spoof emails, please forward the email to support@BAMThobe.com. 
Protecting your personal information:
We hold your personal information on our servers by providing us with personal information, you consent to the transfer of your personal information to, and its storage on, our servers. We take every precaution to safeguard all your personal information from unauthorized access, use or disclosure.
All personal information is encrypted. However, the Internet is not a secure medium and we cannot guarantee the privacy of your personal information.
Your mobile number will be used to confirm your login to the app. 
Never share your otpsmswith anyone.
If you are concerned that your mobile number changed orhas been compromised, please contact our customer support team immediately and ensure you change your mobile number by logging onto the profile section of the App.
How you can contact us about privacy questions:
The Website Policies and Terms & Conditions may be changed or updated occasionally to meet the requirements and standards. Therefore the Customers’ are encouraged to frequently visit these sections in order to be updated about the changes on the website. Modifications will be effective on the day they are posted.
You may request access to your personal information and you may request that we correct any inaccuracies in that personal information. At any stage you also have the right to ask us to stop using your personal information for direct marketing purposes.
If you have questions or concerns about our collection and use of your personal information, please contact our customer support team at support@BAMthoe.com.
© 2020Reda Khass for Tailoring Services



Vestibulum sed mattis odio. Nunc sagittis, dolor ut congue auctor, lacus diam imperdiet massa, ut posuere orci enim vel est. Cras gravida mollis convallis. Integer euismod venenatis molestie. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas aliquam luctus vulputate. Aenean egestas eleifend lectus, at facilisis diam lobortis vel. Etiam vehicula tellus sit amet leo viverra, et dignissim diam maximus. Fusce imperdiet laoreet dolor sagittis dignissim.

In felis ante, dictum gravida tempus ut, pulvinar at metus. In viverra dignissim portti</h1>