var dataTable;
$(document).ready(function() {
    dataTable = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        ajax: $('#data-table-url').val(),
        order: [[ 0, "desc" ]],
        fnDrawCallback: function() {
            if ($('.pagination.pagination-sm li').length <= 5) {
                $('.dataTables_paginate').hide();
            } else {
                $('.dataTables_paginate').show();
            }
        },
        columns: [
            { data: 'emp_id', name: 'emp_id', visible: true ,width: 20 },
            { data: 'name', name: 'name', visible: true , width: 40},
            { data: 'salary', name: 'salary', visible: true ,width: 50 },
            { data: 'assigned_hours', name: 'assigned_hours',width: 50 },
            { data: 'acheived_hours', name: 'acheived_hours',width: 50 },
            { data: 'other_deduction', name: 'other_deduction',width: 50 },
            { data: 'commission', name: 'commission',width: 50 },
            { data: 'total', name: 'total',width: 50 },
            { data: 'action', name: 'action', orderable: false, searchable: false, width: 40 }
        ]
    });
    $('.dataTables_filter input[type="search"]').on('keypress', function() {
        $('.dataTables_processing').css('visibility','hidden');  
    });
    var checkImageFile = true;
    $('#changeModuleForm').validate({
        debug: true,
        ignore: [],
        errorClass:'invalid-feedback',
        errorElement: 'span',
        rules:{
           title: {
                 required: true
            },
        },
        messages:{
            title: {
                required: 'Please enter title.'
            },
         },
         submitHandler: function(form) {
                form.submit();
            }
        });

    $('body').on('click', '.delete-records',function() {
        var Id = $(this).val();
        $('#confirm-modal-delete-id').val(Id);
        $('#confirm-modal').modal();
    }); 
    $('body').on('change','#changeStatus', function() {
        var checked   = $('input', this).is(':checked');
        var id = $(this).attr('module-id');
        if(id && $.isNumeric(id)) {
            checked = ((checked) ? '1': '0');
            var url = $('#base_url').val() + '/payroll/changeStatus';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: "POST",
                url: url,
                data: {id: id, status: checked},
                dataType: 'json',
                success: function (response) {
                    $('#growls-default').html('');
                    switch($.trim(response.status)) {
                        case 'success':
                            $.growl.notice({title: "Success!", message:  response.message});
                        break;
                        case 'error':
                            $.growl.error({title: "Oops!", message:  response.message});
                        break
                        default:
                            $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                        break;

                    }
                },
                error: function (response) {
                    $('#growls-default').html('');
                    $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
                }
            });
        }
    });

    $("#image_name").change(function(){
        var type = $(this).val();
        $('#Salesman-image').find('span').html('');
        switch(type.substring(type.lastIndexOf('.') + 1).toLowerCase()) {
            case 'png': case 'x-png': case 'gif': case 'jpeg': case 'pjpeg': case 'jpg': case 'svg': case 'svg+xml': 
                checkImageFile = true;
                readImageURL(this,'show-image-preview');
            break;
            default: 
                checkImageFile = false;
                //$('#service-image').find('span').html(langMsgs['imgErr']).css('display','inline-block');
            break;

        }
    });
    //Use for print options
    $('#printme').click(function() {
        window.print();
    });

    //calculate drawing salary
    $('#commission').blur(function() {
        let commission = $(this).val();
        let salary = $('#salary').val();
        let deduction = $('#other_deduction').val();
        let total = (Number(commission)+Number(salary))-Number(deduction);
        $('#total').val(total);
        //alert(total);
    });

    //calculate drawing salary
    $('#other_deduction').blur(function() {
        let deduction = $(this).val();
        let salary = $('#salary').val();
        let commission = $('#commission').val();
        let total = (Number(commission)+Number(salary))-Number(deduction);
        $('#total').val(total);
        //alert(total);
    });
})

function deleteRecord() {
    var delete_url = $('#base_url').val()+'/payroll';
    var id = $("#confirm-modal-delete-id").val();
    $("#confirm-modal-delete-id").val('');
    $('.deleteRecord').attr('disabled',true).css('cursor','wait');
    $('.show-loader').css('display','block');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })
    $.ajax({
        type: "DELETE",
        url: delete_url + '/' + id,
        dataType: 'json',
        success: function (response) {
            $('.deleteRecord').attr('disabled',false).css('cursor','pointer');
            $('.show-loader').css('display','none');
            $('#confirm-modal').modal('hide');
            $('#growls-default').html('');
            switch($.trim(response.status)) {
                case 'success':
                    dataTable.ajax.reload();
                    $.growl.notice({title: "Success!", message:  response.message});
                break;
                case 'error':
                    $.growl.error({title: "Oops!", message:  response.message});
                break
                default:
                    $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                break;

            }
        },
        error: function (response) {
            $('.deleteRecord').attr('disabled',false).css('cursor','pointer');
            $('.show-loader').css('display','none');
            $('#confirm-modal').modal('hide');
            $('#growls-default').html('');
            $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
        }
    });
}



//For search payroll
$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#paySearch").click(function(e){
        e.preventDefault();
        var payYear = $("select[name=payYear]").val();
        var payMonth = $("select[name=payMonth]").val();

        if(payYear == ''){
            console.log("Please Enter Year Before Submit");
        }
        else if(payMonth == ''){
            console.log("Please Enter Month Before Submit");            
        }
        $.ajax({
           type:'get',
           url:'/admin/payroll/table_data',
           data:{payYear:payYear, payMonth:payMonth},

           success:function(data){
              var tableUrl = $('#data-table-url').val();
              var appendData = tableUrl+'?'+'payYear='+payYear+'&'+'payMonth='+payMonth;
              
                dataTable = $('#data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    destroy: true,
                    ajax: appendData,
                    order: [[ 0, "desc" ]],
                    fnDrawCallback: function() {
                        if ($('.pagination.pagination-sm li').length <= 5) {
                            $('.dataTables_paginate').hide();
                        } else {
                            $('.dataTables_paginate').show();
                        }
                    },
                    columns: [
                        { data: 'emp_id', name: 'emp_id', visible: true ,width: 20 },
                        { data: 'name', name: 'name', visible: true , width: 40},
                        { data: 'salary', name: 'salary', visible: true ,width: 50 },
                        { data: 'assigned_hours', name: 'assigned_hours',width: 50 },
                        { data: 'acheived_hours', name: 'acheived_hours',width: 50 },
                        { data: 'other_deduction', name: 'other_deduction',width: 50 },
                        { data: 'commission', name: 'commission',width: 50 },
                        { data: 'total', name: 'total',width: 50 },
                        { data: 'action', name: 'action', orderable: false, searchable: false, width: 40 }
                    ]
                });
           }
        });
    });

//get Emp Details
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#emp_id").on('change',function(e){
        e.preventDefault();
        let selectId = $(this).find(":selected").val();

        if(selectId == ''){
            console.log("Please Select Employee Id");
        }
        $.ajax({
           type:'get',
           url:'/admin/ajax/empData',
           data:{emp_id:selectId},

           success:function(result){
            $('#name').val(result.name);
            $('#salary').val(result.salary);
            $('#assigned_hours').val(result.assign_target);
            $('#no_defective_order').val(result.defectiveOrder);
            
           }
        });
    });