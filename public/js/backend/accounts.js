var dataTable;
$(document).ready(function() {
    dataTable = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        ajax: $('#data-table-url').val(),
        order: [[ 0, "desc" ]],
        fnDrawCallback: function() {
            if ($('.pagination.pagination-sm li').length <= 5) {
                $('.dataTables_paginate').hide();
            } else {
                $('.dataTables_paginate').show();
            }
        },
        columns: [
            { data: 'id', name: 'id', visible: true ,width: 20 },
            { data: 'account_date', name: 'account_date', width: 20 },
            { data: 'expense', name: 'expense', width: 20 },
            { data: 'income', name: 'income', width: 20 },
            // { data: 'category', name: 'category',width: 50 },
            { data: 'amount', name: 'amount',width: 50 },
            { data: 'comment', name: 'comment',width: 100 },
            { data: 'action', name: 'action', orderable: false, searchable: false, width: 20 }
        ]
    });
    $('.dataTables_filter input[type="search"]').on('keypress', function() {
        $('.dataTables_processing').css('visibility','hidden');  
    });
    var checkImageFile = true;
    $('#changeModuleForm').validate({
        debug: true,
        ignore: [],
        errorClass:'invalid-feedback',
        errorElement: 'span',
        rules:{
           attachment: {
                 required: true
            },
            amount: {
                 required: true
            },
            account_date: {
                 required: true
            },
        },
        messages:{
            attachment: {
                required: 'Please enter attachment.'
            },
            amount: {
                required: 'Please enter amount.'
            },
            account_date: {
                required: 'Please enter date.'
            },
         },
         submitHandler: function(form) {
                form.submit();
            }
        });

    $('body').on('click', '.delete-records',function() {
        var Id = $(this).val();
        $('#confirm-modal-delete-id').val(Id);
        $('#confirm-modal').modal();
    }); 
    $('body').on('change','#changeStatus', function() {
        var checked   = $('input', this).is(':checked');
        var id = $(this).attr('module-id');
        if(id && $.isNumeric(id)) {
            checked = ((checked) ? '1': '0');
            var url = $('#base_url').val() + '/accounts/changeStatus';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: "POST",
                url: url,
                data: {id: id, status: checked},
                dataType: 'json',
                success: function (response) {
                    $('#growls-default').html('');
                    switch($.trim(response.status)) {
                        case 'success':
                            $.growl.notice({title: "Success!", message:  response.message});
                        break;
                        case 'error':
                            $.growl.error({title: "Oops!", message:  response.message});
                        break
                        default:
                            $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                        break;

                    }
                },
                error: function (response) {
                    $('#growls-default').html('');
                    $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
                }
            });
        }
    });

    $("#image_name").change(function(){
        var type = $(this).val();
        $('#Salesman-image').find('span').html('');
        switch(type.substring(type.lastIndexOf('.') + 1).toLowerCase()) {
            case 'png': case 'x-png': case 'gif': case 'jpeg': case 'pjpeg': case 'jpg': case 'svg': case 'svg+xml': 
                checkImageFile = true;
                readImageURL(this,'show-image-preview');
            break;
            default: 
                checkImageFile = false;
                //$('#service-image').find('span').html(langMsgs['imgErr']).css('display','inline-block');
            break;

        }
    });
})

function deleteRecord() {
    var delete_url = $('#base_url').val()+'/accounts';
    var id = $("#confirm-modal-delete-id").val();
    $("#confirm-modal-delete-id").val('');
    $('.deleteRecord').attr('disabled',true).css('cursor','wait');
    $('.show-loader').css('display','block');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })
    $.ajax({
        type: "DELETE",
        url: delete_url + '/' + id,
        dataType: 'json',
        success: function (response) {
            $('.deleteRecord').attr('disabled',false).css('cursor','pointer');
            $('.show-loader').css('display','none');
            $('#confirm-modal').modal('hide');
            $('#growls-default').html('');
            switch($.trim(response.status)) {
                case 'success':
                    dataTable.ajax.reload();
                    $.growl.notice({title: "Success!", message:  response.message});
                break;
                case 'error':
                    $.growl.error({title: "Oops!", message:  response.message});
                break
                default:
                    $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                break;

            }
        },
        error: function (response) {
            $('.deleteRecord').attr('disabled',false).css('cursor','pointer');
            $('.show-loader').css('display','none');
            $('#confirm-modal').modal('hide');
            $('#growls-default').html('');
            $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
        }
    });
}
$(document).ready(function() {

    $( "#type" ).change(function() {
      if($( this ).val() == 1){

        $( "#categorySelect" ).hide();
        $('#categorySelect').val('');
        $( "#incomeCategorySelect" ).show();

      } else if($( this ).val() == 0){

        $( "#categorySelect" ).show();
        $( "#incomeCategorySelect" ).hide();
        $('#incomeCategorySelect').val('');
      }
    });
})



//For search account
$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#searchInEx").click(function(e){
        e.preventDefault();
        var daterange = $("input[name=daterange]").val();
        var fromDate = daterange.split("-");
        var from = fromDate[0];
        var to = fromDate[1];

        $.ajax({
           type:'get',
           url:'/admin/accounts/table_data',
           data:{from:from, to:to},

           success:function(data){
              var tableUrl = $('#data-table-url').val();
              var appendData = tableUrl+'?'+'from='+from+'&'+'to='+to;

                dataTable = $('#data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    destroy: true,
                    ajax: appendData,
                    order: [[ 0, "desc" ]],
                    fnDrawCallback: function() {
                        if ($('.pagination.pagination-sm li').length <= 5) {
                            $('.dataTables_paginate').hide();
                        } else {
                            $('.dataTables_paginate').show();
                        }
                    },
                    columns: [
                        { data: 'id', name: 'id', visible: true ,width: 20 },
                        { data: 'account_date', name: 'account_date', width: 20 },
                        { data: 'expense', name: 'expense', width: 20 },
                        { data: 'income', name: 'income', width: 20 },
                        // { data: 'category', name: 'category',width: 50 },
                        { data: 'amount', name: 'amount',width: 50 },
                        { data: 'comment', name: 'comment',width: 100 },
                        { data: 'action', name: 'action', orderable: false, searchable: false, width: 20 }
                    ]
                });
           }
        });
        //Get summary data
          $.ajax({
           type:'get',
           url:'/admin/accounts/summary',
           data:{from:from, to:to},
           dataType:'JSON',

           success:function(data){
            $("#summary").show();

            $(".cash").text('SAR '+data.cash);
            $(".petty_cash").text('SAR '+data.pettyCashIncome);
            $(".inventory").text('SAR '+data.inventoryCash);
            $(".account_payable").text('SAR '+data.payableCash);
            $(".expenses").text('SAR '+data.otherExpenses);
            $(".payroll").text('SAR '+data.payrolls);
            $(".summary_heading").text('Summary: From '+from+' To '+to);
            //console.log(data.cash);
           }
       });
    });

