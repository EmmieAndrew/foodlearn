var dataTable;
$(document).ready(function() {
    dataTable = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        ajax: $('#data-table-url').val(),
        order: [[ 0, "desc" ]],
        fnDrawCallback: function() {
            if ($('.pagination.pagination-sm li').length <= 5) {
                $('.dataTables_paginate').hide();
            } else {
                $('.dataTables_paginate').show();
            }
        },
        columns: [
            { data: 'id', name: 'id', visible: true ,width: 20 },
            { data: 'name', name: 'name', width: 40 },
            { data: 'image', name: 'image',width: 100 },
            { data: 'price', name: 'price',width: 50 },
            { data: 'action', name: 'action', orderable: false, searchable: false, width: 40 }
        ]
    });
    $('.dataTables_filter input[type="search"]').on('keypress', function() {
        $('.dataTables_processing').css('visibility','hidden');  
    });
    var checkImageFile = true;
    $('#changeModuleForm').validate({
        debug: true,
        ignore: [],
        errorClass:'invalid-feedback',
        errorElement: 'span',
        rules:{
           title: {
                 required: true
            },
        },
        messages:{
            title: {
                required: 'Please enter title.'
            },
         },
         submitHandler: function(form) {
                form.submit();
            }
        });
    $('body').on('click', '.delete-records',function() {
        var Id = $(this).val();
        $('#confirm-modal-delete-id').val(Id);
        $('#confirm-modal').modal();
    }); 
    $("#model_image").change(function(){
        var type = $(this).val();
        $('#model_image').find('span').html('');
        switch(type.substring(type.lastIndexOf('.') + 1).toLowerCase()) {
            case 'png': case 'x-png': case 'gif': case 'jpeg': case 'pjpeg': case 'jpg': case 'svg': case 'svg+xml': 
                checkImageFile = true;
                readImageURL(this,'show-image-preview');
            break;
            default: 
                checkImageFile = false;
                //$('#service-image').find('span').html(langMsgs['imgErr']).css('display','inline-block');
            break;

        }
    });
})

function deleteRecord() {
    var delete_url = $('#base_url').val()+'/modelpricing';
    var id = $("#confirm-modal-delete-id").val();
    $("#confirm-modal-delete-id").val('');
    $('.deleteRecord').attr('disabled',true).css('cursor','wait');
    $('.show-loader').css('display','block');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })
    $.ajax({
        type: "DELETE",
        url: delete_url + '/' + id,
        dataType: 'json',
        success: function (response) {
            $('.deleteRecord').attr('disabled',false).css('cursor','pointer');
            $('.show-loader').css('display','none');
            $('#confirm-modal').modal('hide');
            $('#growls-default').html('');
            switch($.trim(response.status)) {
                case 'success':
                    dataTable.ajax.reload();
                    $.growl.notice({title: "Success!", message:  response.message});
                break;
                case 'error':
                    $.growl.error({title: "Oops!", message:  response.message});
                break
                default:
                    $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                break;

            }
        },
        error: function (response) {
            $('.deleteRecord').attr('disabled',false).css('cursor','pointer');
            $('.show-loader').css('display','none');
            $('#confirm-modal').modal('hide');
            $('#growls-default').html('');
            $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
        }
    });
}



