var dataTable;
$(document).ready(function() {
    dataTable = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        ajax: $('#data-table-url').val(),
        order: [[ 0, "desc" ]],
        fnDrawCallback: function() {
            if ($('.pagination.pagination-sm li').length <= 5) {
                $('.dataTables_paginate').hide();
            } else {
                $('.dataTables_paginate').show();
            }
        },
        columns: [
            { data: 'order_id', name:  'order_id', width: 60 },
            { data: 'user_id', name:  'user_id', width: 80 },
            { data: 'transaction_type', name: 'transaction_type', width:50},
            { data: 'points',name: 'points',width:  50 },
            { data: 'reedem_code', name: 'reedem_code', width: 70 },
            { data: 'created_at', name: 'created_at', width: 50 },
            // { data: 'action', name: 'action', orderable: false, searchable: false, width: 60 }
        ]
    });
    $('.dataTables_filter input[type="search"]').on('keypress', function() {
        $('.dataTables_processing').css('visibility','hidden');  
    });

    $('#changeModuleForm').validate({
        debug: true,
        ignore: [],
        errorClass:'invalid-feedback',
        errorElement: 'span',
        rules:{
            name: {
                 required: true
            },
            email: {
                 required: true,
                  email:true
            },
            code: {
                 required: true,
            }, 
        },
        messages:{
            name: {
                required: 'Please enter name.'
            },
            email: {
                required: 'Please enter email.',
                email:  'Please enter valid email.'
            },
            code: {
                required: 'Please enter code.'
               // remote :'This code address has already been taken.'
            },
        },
         submitHandler: function(form) {
                form.submit();
            }
        });

   $('body').on('click', '.delete-records',function() {
        var Id = $(this).val();
        $('#confirm-modal-delete-id').val(Id);
        $('#confirm-modal').modal();
    }); 
    $('body').on('change','#changeStatus', function() {
        var checked   = $('input', this).is(':checked');
        var id = $(this).attr('module-id');
        if(id && $.isNumeric(id)) {
            checked = ((checked) ? '1': '0');
            var url = $('#base_url').val() + '/promoCodes/changeStatus';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: "POST",
                url: url,
                data: {id: id, status: checked},
                dataType: 'json',
                success: function (response) {
                    $('#growls-default').html('');
                    switch($.trim(response.status)) {
                        case 'success':
                            $.growl.notice({title: "Success!", message:  response.message});
                        break;
                        case 'error':
                            $.growl.error({title: "Oops!", message:  response.message});
                        break
                        default:
                            $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                        break;

                    }
                },
                error: function (response) {
                    $('#growls-default').html('');
                    $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
                }
            });
        }
    });

    
})

function deleteRecord() {
    var delete_url = $('#base_url').val()+'/promoCodes';
    var id = $("#confirm-modal-delete-id").val();
    $("#confirm-modal-delete-id").val('');
    $('.deleteRecord').attr('disabled',true).css('cursor','wait');
    $('.show-loader').css('display','block');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })
    $.ajax({
        type: "DELETE",
        url: delete_url + '/' + id,
        dataType: 'json',
        success: function (response) {
            $('.deleteRecord').attr('disabled',false).css('cursor','pointer');
            $('.show-loader').css('display','none');
            $('#confirm-modal').modal('hide');
            $('#growls-default').html('');
            switch($.trim(response.status)) {
                case 'success':
                    dataTable.ajax.reload();
                    $.growl.notice({title: "Success!", message:  response.message});
                break;
                case 'error':
                    $.growl.error({title: "Oops!", message:  response.message});
                break
                default:
                    $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                break;

            }
        },
        error: function (response) {
            $('.deleteRecord').attr('disabled',false).css('cursor','pointer');
            $('.show-loader').css('display','none');
            $('#confirm-modal').modal('hide');
            $('#growls-default').html('');
            $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
        }
    });
}















