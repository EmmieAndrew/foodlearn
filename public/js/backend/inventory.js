var dataTable;
$(document).ready(function() {
    $('#dob').datetimepicker({
        format:'d M Y',
        timepicker: false
    });
	dataTable = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: $('#data-table-url').val(),
        order: [[ 0, "desc" ]],
        fnDrawCallback: function() {
            if ($('.pagination.pagination-sm li').length <= 5) {
                $('.dataTables_paginate').hide();
            } else {
                 $('.dataTables_paginate').show();
            }
        },
        columns: [
            { data: 'id', name: 'id', visible: true, width: 50},
            { data: 'product_image', name: 'product_image',width: 110 }, 
            { data: 'product_name', name: 'product_name',width: 50 },
            { data: 'product_code', name: 'product_code',width: 50 },
            { data: 'comment', name: 'comment',width: 100 },
            { data: 'request_by', name: 'request_by',width:10 },
            { data: 'created_at', name: 'created_at',width: 100 }
        ],
        columnDefs: [ { targets: '_all', defaultContent: '--' } ]
    });
    $('.dataTables_filter input[type="search"]').on('keypress', function() {
        $('.dataTables_processing').css('visibility','hidden');  
    });
    var checkImageFile = true;
    $('#changeModuleForm').validate({
        debug: true,
        ignore: [],
        errorClass:'invalid-feedback',
        errorElement: 'span',
        rules:{
           title: {
                 required: true
            },
        },
        messages:{
            title: {
                required: 'Please enter title.'
            },
         },
         submitHandler: function(form) {
                form.submit();
            }
        });
    $("#product_image").change(function(){
        var type = $(this).val();
        $('#product_image').find('span').html('');
        switch(type.substring(type.lastIndexOf('.') + 1).toLowerCase()) {
            case 'png': case 'x-png': case 'gif': case 'jpeg': case 'pjpeg': case 'jpg': case 'svg': case 'svg+xml': 
                checkImageFile = true;
                readImageURL(this,'show-image-preview');
            break;
            default: 
                checkImageFile = false;
                //$('#service-image').find('span').html(langMsgs['imgErr']).css('display','inline-block');
            break;

        }
    });
    $('body').on('click', '.delete-records',function() {
        var Id = $(this).val();
        $('#confirm-modal-delete-id').val(Id);
        $('#confirm-modal').modal();
    });
    $('#customers-edit-form').validate({
        ignore: [],
        rules:{
            name: {
                required: true,
            }, 
            email: {
                required: true,
                email: true,
                remote: {
                    url: $("#base_url").val()+"/customers/checkEmail",
                    type: "GET",
                    cache: false,
                    data: { 'email':function(){return $('#email').val()}, 'customerid':function() { return $('#customer_id').val() } },
                    dataType: "json"
                }
            },
            
            phone: {
                number: true,
                maxlength: 12
            },
            status: {
            	required: true
            }
        },
        messages:{
            name: {
                required: 'Please enter name.'
            },
            email:{
                required: 'Please enter email address.',
                email: 'Please enter a valid email address.',
                remote: 'This email address has already been taken.'
            },
            phone: {
                number: "Please enter numbers only.",
                maxlength: "Please enter no more than 12 digits."
            },
            status: {
            	required: 'Please select status.'
            }
        },
        errorElement:"span",
        errorClass:"invalid-feedback",
        errorPlacement: function(error, element) {
            $(element).next('p').html(error);
        },
    });
});

function deleteRecord() {
    var delete_url = $('#base_url').val()+'/catalogues';
    var id = $("#confirm-modal-delete-id").val();
    $("#confirm-modal-delete-id").val('');
    $('.deleteRecord').attr('disabled',true).css('cursor','wait');
    $('.show-loader').css('display','block');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })
    $.ajax({
        type: "DELETE",
        url: delete_url + '/' + id,
        dataType: 'json',
        success: function (response) {
            $('.deleteRecord').attr('disabled',false).css('cursor','pointer');
            $('.show-loader').css('display','none');
            $('#confirm-modal').modal('hide');
            $('#growls-default').html('');
            switch($.trim(response.status)) {
                case 'success':
                    dataTable.ajax.reload();
                    $.growl.notice({title: "Success!", message:  response.message});
                break;
                case 'error':
                    $.growl.error({title: "Oops!", message:  response.message});
                break
                default:
                    $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                break;

            }
        },
        error: function (response) {
            $('.deleteRecord').attr('disabled',false).css('cursor','pointer');
            $('.show-loader').css('display','none');
            $('#confirm-modal').modal('hide');
            $('#growls-default').html('');
            $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
        }
    });
}
