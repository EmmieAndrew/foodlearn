var dataTable;
$(document).ready(function() {
    $('#dob').datetimepicker({
        format:'d M Y',
        timepicker: false
    });
	dataTable = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: $('#data-table-url').val(),
        order: [[ 0, "desc" ]],
        fnDrawCallback: function() {
            if ($('.pagination.pagination-sm li').length <= 5) {
                $('.dataTables_paginate').hide();
            } else {
                 $('.dataTables_paginate').show();
            }
        },
        columns: [
            { data: 'customer_unique_id', name: 'customer_unique_id',width: 80 },
            { data: 'name', name: 'name',width: 80 },
            { data: 'phone', name: 'phone',width: 100 },
            { data: 'email', name: 'email',width:10 },
            // { data: 'created_by', name: 'created_by',width: 100 },
            // { data: 'account_type', name: 'account_type',width: 100 },
            // { data: 'last_order_date', name: 'last_order_date',width: 110 },
            // { data: 'total_order', name: 'total_order',width: 40 },
            { data: 'status', name: 'status', width: 60, searchable: false },
            { data: 'action', name: 'action', orderable: false, searchable: false, width: 20 }
        ],
        columnDefs: [ { targets: '_all', defaultContent: '--' } ]
    });
    $(document).on('click','.delete-customer',function(){
        var faq_id = $(this).val();
        $('#DeleteCustomer-delete-id').val(faq_id);
        $('#DeleteCustomer').modal('show');
    });
    $('#customers-edit-form').validate({
        ignore: [],
        rules:{
            name: {
                required: true,
            }, 
            email: {
                required: true,
                email: true,
                remote: {
                    url: $("#base_url").val()+"/customers/checkEmail",
                    type: "GET",
                    cache: false,
                    data: { 'email':function(){return $('#email').val()}, 'customerid':function() { return $('#customer_id').val() } },
                    dataType: "json"
                }
            },
            
            phone: {
                number: true,
                maxlength: 12
            },
            status: {
            	required: true
            }
        },
        messages:{
            name: {
                required: 'Please enter name.'
            },
            email:{
                required: 'Please enter email address.',
                email: 'Please enter a valid email address.',
                remote: 'This email address has already been taken.'
            },
            phone: {
                number: "Please enter numbers only.",
                maxlength: "Please enter no more than 12 digits."
            },
            status: {
            	required: 'Please select status.'
            }
        },
        errorElement:"span",
        errorClass:"invalid-feedback",
        errorPlacement: function(error, element) {
            $(element).next('p').html(error);
        },
    });
});
// $(document).on('change','#country',function() {
//     var countryCode = $('#country').val();
//     if(countryCode != '') {
//         $.ajax({
//             type: 'get',
//             url: $('#getStates').val(),
//             data: { country_code: countryCode },
//             success: function (response) {
//                 $('#state').html(response.state_html);
//             }
//         });
//     }
// });
function changePasswordCustomers(customers_id) {   
    $("#customer_id").val(customers_id);
}
function changeCustomerStatus(customer_id, status) {
    if(customer_id && $.isNumeric(status)) {
        var url = $('#base_url').val() + '/customers/changeStatus';
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        $.ajax({
            type: "POST",
            url: url,
            data: {customer_id: customer_id, status: status},
            dataType: 'json',
            success: function (response) {
                switch($.trim(response.status)) {
                    case 'success':
                        $.growl.notice({title: "Success!", message:  response.message});
                    break;
                    case 'error':
                        $.growl.error({title: "Oops!", message:  response.message});
                    break
                    default:
                        $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                    break;
                }
                dataTable.ajax.reload();
            },
            error: function (response) {
                $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
                dataTable.ajax.reload();
            }
        });
    }
}
function DeleteCustomer() {
    var delete_url = $('#base_url').val()+'/customers/delete';
    var customer_id = $("#DeleteCustomer-delete-id").val();
    $("#DeleteCustomer-delete-id").val('');
    $('.show-loader').css('display','block');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })
    $.ajax({
        type: "POST",
        url: delete_url ,
        data: {customer_id: customer_id},
        dataType: 'json',
        success: function (data) {
            $('.show-loader').css('display','none');
            dataTable.ajax.reload();
            $('#growls-default').html('');
            switch($.trim(data.status)) {
                case 'success':
                    $.growl.notice({title: "Success!", message:  data.message});
                break;
                case 'error':
                    $.growl.error({title: "Oops!", message:  data.message});
                break
                default:
                    $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                break;
            }
        },
        error: function (data) {
            $('.show-loader').css('display','none');
            $('#growls-default').html('');
            $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
        }
    });
}

