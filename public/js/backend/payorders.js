var dataTable;
$(document).ready(function() {
    dataTable = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        ajax: $('#data-table-url').val(),
        order: [[ 0, "desc" ]],
        fnDrawCallback: function() {
            if ($('.pagination.pagination-sm li').length <= 5) {
                $('.dataTables_paginate').hide();
            } else {
                $('.dataTables_paginate').show();
            }
        },
        columns: [
            //{ data: 'id', name: 'id', visible: true ,width: 20 },
            { data: 'refrence_number', name: 'refrence_number',width: 50 },
            { data: 'date', name: 'date',width: 50 },
            { data: 'supplier', name: 'supplier', width: 40 },
            { data: 'product_name', name: 'product_name',width: 50 },
            { data: 'total', name: 'total',width: 50 },
            { data: 'tax', name: 'tax',width: 50 },
            { data: 'purchase_amount', name: 'purchase_amount',width: 50 },
            { data: 'paid_amount', name: 'paid_amount',width: 50 },
            { data: 'remaining_amount', name: 'remaining_amount',width: 50 },
            { data: 'action', name: 'action', orderable: false, searchable: false, width: 10 }
        ]
    });
    $('.dataTables_filter input[type="search"]').on('keypress', function() {
        $('.dataTables_processing').css('visibility','hidden');  
    });
    var checkImageFile = true;
    $('#changeModuleForm').validate({
        debug: true,
        ignore: [],
        errorClass:'invalid-feedback',
        errorElement: 'span',
        rules:{
           quantity: {
                 required: true
            },
            unit_price: {
                 required: true
            },
            paid_amount: {
                 required: true
            },
            supplier_id: {
                 required: true
            },
        },
        messages:{
            quantity: {
                required: 'Please enter quantity.'
            },
            unit_price: {
                required: 'Please enter unit price.'
            },
            paid_amount: {
                required: 'Please enter paid amount.'
            },
            supplier_id: {
                required: 'Please select supplier.'
            },
         },
         submitHandler: function(form) {
                form.submit();
            }
        });

    $('body').on('click', '.delete-records',function() {
        var Id = $(this).val();
        $('#confirm-modal-delete-id').val(Id);
        $('#confirm-modal').modal();
    }); 
    $('body').on('change','#changeStatus', function() {
        var checked   = $('input', this).is(':checked');
        var id = $(this).attr('module-id');
        if(id && $.isNumeric(id)) {
            checked = ((checked) ? '1': '0');
            var url = $('#base_url').val() + '/suppliers/changeStatus';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: "POST",
                url: url,
                data: {id: id, status: checked},
                dataType: 'json',
                success: function (response) {
                    $('#growls-default').html('');
                    switch($.trim(response.status)) {
                        case 'success':
                            $.growl.notice({title: "Success!", message:  response.message});
                        break;
                        case 'error':
                            $.growl.error({title: "Oops!", message:  response.message});
                        break
                        default:
                            $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                        break;

                    }
                },
                error: function (response) {
                    $('#growls-default').html('');
                    $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
                }
            });
        }
    });

    $("#image_name").change(function(){
        var type = $(this).val();
        $('#supplier-image').find('span').html('');
        switch(type.substring(type.lastIndexOf('.') + 1).toLowerCase()) {
            case 'png': case 'x-png': case 'gif': case 'jpeg': case 'pjpeg': case 'jpg': case 'svg': case 'svg+xml': 
                checkImageFile = true;
                readImageURL(this,'show-image-preview');
            break;
            default: 
                checkImageFile = false;
                //$('#service-image').find('span').html(langMsgs['imgErr']).css('display','inline-block');
            break;

        }
    });

//Calculate product price
    $("#unit_price").change(function(){
        var unitPrice = $(this).val();
        var quantity = $("#quantity").val();        
        var purchaseAmount = quantity*unitPrice;
        $("#purchase_amount").val(purchaseAmount);
        //calcualte remaining amount
        var paidPrice = $("#paid_amount").val();
        var remainingAmount = purchaseAmount-paidPrice;
        $("#remaining_amount").val(remainingAmount); 
        $("#tax").val('');       
    });

    $("#quantity").change(function(){
        var quantity = $(this).val();
        var unitPrice = $("#unit_price").val();
        var purchaseAmount = quantity*unitPrice;
        $("#purchase_amount").val(purchaseAmount);
        //calcualte remaining amount
        var paidPrice = $("#paid_amount").val();
        var remainingAmount = purchaseAmount-paidPrice;
        $("#remaining_amount").val(remainingAmount);
        $("#tax").val('');
    });
//Calculate tax
    $("#tax").change(function(){
        var taxRate = $(this).val();
        var totalAmt = $("#purchase_amount").val();
        var taxAmt = ((totalAmt*taxRate)/100);
        var grandTotal = parseInt(totalAmt)+parseInt(taxAmt);
        $("#purchase_amount").val(grandTotal);
        $("#remaining_amount").val(grandTotal);
    });

//calcualte remaining amount
    $("#paid_amount").change(function(){
        var paidPrice = $(this).val();
        var purchaseAmount = $("#purchase_amount").val();
        var remainingAmount = purchaseAmount-paidPrice;
        $("#remaining_amount").val(remainingAmount);
    });
    
    $('#printme').click(function() {
        window.print();
    });
})

function deleteRecord() {
    var delete_url = $('#base_url').val()+'/payorders';
    var id = $("#confirm-modal-delete-id").val();
    $("#confirm-modal-delete-id").val('');
    $('.deleteRecord').attr('disabled',true).css('cursor','wait');
    $('.show-loader').css('display','block');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })
    $.ajax({
        type: "DELETE",
        url: delete_url + '/' + id,
        dataType: 'json',
        success: function (response) {
            $('.deleteRecord').attr('disabled',false).css('cursor','pointer');
            $('.show-loader').css('display','none');
            $('#confirm-modal').modal('hide');
            $('#growls-default').html('');
            switch($.trim(response.status)) {
                case 'success':
                    dataTable.ajax.reload();
                    $.growl.notice({title: "Success!", message:  response.message});
                break;
                case 'error':
                    $.growl.error({title: "Oops!", message:  response.message});
                break
                default:
                    $.growl.error({title: "Oops!", message: langMsgs['networkErr'] });
                break;

            }
        },
        error: function (response) {
            $('.deleteRecord').attr('disabled',false).css('cursor','pointer');
            $('.show-loader').css('display','none');
            $('#confirm-modal').modal('hide');
            $('#growls-default').html('');
            $.growl.error({title: "Oops!", message: langMsgs['internalErr'] });
        }
    });
}


