$(document).ready(function() {
	$('#email').valid();
    $('#email-error').hide();
	$(document).on('click', '#verifybtnEmail', function() {
		if($('#email').valid()) {
			sendOTPEmail();
		} else {
			return false;
		}
	});
    $(document).on('change', '#email', function() {
        $('.emailDiv').removeClass('varified-block');
        $('.emailDiv').addClass('phonenumberfied');
        $('#varifiedEmail').hide();
        $('#otpEmail').attr('placeholder', 'Verify');
        $('#hashEmail').val('');
		$('#verifybtnEmail').show();
        $('#resendOTPEmail').hide();
        $('#otpEmail').val('');
        $('#otpDivEmail').show();
	});
	$(document).on('click', '#resendOTPEmail', function() {
		if($('#email').valid()) {
			sendOTPEmail();
		}
	});
});
function sendOTPEmail() {
    var email = $('#email').val();
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })
    show_loader();
	$.ajax({
        type: 'POST',
        url: sendOTPEmailURL,
        data: { email: email },
        success:function(response) {
            hide_loader();
        	response = $.parseJSON(response);
            if(response.status == false) {
            	if(response.msg != '') {
            		$('#email-error').text(response.msg).show();
            	}
            } else {
                $('#otpEmail').attr('placeholder', 'OTP');
                $('#hashEmail').val(response.hash);
                $('#verifybtnEmail').hide();
            	$('#resendOTPEmail').show();
            	$('#otpEmail').val('');
            	$('#otpEmail').focus();
            }
        }
    });
}