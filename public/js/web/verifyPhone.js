$(document).ready(function() {
	$('#phone').valid();
	$('#phone-error').hide();
	$(document).on('click', '#verifybtn', function() {
		if($('#phone').valid()) {
			sendOTP();
		} else {
			return false;
		}
	});
    $(document).on('change', '#phone, #country_code', function() {
        $('.phoneDiv').removeClass('varified-block');
        $('.phoneDiv').addClass('phonenumberfied');
        $('#varifiedPhone').hide();
        $('#otp').attr('placeholder', 'Verify');
        $('#hash').val('');
		$('#verifybtn').show();
        $('#resendOTP').hide();
        $('#otp').val('');
        $('#otpDiv').show();
	});
	$(document).on('click', '#resendOTP', function() {
		if($('#phone').valid()) {
			sendOTP();
		}
	});
});
function sendOTP() {
    var phone = $('#phone').val();
    var country_code = $('#country_code').val();
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })
    show_loader();
	$.ajax({
        type: 'POST',
        url: sendOTPURL,
        data: { phone: phone, country_code: country_code },
        success:function(response) {
            hide_loader();
        	response = $.parseJSON(response);
            if(response.status == false) {
            	if(response.msg != '') {
            		$('#phone-error').text(response.msg).show();
            	}
            } else {
                $('#otp').attr('placeholder', 'OTP');
                $('#hash').val(response.hash);
                $('#verifybtn').hide();
            	$('#resendOTP').show();
            	$('#otp').val('');
            	$('#otp').focus();
            }
        }
    });
}