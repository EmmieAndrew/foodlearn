<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Timeline extends Model
{
    //wp_terms
    public $timestamps = false;
    public $table = 'wp_quick_timelines';

    public $fillable = [
        'asset_id',
        'title',
        'image',
        'date',
        'amount',
        'time',
        'icon',
        'content',
        'list',
        'videourl'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'asset_id' => 'string',
        'title' => 'string',
        'image' => 'string',
        'date' => 'string',
        'amount' => 'integer',
        'time' => 'string',
        'icon' => 'string',
        'content' => 'string',
        'list' => 'string',
        'videourl' => 'string'
    ];
}
