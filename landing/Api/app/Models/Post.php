<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //wp_posts
    public $timestamps = false;
    public $table = 'wp_posts';

    public $fillable = [
        'post_author',
        'post_title',
        'post_content',
        'post_status',
        'comment_status',
        'post_name',
        'menu_order',
        'post_excerpt',
        'to_ping',
        'pinged',
        'post_content_filtered'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'post_author' => 'string',
        'post_title' => 'string',
        'post_content' => 'string',
        'post_status' => 'integer',
        'comment_status' => 'string',
        'post_name' => 'string',
        'menu_order' => 'string',
        'post_excerpt' => 'string',
        'to_ping' => 'string',
        'pinged' => 'string',
        'post_content_filtered' => 'string'
    ];
}
