<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Salesman;
// use App\Models\User;
// use App\Models\Timeline;

use Validator;
use Illuminate\Support\Facades\Hash;
use DB;

class APIController extends Controller
{
    public $unauthorised = 401;
    public $successStatus = 200;
    public $badrequest = 400;


    /**************************************************************************/


    public function register(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'dob' => 'required',
            'country' => 'required',
            'city' => 'required',
            'password' => 'required',
            'type' => 'required', // 1 for totor and 2 for learner
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }

        $checkEmail = Salesman::where(['email'=>$request->email])->first();

        if($checkEmail){
             return response()->json(['status' => 0,'message'=>'Email already exist.']);
        }

        if ($request->type == '1') {
            $type = 1;
        }else{
            $type = 2;
        }
        $data = array(
            'type'=>$type,
            'name'=>$request->title,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'dob'=>$request->dob,
            'city'=>$request->city,
            'country'=>$request->country,
            'password'=>Hash::make($request->password)
        );

        // dd($data);
        $users = Salesman::create($data);
        // $url = url('public/images');
        $user = Salesman::where(['id'=>$users->id])->first();

        // $data['id'] =  $users->id;


        return response()->json(['status' => 1,'message' => 'Registered Successfully.','result' =>$user]);

    }

/**************************************************************************/

    public function login(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }
        // $url = url('public/images');
        $checkEmail = Salesman::where(['email'=>$request->email])->first();
        if($checkEmail){

            if (Hash::check($request->password, $checkEmail->password))
            {
                return response()->json(['status' => 1,'message' => 'Login Successfully.','result' => $checkEmail]);
            }else{
                return response()->json(['status' => 0,'message'=>'Password Mismatched']);
            }
        }else{
            return response()->json(['status' => 0,'message'=>'Account Not Registered or Deactivated Please contact support for more information.']);
        }


    }

/**************************************************************************/


    public function changePassword(Request $request){

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'password' => 'required',
            'cpassword' => 'required',

        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }

        $user = Salesman::where(['id'=>$request->user_id])->first();

        if(!$user){
            return response()->json(['status' => 0,'message'=>'Invalid user id']);
        }

        if ($request->password  != $request->cpassword)
        {
            return response()->json(['status' => 0,'message'=>'Password Mismatch']);
        }

        Salesman::where('id', $request->user_id)->update([
           'password' => Hash::make($request->password),
           // 'user_pass' => $request->password,
        ]);

        return response()->json(['status' => 1,'message' => 'Changed Successfully.']);


    }


/**************************************************************************/


    public function forgotPassword(Request $request){

        $validator = Validator::make($request->all(), [
            // 'user_id' => 'required',
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }

        $user = Salesman::where(['email'=>$request->email])->first();

        if(!$user){
            return response()->json(['status' => 0,'message'=>'Invalid user email']);
        }


        $otp  = rand(1111,9999);

        Salesman::where('email', $request->email)->update([
           'otp' => $otp
        ]);

       	$result = ['otp'=>$otp];

       return response()->json(['status' => 1,'message' => 'OTP sent to your email.','result' => $result]);


    }


/**************************************************************************/


    public function verifyOtp(Request $request){

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'otp' => 'required',

        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }

        $user = Salesman::where(['id'=>$request->user_id,'otp'=>$request->otp])->first();

        if(!$user){
            return response()->json(['status' => 0,'message'=>'Invalid OTP']);
        }

        // User::where('id', $request->user_id)->update([
        //    'is_active' => 2
        // ]);

        return response()->json(['status' => 1,'message' => 'Verified Successfully.']);

    }

/**************************************************************************/

    public function editProfile(Request $request){

         $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }

        $user = Salesman::where(['id'=>$request->user_id])->first();
        // print_r(json_encode($user));die();
        if(!$user){
            return response()->json(['status' => 0,'message'=>'Invalid user id']);
        }

        if ($request->hasFile('image')) {

            if ($request->file('image')->isValid()) {

               /* $validated = $request->validate([

                    'image' => 'mimes:jpeg,png|max:1014',

                ]);*/

                //$extension = $request->image->extension();
                //$image_name = time().".".$extension;
                //$request->image->storeAs('/images', $image_name);

                $image_name = time().'.'.$request->image->extension();

                $request->image->move(public_path('user_images'), $image_name);

            }else{
                $image_name = $user->image;
            }

        }else{
            $image_name = $user->image;
        }


        $data = array(
        	'title'=>$request->name?$request->name:$user['title'],
        	'phone'=>$request->phone?$request->phone:$user['phone'],
            'country'=>$request->country?$request->country:$user['country'],
            'city'=>$request->city?$request->city:$user['city'],
            'email'=>$request->email?$request->email:$user['email'],
            'image'=>$image_name?$image_name:'default.jpg',
        );

         Salesman::where('id', $request->user_id)->update($data);

        return response()->json(['status' => 1,'message' => 'Updated Successfully.']);

    }

    public function getProfile(Request $request){

         $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }


        $url = url('/user_images');

        $user = Salesman::where(['id'=>$request->user_id])->select(array('*', DB::raw("CONCAT('$url/', image) AS image")))->first();

        // $user = Salesman::where(['id'=>$request->user_id])->first();

        if(!$user){
            return response()->json(['status' => 0,'message'=>'Invalid user id']);
        }

        return response()->json(['status' => 1,'message' => 'Success','result'=>$user]);

    }

    // public function getCategory(Request $request){
    // 	$validator = Validator::make($request->all(), [
    //         'user_id' => 'required'
    //     ]);
    //     if ($validator->fails()) {
    //         return response()->json(['message'=>$validator->errors()->first(),'status'=>'false'], $this->badrequest);
    //     }else{
    //     	$addArr = [];
    //         $addresses = Category::orderBy('term_id','ASC')
    //         ->where('slug','!=','uncategorized')
    //         ->where('slug','!=','main-menu')
    //         ->where('slug','!=','footer-menu')
    //         ->get();
    //         $larr = json_decode($addresses,true);
    //         foreach($larr AS $add){
    //         	$list['id'] = (String)$add['term_id'];
    //         	$list['name'] = $add['name'];
    //         	$addArr[] = $list;
    //         }
	   //      return response()->json([
	   //      	'status'=>'1',
    //             'message'=>'Categories fetched successfully',
    //             'cartArray'=>$addArr
    //         ], $this->successStatus);
    //     }
    // }

    // public function addPost(Request $request){

    //     $validator = Validator::make($request->all(), [
    //     	'user_id' => 'required',
    //         'post_name' => 'required',
    //         'post_title' => 'required',
    //         'post_content' => 'required',
    //         'category' => 'required'
    //     ]);

    //     if ($validator->fails()) {
    //         return response()->json(['status' => 0,'message'=>"All fields are required"]);
    //     }


    //     if ($request->hasFile('picture')) {

    //         if ($request->file('picture')->isValid()) {

    //         	$image_name = time().'.'.$request->picture->extension();

    //             $request->picture->move(public_path('asset_images'), $image_name);

    //         }else{
    //             $image_name ="default.png";
    //         }

    //     }else{
    //         $image_name ="default.png";
    //     }


    //     $data = array(
    //     	'post_author'=>$request->user_id,
    //     	'post_title'=>$request->post_title,
    //         'post_content'=>$request->post_content,
    //         'post_status'=>'publish',
    //         'comment_status'=>'open',
    //         'post_excerpt'=>'NULL',
    //         'to_ping'=>'NULL',
    //         'pinged'=>'NULL',
    //         'post_content_filtered'=>'NULL',
    //         'post_name'=>$request->post_title,
    //         'menu_order'=>$request->category
    //     );


    //     $posts = Post::create($data);
    //     // $url = url('public/images');
    //     $post = Post::where(['id'=>$posts->id])->first();


    //     return response()->json(['status' => 1,'message' => 'Post Created Successfully.','result' =>$post]);

    // }

    // public function editAsset(Request $request){

    //      $validator = Validator::make($request->all(), [
    //         'user_id' => 'required',
    //         'asset_id' => 'required'
    //     ]);

    //     if ($validator->fails()) {
    //         return response()->json(['status' => 0,'message'=>"All fields are required"]);
    //     }

    //     $user = User::where(['id'=>$request->user_id])->first();
    //     // print_r(json_encode($user));die();
    //     if(!$user){
    //         return response()->json(['status' => 0,'message'=>'Invalid user id']);
    //     }

    //     if ($request->hasFile('image')) {

    //         if ($request->file('image')->isValid()) {

    //            /* $validated = $request->validate([

    //                 'image' => 'mimes:jpeg,png|max:1014',

    //             ]);*/

    //             //$extension = $request->image->extension();
    //             //$image_name = time().".".$extension;
    //             //$request->image->storeAs('/images', $image_name);

    //             $image_name = time().'.'.$request->image->extension();

    //             $request->image->move(public_path('user_images'), $image_name);

    //         }else{
    //             $image_name = $user->image;
    //         }

    //     }else{
    //         $image_name = $user->image;
    //     }
    //     $post = Post::where(['id'=>$request->asset_id])->first();

    //     $data = array(
    //     	'post_title'=>$request->post_title?$request->post_title:$post['post_title'],
    //         'post_content'=>$request->post_content?$request->post_content:$post['post_content'],
    //         'post_status'=>'publish',
    //         'comment_status'=>'open',
    //         'post_excerpt'=>'NULL',
    //         'to_ping'=>'NULL',
    //         'pinged'=>'NULL',
    //         'post_content_filtered'=>'NULL',
    //         'post_name'=>$request->post_name?$request->post_name:$post['post_name'],
    //         'menu_order'=>$request->menu_order?$request->menu_order:$post['menu_order'],
    //     );

    //      Post::where('id', $request->asset_id)->update($data);

    //     return response()->json(['status' => 1,'message' => 'Updated Successfully.']);

    // }

    // public function getAssets(Request $request){
    // 	$validator = Validator::make($request->all(), [
    //         'user_id' => 'required'
    //     ]);
    //     if ($validator->fails()) {
    //         return response()->json(['message'=>$validator->errors()->first(),'status'=>'false'], $this->badrequest);
    //     }else{
    //     	$addArr = [];
    //         $addresses = Post::orderBy('id','ASC')
    //         ->where('post_author','=',$request->user_id)
    //         ->get();
    //         $larr = json_decode($addresses,true);
	   //      return response()->json([
	   //      	'status'=>'1',
    //             'message'=>'Assets fetched successfully',
    //             'assetsArray'=>$larr
    //         ], $this->successStatus);
    //     }
    // }

    // public function addAssetToTimeline(Request $request){

    //     $validator = Validator::make($request->all(), [
    //         'asset_id' => 'required',
    //         'title' => 'required',
    //         'description' => 'required',
    //         // 'status' => 'required',
    //         'expense' => 'required',
    //         'year' => 'required',
    //         'month' => 'required',
    //         'videourl' => 'required',
    //         'picture' => 'required'
    //     ]);

    //     if ($validator->fails()) {
    //         return response()->json(['status' => 0,'message'=>"All fields are required"]);
    //     }


    //     if ($request->hasFile('picture')) {

    //         if ($request->file('picture')->isValid()) {

    //         	$image_name = time().'.'.$request->picture->extension();

    //             $request->picture->move(public_path('timeline_images'), $image_name);

    //         }else{
    //             $image_name ="default.png";
    //         }

    //     }else{
    //         $image_name ="default.png";
    //     }


    //     $data = array(
    //         'asset_id'=>$request->asset_id,
    //     	'title'=>$request->title,
    //         'image'=>$image_name,
    //         'content'=>$request->description,
    //         'date'=>$request->month.$request->year,
    //         'amount'=>$request->expense,
    //         'videourl'=>$request->videourl
    //     );


    //     $posts = Timeline::create($data);
    //     $post = Timeline::where(['id'=>$posts->id])->first();


    //     return response()->json(['status' => 1,'message' => 'Addto Timeline Successfully.','result' =>$post]);

    // }
}
